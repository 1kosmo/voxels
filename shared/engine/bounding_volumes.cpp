#include <glm/gtc/matrix_transform.hpp>

#include "bounding_volumes.hpp"
#include "intersection.hpp"

using namespace glm;

plane::plane(vec3 normal, float d)
    : normal(normal), d(d)
{
    // Consider normalizing the plane normal
}


aabb::aabb(vec3 c, vec3 hws)
    : center(c), halfwidths(hws)
{
}

aabb::aabb(const obb& oriented)
{
    glm::vec3 min(std::numeric_limits<float>::max());
    glm::vec3 max(std::numeric_limits<float>::min());

    std::vector<glm::vec3> points; points.reserve(8);
    glm::vec3 x_half = oriented.obb_basis[0] * oriented.halfwidths;
    glm::vec3 y_half = oriented.obb_basis[1] * oriented.halfwidths;
    glm::vec3 z_half = oriented.obb_basis[2] * oriented.halfwidths;    

    points.push_back(oriented.center + x_half + y_half + z_half);
    points.push_back(oriented.center + x_half + y_half - z_half);
    points.push_back(oriented.center + x_half - y_half + z_half);
    points.push_back(oriented.center + x_half - y_half - z_half);
    points.push_back(oriented.center - x_half + y_half + z_half);
    points.push_back(oriented.center - x_half + y_half - z_half);
    points.push_back(oriented.center - x_half - y_half + z_half);
    points.push_back(oriented.center - x_half - y_half - z_half);

    for (int i = 0; i < 8; ++i)
    {
        for (int j = 0; j < 3; ++j)
        {
            min[j] = fmin(min[j], points[i][j]);
            max[j] = fmax(max[j], points[i][j]);
        }
    }

    halfwidths = (max - min) / 2.0f;
    center = min + halfwidths;    
}

void aabb::expand_to_fit(const aabb& other)
{
    glm::vec3 this_min = center - halfwidths;
    glm::vec3 this_max = center + halfwidths;

    glm::vec3 other_min = other.center - other.halfwidths;
    glm::vec3 other_max = other.center + other.halfwidths;

    for (int i = 0; i < 3; ++i)
    {
        this_min[i] = fmin(this_min[i], other_min[i]);
        this_max[i] = fmax(this_max[i], other_max[i]);
    }

    halfwidths = (this_max - this_min) / 2.0f;
    center = this_min + halfwidths;
}

obb::obb(glm::vec3 basis[3], glm::vec3 halfwidths)
    : center(0), obb_basis{basis[0], basis[1], basis[2]}, halfwidths(halfwidths)
{

}

obb::obb(glm::vec3 center, glm::vec3 basis[3], glm::vec3 halfwidths)
    : center(center), obb_basis{basis[0], basis[1], basis[2]},
      halfwidths(halfwidths)
{

}

obb::obb(aabb axis_aligned, glm::quat& rotation)
    : center(axis_aligned.center), halfwidths(axis_aligned.halfwidths)
{
    obb_basis[0] = rotation * vec3(1.f, 0.f, 0.f);
    obb_basis[1] = rotation * vec3(0.f, 1.f, 0.f);
    obb_basis[2] = rotation * vec3(0.f, 0.f, 1.f);    
}

frustum::frustum(float near, float far, float fov, float aspect_ratio)
    : m_near(near), m_far(far), m_fov(fov)
{
    m_projection = glm::perspective(glm::radians(fov), aspect_ratio, near, far);
    m_inverse_projection = glm::inverse(m_projection);

    // Init sides
    sides[LEFT] = plane(glm::vec3(m_projection[3][0] + m_projection[0][0],
                                  m_projection[3][1] + m_projection[0][1],
                                  m_projection[3][3] + m_projection[0][2]),
                        m_projection[3][3] + m_projection[0][3]);

    sides[RIGHT] = plane(glm::vec3(m_projection[3][0] - m_projection[0][0],
                                   m_projection[3][1] - m_projection[0][1],
                                   m_projection[3][3] - m_projection[0][2]),
                         m_projection[3][3] - m_projection[0][3]);

    sides[TOP] = plane(glm::vec3(m_projection[3][0] - m_projection[1][0],
                                 m_projection[3][1] - m_projection[1][1],
                                 m_projection[3][3] - m_projection[1][2]),
                       m_projection[3][3] - m_projection[1][3]);

    sides[BOTTOM] = plane(glm::vec3(m_projection[3][0] + m_projection[1][0],
                                    m_projection[3][1] + m_projection[1][1],
                                    m_projection[3][3] + m_projection[1][2]),
                          m_projection[3][3] + m_projection[1][3]);

    sides[NEAR] = plane(glm::vec3(m_projection[3][0] + m_projection[2][0],
                                  m_projection[3][1] + m_projection[2][1],
                                  m_projection[3][3] + m_projection[2][2]),
                        m_projection[3][3] + m_projection[2][3]);

    sides[FAR] = plane(glm::vec3(m_projection[3][0] - m_projection[2][0],
                                 m_projection[3][1] - m_projection[2][1],
                                 m_projection[3][3] - m_projection[2][2]),
                       m_projection[3][3] - m_projection[2][3]);

}

bool frustum::is_aabb_inside(aabb box)
{
    for (int i = 0; i < 6; ++i)
    {
        if (test_aabb_plane(box, sides[i]) == test_result::BELOW)
        {
            return false;
        }
    }
    return true;
}

bool frustum::is_obb_inside(obb box)
{
    for (int i = 0; i < 6; ++i)
    {
        if (test_obb_plane(box, sides[i]) == test_result::BELOW)
        {
            return false;
        }
    }
    return true;
}

vec3 frustum::send_ray_from_origin(float x_ndc, float y_ndc,
                                   mat4 view, vec4 viewport)
{
    // vec4 ndc_point(x_ndc, y_ndc, -1.0f, 1.0); // At the near edge of NDC space
    vec3 near_dc_point(x_ndc, y_ndc, 0.0f); // At the near edge of NDC space
    vec3 far_dc_point(x_ndc, y_ndc, 1.0f); // At the near edge of NDC space

    vec3 near_world_space =
        glm::unProject(near_dc_point, view, m_projection, viewport);
    vec3 far_world_space =
        glm::unProject(far_dc_point, view, m_projection, viewport);
    return far_world_space - near_world_space;
    // vec4 world_space_point = inverse_view * m_inverse_projection * ndc_point;

    // out_ray_dir = vec3(world_space_point) - view_pos;
}
