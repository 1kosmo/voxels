#pragma once

#include <SDL2/SDL.h>
#include <string>

enum class window_flags { INVISIBLE = 0x1, FULLSCREEN = 0x2, BORDERLESS = 0x4 };

enum class input_flags { QUIT = 1 };

class window
{
public: 
    int create_window(std::string window_name, int screen_width, int screen_height,
                      unsigned int current_flags);

    SDL_Window* get_sdl_window();
    void double_buffer();
    void close_window();

    void handle_input(int& flags);

protected:
    virtual void handle_key(int& flags, SDL_Event& event) = 0;
    virtual void handle_mouse_button(int&flags, SDL_Event& event) = 0;
    virtual void handle_mouse_move(int&flags, SDL_Event& event) = 0;
    bool m_shift_mod;
    bool m_alt_mod;
    bool m_ctrl_mod;

    SDL_Window* m_sdl_window;
    SDL_GLContext m_gl_context;
    int m_screen_width, m_screen_height; 
};
