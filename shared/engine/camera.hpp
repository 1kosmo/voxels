#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/constants.hpp>

class camera
{
public:
    camera();
    camera(glm::vec3 pos, glm::vec3 look_at);

    ~camera() = default;

    void relative_move(glm::vec3 delta);
    void pan_x(float delta);
    void pan_y(float delta);
    void pan_z(float delta);
    void set_new_target(glm::vec3 target);

    bool changed();
    bool changed_pos();
    glm::mat4 get_transform();
    glm::mat4 get_inverse_transform();
    glm::vec3 get_pos();

    glm::vec3 get_view_dir();
    glm::vec3 get_left_of_view_dir();
    glm::vec3 get_right_of_view_dir();

protected:
    glm::vec3 m_position;
    glm::vec4 m_camera_target;
    glm::vec3 m_look_at;
    glm::vec3 m_up;

    float m_x_rot;
    float m_y_rot;
    float m_z_rot;
    float m_max_x_rot = glm::half_pi<float>() - 0.02;
    // float m_max_y_rot = std::numeric_limits<float>::max();
    // float m_max_z_rot;    
    float m_min_x_rot = -glm::half_pi<float>() + 0.02;
    // float m_min_y_rot;
    // float m_min_z_rot;

    glm::quat m_rotation;
    void calculate_rotation();

    bool m_changed;
    bool m_pos_changed;
    bool m_no_rotation_calculation;
};
