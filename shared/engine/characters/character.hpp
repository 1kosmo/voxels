#pragma once

#include <ctime>
#include <unordered_map>
#include <vector>

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

#include "../utils/types.hpp"
#include "character_node.hpp"
#include "../bounding_volumes.hpp"

const glm::vec3 character_default_direction(0.0, 0.0, 1.0);
const float gg_epsilon = std::numeric_limits<float>::epsilon();
const float gravity_constant = 11.f;
const float min_fall_velocity = -20.f;

class character
{
public:
    character(glm::vec3 pos);
    character(glm::vec3 pos, engine::cube_id color);
    character(glm::vec3 pos, const character* other);
    ~character();

    std::vector<glm::vec3> perform_movement();

    bool is_updated();

    void get_changed_chunks(
        engine::cube_type_dict& type_dict, engine::changed_chunks& collection);

    void get_transformed_chunks(engine::transformed_chunks& collection);

    void set_position(glm::vec3& pos);
    void move_by_vec(glm::vec3 dir);
    void set_absolute_orientation(float a);
    void add_relative_orientation(float a);
    void revert_last_orientation();
    void set_cube_scale(float scale);
    void set_forward_acceleration(float a);
    void set_side_acceleration(float a);
    void set_up_velocity(float v);

    glm::vec3 get_facing_direction();
    glm::vec3 get_position();
    float get_up_velocity();
    aabb get_global_aabb();
    obb get_global_obb();

private:
    bool m_updated;    
    glm::vec3 m_global_position; // Center of the model
    glm::quat m_global_orientation; // 
    glm::vec3 m_bounding_halfwidths; // Halfwidths of model root bounding box
    float m_cube_scale;
    glm::vec3 get_offset_to_center();

    std::vector<engine::cube_id> m_data_to_cube_type;
    std::unordered_map<engine::cube_id, unsigned char> m_cube_type_to_data;

    // Identifier for each body part; Root node = 0;
    typedef unsigned int node_id;

    std::vector<character_node*> m_nodes;
    std::vector<children_list> m_children_lists;
    std::vector<engine::chunk_id> m_node_chunk_ids;
    void compute_transform_for(node_id node, glm::mat4& parent_transform,
                               engine::transformed_chunks& transform_collection);
    aabb compute_global_bounding_box_for(node_id node, glm::quat& parent_rotation);

    // Vars for animation/physical movement
    timespec m_last_recorded_time;
    float m_max_velocity = 5.0f;

    float m_forward_acceleration = 0.0f;
    float m_side_acceleration = 0.0f;

    float m_forward_velocity = 0.0f;
    float m_side_velocity = 0.0f;
    float m_up_velocity = 0.0f;

    float m_sec_for_full_rot = 1.0f;
    glm::quat m_target_orientation;
    glm::quat m_last_orientation;
};
