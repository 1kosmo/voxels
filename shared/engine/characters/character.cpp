#include <limits.h>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/constants.hpp>

#include "character.hpp"
#include "character_node.hpp"

character::character(glm::vec3 pos)
    : m_updated(true), m_global_position(pos), m_cube_scale(1.0f)
{
    glm::vec3 y_axis(0.0, 1.0, 0.0);
    float angle = 0.0;
    m_global_orientation = glm::angleAxis(angle, y_axis);

    character_node* root_node = new character_node;
    m_nodes.push_back(root_node);
    m_children_lists.resize(1);
    m_node_chunk_ids.push_back(engine::id_generator::get().chunk());

    root_node->set_cube(1, 0, 0, 0); // Cube of type 1, whatever that is

    // Register that 'type 1' cube so its legit
    m_data_to_cube_type.push_back(1);
    m_cube_type_to_data[1] = 1;
}

character::character(glm::vec3 pos, engine::cube_id color)
    : m_updated(true), m_global_position(pos), m_cube_scale(1.0f)
{
    glm::vec3 y_axis(0.0, 1.0, 0.0);
    float angle = 0.0;
    m_global_orientation = glm::angleAxis(angle, y_axis);

    character_node* root_node = new character_node;
    m_nodes.push_back(root_node);
    m_children_lists.resize(1);
    m_node_chunk_ids.push_back(engine::id_generator::get().chunk());

    root_node->set_cube(1, 0, 0, 0); // Cube of type 1, whatever that is

    // Register that 'type 1' cube so its legit
    m_data_to_cube_type.push_back(color);
    m_cube_type_to_data[color] = 1;
}

character::character(glm::vec3 pos, const character* other)
    : m_updated(true), m_global_position(pos), m_cube_scale(1.0f)
{
    glm::vec3 y_axis(0.0, 1.0, 0.0);
    float angle = 0.0;
    m_global_orientation = glm::angleAxis(angle, y_axis);
    // TODO : Fill this function
}

bool character::is_updated()
{
    return m_updated;
}

void character::set_position(glm::vec3& pos)
{
    m_global_position = pos;
    m_updated = true;
}

void character::move_by_vec(glm::vec3 dir)
{
    m_global_position += dir;
    m_updated = true;
}

void character::set_absolute_orientation(float a)
{
    float angle = glm::radians(a);
    glm::vec3 y_axis(0.0f, 1.0f, 0.0f);
    m_global_orientation = glm::angleAxis(angle, y_axis);
    m_updated = true;
}

void character::add_relative_orientation(float a)
{
    float angle = glm::radians(a);
    glm::vec3 y_axis(0.0f, 1.0f, 0.0f);
    m_target_orientation = glm::rotate(m_global_orientation, angle, y_axis);
}

// This function does not mark updated because it gets called right after
//  global orientation gets updated, at "move_characters" in the manager,
//  so a revert to last state is not an update
void character::revert_last_orientation()
{
    m_global_orientation = m_last_orientation;
}

void character::set_cube_scale(float scale)
{
    m_cube_scale = scale;
    m_updated = true;
}

glm::vec3 character::get_facing_direction()
{
    return m_global_orientation * character_default_direction;
}

glm::vec3 character::get_position()
{
    return m_global_position;
}

glm::vec3 character::get_offset_to_center()
{
    character_node* root = m_nodes[0];
    float offset = (root->m_scale * m_cube_scale) / 2.0f;
    return glm::vec3((root->m_xdim % 2) ? -offset : 0.0f,
                     (root->m_ydim % 2) ? -offset : 0.0f,
                     (root->m_zdim % 2) ? -offset : 0.0f);
}

void character::get_changed_chunks(
    engine::cube_type_dict& type_dict, engine::changed_chunks& collection)
{
    int num_nodes = m_nodes.size();
    for (int i = 0; i < num_nodes; ++i)
    {
        character_node* node = m_nodes[i];
        if (node != NULL && node->m_updated)
        {
            collection.emplace_back(
                m_node_chunk_ids[i],
                node->generate_mesh(m_data_to_cube_type, type_dict));
            node->m_updated = false;
        }
    }
    // We do not compute mesh transforms here, because those will be handled by
    // get_transformed_chunks()
}

void character::get_transformed_chunks(engine::transformed_chunks& collection)
{
    if (!m_updated)
    {
        return;
    }

    // Compute transforms starting from the root node
    glm::vec3 offset_to_center = get_offset_to_center();

    glm::mat4 general_transform(1.0);
    general_transform =
        glm::translate(general_transform, m_global_position) *
        glm::mat4_cast(m_global_orientation) *
        glm::translate(general_transform, offset_to_center) * 
        glm::scale(general_transform, glm::vec3(m_cube_scale));

    compute_transform_for(0, general_transform, collection);

    m_updated = false;
}

void character::compute_transform_for(
    node_id node, glm::mat4& parent_transform,
    engine::transformed_chunks& transform_collection)
{
    character_node* n = m_nodes[node];
    if (n == NULL)
    {
        return; 
    }

    glm::mat4 identity(1.0f);
    glm::mat4 translate = glm::translate(identity, n->m_offset);
    glm::mat4 scale = glm::scale(identity, glm::vec3(n->m_scale));

    // Child transform does not include scale
    glm::mat4 child_transform =
        parent_transform * translate * mat4_cast(n->m_rotation);

    transform_collection.push_back(
        std::make_pair(m_node_chunk_ids[node], child_transform * scale));

    for (node_id child : m_children_lists[node])
    {
        compute_transform_for(child, child_transform, transform_collection);
    }
}

aabb character::compute_global_bounding_box_for(node_id node,
                                                glm::quat& parent_rotation)
{
    character_node* n = m_nodes[node];
    obb my_obb = n->compute_obb(parent_rotation);
    aabb my_aabb(my_obb);

    glm::quat total_rotation = n->m_rotation * parent_rotation;

    for (node_id child : m_children_lists[node])
    {
        character_node* c = m_nodes[child];
        if (c == NULL) { continue; }
        aabb child_aabb = compute_global_bounding_box_for(child, total_rotation);
        child_aabb.center += c->m_offset;

        my_aabb.expand_to_fit(child_aabb);
    }

    return my_aabb;
}

aabb character::get_global_aabb()
{
    aabb box = compute_global_bounding_box_for(0, m_global_orientation);
    // box.center = m_global_position + get_offset_to_center();
    box.center = m_global_position;
    return box;
}

obb character::get_global_obb()
{
    glm::quat no_rot(1.f, 0.f, 0.f, 0.f);
    aabb axis_aligned = compute_global_bounding_box_for(0, no_rot);
    axis_aligned.center = m_global_position + get_offset_to_center();
    return obb(axis_aligned, m_global_orientation);
}

// Returns difference in time in seconds (t1 - t2)
// Assumes t1 is later in time than t2
static float time_diff(timespec& t1, timespec& t2)
{
    float diff;
    if (t1.tv_sec == t2.tv_sec)
    {
        diff = ((float)(t1.tv_nsec - t2.tv_nsec)) / 1000000000.0;
    }
    else
    {
        diff = ((float)t2.tv_nsec) / 1000000000.0 +
            ((float)(1000000000 - t1.tv_nsec)) / 1000000000.0 +
            (float)(t1.tv_sec - t2.tv_sec);
    }
    return diff;
}

std::vector<glm::vec3> character::perform_movement()
{
    timespec now;
    clock_gettime(CLOCK_REALTIME, &now);

    float diff = time_diff(now, m_last_recorded_time);
    m_last_recorded_time = now;

    std::vector<glm::vec3> movements;

    if (diff > 0.1)
    {
        return movements;
    }

    // Orientation
    // TODO : Make orientation time based 
    m_last_orientation = m_global_orientation;
    // float angle_delta = glm::yaw(m_target_orientation) -
    //     glm::yaw(m_global_orientation);
    // float max_angle = glm::pi<float>() * 2.f * diff / m_sec_for_full_rot;
    // if (fabs(angle_delta) < max_angle)
    // {
    m_global_orientation = m_target_orientation;
    // }
    // else
    // {
    //     max_angle *= ((angle_delta < 0.f) ? -1.f : 1.f);
    //     m_global_orientation = glm::rotate(m_global_orientation, max_angle,
    //                                        glm::vec3(0.f, 1.f, 0.f));
    // }

    // Y plane
    m_up_velocity -= diff * gravity_constant;
    m_up_velocity = fmax(m_up_velocity, min_fall_velocity);
    movements.push_back(diff * m_up_velocity * glm::vec3(0.f, 1.f, 0.f));

    // X Z planes
    m_forward_velocity += diff * m_forward_acceleration;
    m_side_velocity += diff * m_side_acceleration;

    m_forward_velocity = fmax(fmin(m_forward_velocity, m_max_velocity), -m_max_velocity);
    m_side_velocity = fmax(fmin(m_side_velocity, m_max_velocity), -m_max_velocity);
        
    glm::vec3 forward =
        glm::normalize(m_global_orientation * character_default_direction);

    if (fabs(m_forward_velocity) > gg_epsilon)
    {
        // m_updated = true;
        // m_global_position += m_forward_velocity * forward * diff;
        movements.push_back(m_forward_velocity * forward * diff);
    }

    if (fabs(m_side_velocity) > gg_epsilon)
    {
        // m_updated = true;
        glm::vec3 side =
            glm::normalize(glm::cross(forward, glm::vec3(0.0f, 1.0f, 0.0f)));
        // m_global_position += m_side_velocity * side * diff;
        movements.push_back(m_side_velocity * side * diff);
    }
}

void character::set_forward_acceleration(float a)
{
    m_forward_acceleration = a;
    if (fabs(m_forward_acceleration) <= gg_epsilon)
    {
        m_forward_velocity = 0.0;
    }
}

void character::set_side_acceleration(float a)
{
    m_side_acceleration = a;
    if (fabs(m_side_acceleration) <= gg_epsilon)
    {
        m_side_velocity = 0.0;
    }
}

void character::set_up_velocity(float v)
{
    m_up_velocity = v;
}

float character::get_up_velocity()
{
    return m_up_velocity;
}
