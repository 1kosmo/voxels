#pragma once

#include <unordered_map>

#include "../utils/types.hpp"
#include "../transforms.hpp"
#include "../bounding_volumes.hpp"

class character;
class environment_manager;
class audio_system;

typedef
std::vector<std::pair<engine::character_id, engine::character_id> >
character_collisions;

class character_manager
{
public:
    void init(audio_system* audio);

    engine::changed_chunks get_changed_chunks(engine::cube_type_dict& type_dict);
    engine::transformed_chunks get_transformed_chunks();

    engine::character_id spawn_character(std::string name, glm::vec3 pos);
    engine::character_id spawn_cube(glm::vec3 pos);
    engine::character_id spawn_cube_with_color(glm::vec3 pos, engine::cube_id c);

    character_collisions move_characters(environment_manager& environment);
    void set_character_forward_acceleration(engine::character_id c, float a);
    void set_character_side_acceleration(engine::character_id c, float a);
    void set_character_absolute_orientation(engine::character_id c, float a);
    void add_character_relative_orientation(engine::character_id c, float a);

    void make_character_jump(engine::character_id c, float up_velocity);

    // Character specific functions
    void set_character_position(engine::character_id c, glm::vec3& pos);
    void set_character_cube_scale(engine::character_id c, float scale);
    glm::vec3 get_character_facing_direction(engine::character_id c);
    glm::vec3 get_character_position(engine::character_id c);

    aabb get_character_bounding_aabb(engine::character_id c);

private:
    std::unordered_map<std::string, character*> m_defined_models;
    std::unordered_map<engine::character_id, character*> m_active_characters;

    int test_collision(aabb& box, environment_manager& environment);
    void test_collision_and_move(
        aabb& box, environment_manager& environment);

    audio_system* m_audio_system;
};
