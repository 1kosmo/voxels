#pragma once

#include "../camera.hpp"
#include "character_manager.hpp"

class camera_3p : public camera
{
public:
    camera_3p();

    void set_tracking_distance(float dist);
    void track_character(
        engine::character_id character, character_manager& manager);
    void track_point(glm::vec3 look_at, glm::vec3 point_orientation_vec);

private:
    float m_tracking_distance;
};
