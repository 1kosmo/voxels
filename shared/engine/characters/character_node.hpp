#pragma once

#include <unordered_map>
#include <vector>

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

#include "../utils/types.hpp"
#include "../environment/chunk.hpp"
#include "../transforms.hpp"
#include "../bounding_volumes.hpp"

struct character_node
{
    bool m_is_joint;
    bool m_updated;

    glm::ivec3 m_min_dimensions;
    glm::ivec3 m_max_dimensions;
    unsigned int m_xdim, m_ydim, m_zdim; // TODO : Remove these, they are probably useless
    unsigned int m_num_cubes;

    // std::vector<unsigned char> m_data;
    std::unordered_map<glm::ivec3, unsigned char, ivec3_hash> m_data;

    character_node();
    chunk_mesh_ptr generate_mesh(std::vector<engine::cube_id>& data_to_cube_type,
                                 engine::cube_type_dict& types);
    void set_cube(unsigned char type, glm::ivec3 pos);
    void set_cube(unsigned char type, int x, int y, int z);

    // size_t get_node_index_at_pos(int x, int y, int z);
    // size_t get_node_index_at_pos(glm::ivec3& pos);

    // Transformation Info
    glm::vec3 m_offset;
    glm::quat m_rotation;
    float m_scale;

    obb compute_obb(glm::quat parent_rotation);

private:
    void update_dimensions_if_required(int x, int y, int z);
};

// struct child_node_info
// {
//     node_id m_id;
// };

// Typedefs
typedef unsigned int node_id;
typedef std::vector<node_id> children_list;
