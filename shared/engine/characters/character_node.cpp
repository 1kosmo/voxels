#include "character_node.hpp"

character_node::character_node()
    : m_min_dimensions(0), m_max_dimensions(0),
      m_xdim(0), m_ydim(0), m_zdim(0), m_num_cubes(0),
      m_offset(0.0), m_scale(1.0)
{
    float angle = 0.0;
    glm::vec3 axis(1.0, 0.0, 0.0);
    m_rotation = glm::angleAxis(angle, axis);
}

void character_node::set_cube(unsigned char type, glm::ivec3 pos)
{
    update_dimensions_if_required(pos.x, pos.y, pos.z);
    m_data[pos] = type;
    m_updated = true;
}

void character_node::set_cube(unsigned char type, int x, int y, int z)
{
    update_dimensions_if_required(x, y, z);
    // m_data[get_node_index_at_pos(x, y, z)] = type;
    m_data[glm::ivec3(x, y, z)] = type;
    m_updated = true;
}

obb character_node::compute_obb(glm::quat parent_rotation)
{
    glm::vec3 halfwidths((float)m_xdim / 2.0f,
                         (float)m_ydim / 2.0f,
                         (float)m_zdim / 2.0f);
    glm::quat abs_rotation = m_rotation * parent_rotation;
    glm::vec3 basis[3] = {abs_rotation * glm::vec3(1.f, 0.f, 0.f),
                          abs_rotation * glm::vec3(0.f, 1.f, 0.f),
                          abs_rotation * glm::vec3(0.f, 0.f, 1.f)};
    return obb((glm::vec3(m_max_dimensions) - halfwidths) * m_scale,
               basis, halfwidths * m_scale);    
}

chunk_mesh_ptr character_node::generate_mesh(
    std::vector<engine::cube_id>& data_to_cube_type, engine::cube_type_dict& types)
{
    chunk_mesh_ptr mesh(new chunk_mesh);
    mesh->m_num_quads = 0;

    for (const std::pair<glm::ivec3, unsigned char>& cube : m_data)
    {
        unsigned char type = cube.second;
        if (type == 0)
        {
            continue;
        }
        glm::vec3 color = types[data_to_cube_type[type - 1]];

        int x = cube.first.x;
        int y = cube.first.y;
        int z = cube.first.z;        

        // Check above
        if (m_data.count(glm::ivec3(x, y + 1, z)) == 0)
        {
            mesh->m_vertices.emplace_back(x, y + 1, z + 1);
            mesh->m_vertices.emplace_back(x + 1, y + 1, z + 1);
            mesh->m_vertices.emplace_back(x + 1, y + 1, z);
            mesh->m_vertices.emplace_back(x, y + 1, z);

            mesh->m_normals.emplace_back(0, 1, 0);
            mesh->m_normals.emplace_back(0, 1, 0);
            mesh->m_normals.emplace_back(0, 1, 0);
            mesh->m_normals.emplace_back(0, 1, 0);

            mesh->m_colors.push_back(color);
            mesh->m_colors.push_back(color);
            mesh->m_colors.push_back(color);
            mesh->m_colors.push_back(color);

            mesh->m_num_quads += 1;
        }
        // Check below
        if (m_data.count(glm::ivec3(x, y - 1, z)) == 0)
        {
            mesh->m_vertices.emplace_back(x, y, z);
            mesh->m_vertices.emplace_back(x + 1, y, z);
            mesh->m_vertices.emplace_back(x + 1, y, z + 1);
            mesh->m_vertices.emplace_back(x, y, z + 1);

            mesh->m_normals.emplace_back(0, -1, 0);
            mesh->m_normals.emplace_back(0, -1, 0);
            mesh->m_normals.emplace_back(0, -1, 0);
            mesh->m_normals.emplace_back(0, -1, 0);

            mesh->m_colors.push_back(color);
            mesh->m_colors.push_back(color);
            mesh->m_colors.push_back(color);
            mesh->m_colors.push_back(color);
                
            mesh->m_num_quads += 1;
        }

        // Check front
        if (m_data.count(glm::ivec3(x, y, z - 1)) == 0)
        {
            mesh->m_vertices.emplace_back(x, y + 1, z);
            mesh->m_vertices.emplace_back(x + 1, y + 1, z);
            mesh->m_vertices.emplace_back(x + 1, y, z);
            mesh->m_vertices.emplace_back(x, y, z);

            mesh->m_normals.emplace_back(0, 0, -1);
            mesh->m_normals.emplace_back(0, 0, -1);
            mesh->m_normals.emplace_back(0, 0, -1);
            mesh->m_normals.emplace_back(0, 0, -1);

            mesh->m_colors.push_back(color);
            mesh->m_colors.push_back(color);
            mesh->m_colors.push_back(color);
            mesh->m_colors.push_back(color);
                
            mesh->m_num_quads += 1;
        }
        // Check back
        if (m_data.count(glm::ivec3(x, y, z + 1)) == 0)
        {
            mesh->m_vertices.emplace_back(x + 1, y + 1, z + 1);
            mesh->m_vertices.emplace_back(x, y + 1, z + 1);
            mesh->m_vertices.emplace_back(x, y, z + 1);
            mesh->m_vertices.emplace_back(x + 1, y, z + 1);

            mesh->m_normals.emplace_back(0, 0, 1);
            mesh->m_normals.emplace_back(0, 0, 1);
            mesh->m_normals.emplace_back(0, 0, 1);
            mesh->m_normals.emplace_back(0, 0, 1);

            mesh->m_colors.push_back(color);
            mesh->m_colors.push_back(color);
            mesh->m_colors.push_back(color);
            mesh->m_colors.push_back(color);
                
            mesh->m_num_quads += 1;
        }
        // Check left
        if (m_data.count(glm::ivec3(x - 1, y, z)) == 0)
        {
            mesh->m_vertices.emplace_back(x, y + 1, z + 1);
            mesh->m_vertices.emplace_back(x, y + 1, z);
            mesh->m_vertices.emplace_back(x, y, z);
            mesh->m_vertices.emplace_back(x, y, z + 1);

            mesh->m_normals.emplace_back(-1, 0, 0);
            mesh->m_normals.emplace_back(-1, 0, 0);
            mesh->m_normals.emplace_back(-1, 0, 0);
            mesh->m_normals.emplace_back(-1, 0, 0);

            mesh->m_colors.push_back(color);
            mesh->m_colors.push_back(color);
            mesh->m_colors.push_back(color);
            mesh->m_colors.push_back(color);
                
            mesh->m_num_quads += 1;
        }
        // Check right
        if (m_data.count(glm::ivec3(x + 1, y, z)) == 0)
        {
            mesh->m_vertices.emplace_back(x + 1, y + 1, z);
            mesh->m_vertices.emplace_back(x + 1, y + 1, z + 1);
            mesh->m_vertices.emplace_back(x + 1, y, z + 1);
            mesh->m_vertices.emplace_back(x + 1, y, z);

            mesh->m_normals.emplace_back(1, 0, 0);
            mesh->m_normals.emplace_back(1, 0, 0);
            mesh->m_normals.emplace_back(1, 0, 0);
            mesh->m_normals.emplace_back(1, 0, 0);

            mesh->m_colors.push_back(color);
            mesh->m_colors.push_back(color);
            mesh->m_colors.push_back(color);
            mesh->m_colors.push_back(color);
                
            mesh->m_num_quads += 1;
        }        
    }

    return mesh;
}

//////////////////// Helper functions ////////////////////

// size_t character_node::get_node_index_at_pos(int x, int y, int z)
// {
//     x += -m_min_dimensions.x;
//     y += -m_min_dimensions.y;
//     z += -m_min_dimensions.z;    
//     return (x + m_ydim * (y + m_zdim * z));    
// }

// size_t character_node::get_node_index_at_pos(glm::ivec3& pos)
// {
//     return get_node_index_at_pos(x, y, z);
// }

void character_node::update_dimensions_if_required(int x, int y, int z)
{
    if (x < m_min_dimensions.x)
    {
        m_min_dimensions.x -= 1;
        m_xdim += 1;
    }
    else if (x >= m_max_dimensions.x)
    {
        m_min_dimensions.x += 1;
        m_xdim += 1;
    }

    if (y < m_min_dimensions.y)
    {
        m_min_dimensions.y -= 1;
        m_ydim += 1;
    }
    else if (y >= m_max_dimensions.y)
    {
        m_min_dimensions.y += 1;
        m_ydim += 1;
    }

    if (z < m_min_dimensions.z)
    {
        m_min_dimensions.z -= 1;
        m_zdim += 1;
    }
    else if (z >= m_max_dimensions.z)
    {
        m_min_dimensions.z += 1;
        m_zdim += 1;
    }
}
