#include <glm/gtc/matrix_transform.hpp>

#include "camera_3p.hpp"

camera_3p::camera_3p()
    : camera(), m_tracking_distance(10.0f)
{
    m_no_rotation_calculation = true;
    m_x_rot = glm::radians(45.0f);
    m_min_x_rot = glm::radians(10.0f);
    m_max_x_rot = glm::radians(85.0f);
}

void camera_3p::set_tracking_distance(float dist)
{
    m_tracking_distance = dist;
}

void camera_3p::track_character(
    engine::character_id character, character_manager& manager)
{
    m_rotation = glm::angleAxis(m_y_rot, glm::vec3(0.0f, 1.0f, 0.0f));

    glm::vec3 char_dir =
        glm::normalize(manager.get_character_facing_direction(character));
    m_rotation = glm::rotate(m_rotation, -m_x_rot,
                             glm::cross(char_dir, glm::vec3(0.0f, 1.0f, 0.0f)));

    m_look_at = manager.get_character_position(character);
    m_position = m_look_at - m_rotation * char_dir * m_tracking_distance;

    m_changed = true;
    m_pos_changed = true;
}

// TODO : Move this and above calculation into a better place .
//        Right now, it MUST be run at all times, which isn't great
void camera_3p::track_point(glm::vec3 look_at, glm::vec3 point_orientation_vec)
{
    m_rotation = glm::angleAxis(m_y_rot, glm::vec3(0.0f, 1.0f, 0.0f));

    m_rotation = glm::rotate(m_rotation, -m_x_rot,
                             glm::cross(point_orientation_vec, glm::vec3(0.0f, 1.0f, 0.0f)));

    m_look_at = look_at;
    m_position = m_look_at - m_rotation * point_orientation_vec * m_tracking_distance;

    m_changed = true;
    m_pos_changed = true;    
}
