#include <glm/gtx/string_cast.hpp>

#include "character_manager.hpp"
#include "character.hpp"
#include "../msg_log.hpp"
#include "../environment/environment_manager.hpp"
#include "../intersection.hpp"
#include "../audio_system.hpp"

void character_manager::init(audio_system* audio)
{
    m_audio_system = audio;
}

engine::changed_chunks character_manager::get_changed_chunks(
    engine::cube_type_dict& type_dict)
{
    engine::changed_chunks chunks;

    for (auto& character : m_active_characters)
    {
        character.second->get_changed_chunks(type_dict, chunks);
    }

    return chunks;
}

engine::transformed_chunks character_manager::get_transformed_chunks()
{
    engine::transformed_chunks transforms;

    for (auto& character : m_active_characters)
    {
        character.second->get_transformed_chunks(transforms);
    }

    return transforms;
}

engine::character_id character_manager::spawn_character(
    std::string name, glm::vec3 pos)
{
    if (m_defined_models.count(name) == 0)
    {
        return engine::id_generator::get().invalid_character();
    }

    engine::character_id id = engine::id_generator::get().character();
    m_active_characters[id] = new character(pos, m_defined_models[name]);

    return id;
}

engine::character_id character_manager::spawn_cube(glm::vec3 pos)
{
    engine::character_id id = engine::id_generator::get().character();
    m_active_characters[id] = new character(pos);

    return id;
}

engine::character_id character_manager::spawn_cube_with_color(glm::vec3 pos,
                                                              engine::cube_id c)
{
    engine::character_id id = engine::id_generator::get().character();
    m_active_characters[id] = new character(pos, c);

    return id;
}

character_collisions character_manager::move_characters(
    environment_manager& environment)
{
    std::vector<aabb> bounding_boxes;
    std::vector<glm::vec3> char_movements;
    std::vector<std::pair<engine::character_id, character*> >chars;

    // Static collisions
    for (const std::pair<engine::character_id, character*>& c : m_active_characters)
    {
        std::vector<glm::vec3> movements = c.second->perform_movement();
        int num_moves = movements.size();
        if (num_moves == 0)
        {
            continue;
        }

        aabb bounding_box = c.second->get_global_aabb();
        if (test_collision(bounding_box, environment))
        {
            c.second->revert_last_orientation();
            bounding_box = c.second->get_global_aabb();
        }
        // test_collision_and_move(bounding_box, environment);

        glm::vec3 original_pos = bounding_box.center;
        glm::vec3 accumulated_move = glm::vec3(0.f);

        // Manually resolve down movement, which is always the first in vector
        bounding_box.center += movements[0];
        if (test_collision(bounding_box, environment))
        {
            if (c.second->get_up_velocity() < (min_fall_velocity * 0.25))
            {
                m_audio_system->PlaySound("../assets/effect_bounce.wav");
            }
            c.second->set_up_velocity(0.0);
        }
        else
        {
            // original_pos = bounding_box.center;
            accumulated_move += movements[0];
            // if (c.second->get_up_velocity() < (min_fall_velocity * 0.75) &&
            //     !m_audio_system->IsPlaying("../assets/effect_wind.wav"))
            // {
            //     m_audio_system->PlaySound("../assets/effect_wind.wav");
            // }
        }

        for (int i = 1; i < num_moves; ++i)
        {
            bounding_box.center = original_pos + movements[i];
            if (0 == test_collision(bounding_box, environment))
            {
                accumulated_move += movements[i];
            }
        }
        bounding_box.center = original_pos;
        // glm::vec3 final_pos = original_pos + accumulated_move;
        // c.second->set_position(final_pos);
        bounding_boxes.push_back(bounding_box);
        char_movements.push_back(accumulated_move);
        chars.push_back(c);
    }

    // Dynamic Collisions    
    character_collisions collisions;
    int num_chars = chars.size();
    for (int i = 0; i < num_chars; ++i)
    {
        for (int j = 0; j < num_chars; ++j)
        {
            if (j == i) { continue; }

            float tfirst, tlast;
            if (test_moving_aabb_aabb(bounding_boxes[i], bounding_boxes[j],
                                      char_movements[i], char_movements[j],
                                      tfirst, tlast) == test_result::TRUE)
            {
                collisions.emplace_back(chars[i].first, chars[j].first);

                aabb moved_i = bounding_boxes[i];
                moved_i.center += char_movements[i];
                aabb moved_j = bounding_boxes[j];
                moved_j.center += char_movements[j];

                float penetration;
                glm::vec3 rebound_dir;
                if (test_aabb_aabb(moved_i, moved_j, rebound_dir, penetration) ==
                    test_result::TRUE)
                {
                    // INFO("Rebounding in direction %s with penetration %f\n",
                    //      glm::to_string(rebound_dir).c_str(), penetration);
                    char_movements[i] = rebound_dir * penetration * 0.5f;
                    char_movements[j] = -rebound_dir * penetration * 0.5f;
                }
            }
        }
    }

    // Apply the movements
    for (int i = 0; i < num_chars; ++i)
    {
        glm::vec3 new_pos = bounding_boxes[i].center + char_movements[i];
        chars[i].second->set_position(new_pos);
    }
    
    return collisions;
}

int character_manager::test_collision(aabb& box, environment_manager& environment)
{
    std::vector<aabb> intersect_with;

    environment.get_relevant_cubes_for_intersection(box, intersect_with);

    for (aabb& voxel : intersect_with)
    {
        if (test_aabb_aabb(box, voxel) == test_result::TRUE)
        {
            return 1;
        }
    }

    return 0;
}

void character_manager::test_collision_and_move(
    aabb& box, environment_manager& environment)
{
    std::vector<aabb> intersect_with;

    environment.get_relevant_cubes_for_intersection(box, intersect_with);

    for (aabb& voxel : intersect_with)
    {
        if (test_aabb_aabb(box, voxel) == test_result::TRUE)
        {
            glm::vec3 to_char_dir = box.center - voxel.center;
            glm::vec3 norm_dir = glm::normalize(to_char_dir);
            to_char_dir = to_char_dir - box.halfwidths * norm_dir
                - voxel.halfwidths * norm_dir;
            // to_char_dir = -to_char_dir;
            to_char_dir.z = 0.f;

            // c->move_by_vec(to_char_dir);
            box.center += to_char_dir;
        }
    }
}

void character_manager::set_character_forward_acceleration(
    engine::character_id c, float a)
{
    m_active_characters[c]->set_forward_acceleration(a);
}

void character_manager::set_character_side_acceleration(
    engine::character_id c, float a)
{
    m_active_characters[c]->set_side_acceleration(a);
}

void character_manager::set_character_absolute_orientation(
    engine::character_id c, float a)
{
    m_active_characters[c]->set_absolute_orientation(a);    
}

void character_manager::add_character_relative_orientation(
    engine::character_id c, float a)
{
    m_active_characters[c]->add_relative_orientation(a);
}

void character_manager::set_character_position(
    engine::character_id c, glm::vec3& pos)
{
    if (m_active_characters.count(c) == 0)
    {
        ERROR("Tried to manipulate character with ID '%u' which doesn't exist", c);
        return;
    }
    m_active_characters[c]->set_position(pos);
}

void character_manager::set_character_cube_scale(
    engine::character_id c, float scale)
{
    if (m_active_characters.count(c) == 0)
    {
        ERROR("Tried to manipulate character with ID '%u' which doesn't exist", c);
        return;
    }
    m_active_characters[c]->set_cube_scale(scale);
}

void character_manager::make_character_jump(engine::character_id c,
                                            float up_velocity)
{
    character* ch = m_active_characters[c];
    if (ch->get_up_velocity() == 0.f)
    {
        m_audio_system->PlaySound("../assets/effect_jump.wav");        
        ch->set_up_velocity(up_velocity);
    }
}

glm::vec3 character_manager::get_character_facing_direction(engine::character_id c)
{
    if (m_active_characters.count(c) == 0)
    {
        ERROR("Tried to manipulate character with ID '%u' which doesn't exist", c);
        return glm::vec3(0.0);
    }
    return m_active_characters[c]->get_facing_direction();
}

glm::vec3 character_manager::get_character_position(engine::character_id c)
{
    if (m_active_characters.count(c) == 0)
    {
        ERROR("Tried to manipulate character with ID '%u' which doesn't exist", c);
        return glm::vec3(0.0);
    }
    return m_active_characters[c]->get_position();
}

aabb character_manager::get_character_bounding_aabb(engine::character_id c)
{
    return m_active_characters[c]->get_global_aabb();
}
