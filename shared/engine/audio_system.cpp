#include "audio_system.hpp"

using namespace std;
using namespace glm;

Implementation::Implementation() {
    mpStudioSystem = NULL;
    audio_system::ErrorCheck(FMOD::Studio::System::create(&mpStudioSystem));
    audio_system::ErrorCheck(mpStudioSystem->initialize(32, FMOD_STUDIO_INIT_LIVEUPDATE, FMOD_INIT_PROFILE_ENABLE, NULL));

    mpSystem = NULL;
    audio_system::ErrorCheck(mpStudioSystem->getLowLevelSystem(&mpSystem));
}

Implementation::~Implementation() {
    audio_system::ErrorCheck(mpStudioSystem->unloadAll());
    audio_system::ErrorCheck(mpStudioSystem->release());
}

void Implementation::Update() {
    vector<ChannelMap::iterator> pStoppedChannels;
    for (auto it = mChannels.begin(), itEnd = mChannels.end(); it != itEnd; ++it)
    {
        bool bIsPlaying = false;
        it->second->isPlaying(&bIsPlaying);
        if (!bIsPlaying)
        {
            pStoppedChannels.push_back(it);
        }
    }
    for (auto& it : pStoppedChannels)
    {
        mChannels.erase(it);
    }
    audio_system::ErrorCheck(mpStudioSystem->update());
}

Implementation* sgpImplementation = nullptr;

void audio_system::Init() {
    sgpImplementation = new Implementation;
}

void audio_system::Update() {
    sgpImplementation->Update();
}

void audio_system::LoadSound(const std::string& strSoundName, bool b3d,
                             bool bLooping, bool bStream)
{
    auto tFoundIt = sgpImplementation->mSounds.find(strSoundName);
    if (tFoundIt != sgpImplementation->mSounds.end())
        return;
    
    FMOD_MODE eMode = FMOD_DEFAULT;
    eMode |= b3d ? FMOD_3D : FMOD_2D;
    eMode |= bLooping ? FMOD_LOOP_NORMAL : FMOD_LOOP_OFF;
    eMode |= bStream ? FMOD_CREATESTREAM : FMOD_CREATECOMPRESSEDSAMPLE;
    
    FMOD::Sound* pSound = nullptr;
    audio_system::ErrorCheck(sgpImplementation->mpSystem->createSound(strSoundName.c_str(), eMode, nullptr, &pSound));
    if (pSound){
        sgpImplementation->mSounds[strSoundName] = pSound;
    }

}

void audio_system::UnLoadSound(const std::string& strSoundName)
{
    auto tFoundIt = sgpImplementation->mSounds.find(strSoundName);
    if (tFoundIt == sgpImplementation->mSounds.end())
        return;
    
    audio_system::ErrorCheck(tFoundIt->second->release());
    sgpImplementation->mSounds.erase(tFoundIt);
}

int audio_system::PlaySound(const string& strSoundName, const vec3& vPosition,
                            float fVolumedB)
{
    int nChannelId = sgpImplementation->mnNextChannelId++;
    auto tFoundIt = sgpImplementation->mSounds.find(strSoundName);
    if (tFoundIt == sgpImplementation->mSounds.end())
    {
        LoadSound(strSoundName);
        tFoundIt = sgpImplementation->mSounds.find(strSoundName);
        if (tFoundIt == sgpImplementation->mSounds.end())
        {
            return nChannelId;
        }
    }
    FMOD::Channel* pChannel = nullptr;
    audio_system::ErrorCheck(sgpImplementation->mpSystem->playSound(tFoundIt->second, nullptr, true, &pChannel));
    if (pChannel)
    {
        FMOD_MODE currMode;
        tFoundIt->second->getMode(&currMode);
        if (currMode & FMOD_3D){
            FMOD_VECTOR position = VectorToFmod(vPosition);
            audio_system::ErrorCheck(pChannel->set3DAttributes(&position, nullptr));
        }
        audio_system::ErrorCheck(pChannel->setVolume(dbToVolume(fVolumedB)));
        audio_system::ErrorCheck(pChannel->setPaused(false));
        sgpImplementation->mChannels[nChannelId] = pChannel;
    }
    return nChannelId;
}

void audio_system::SetChannel3dPosition(int nChannelId, const vec3& vPosition)
{
    auto tFoundIt = sgpImplementation->mChannels.find(nChannelId);
    if (tFoundIt == sgpImplementation->mChannels.end())
        return;

    FMOD_VECTOR position = VectorToFmod(vPosition);
    audio_system::ErrorCheck(tFoundIt->second->set3DAttributes(&position, NULL));
}

void audio_system::SetChannelVolume(int nChannelId, float fVolumedB)
{
    auto tFoundIt = sgpImplementation->mChannels.find(nChannelId);
    if (tFoundIt == sgpImplementation->mChannels.end())
        return;

    audio_system::ErrorCheck(tFoundIt->second->setVolume(dbToVolume(fVolumedB)));
}

void audio_system::LoadBank(const std::string& strBankName, FMOD_STUDIO_LOAD_BANK_FLAGS flags) {
    auto tFoundIt = sgpImplementation->mBanks.find(strBankName);
    if (tFoundIt != sgpImplementation->mBanks.end())
        return;
    FMOD::Studio::Bank* pBank;
    audio_system::ErrorCheck(sgpImplementation->mpStudioSystem->loadBankFile(strBankName.c_str(), flags, &pBank));
    if (pBank) {
        sgpImplementation->mBanks[strBankName] = pBank;
    }
}

void audio_system::LoadEvent(const std::string& strEventName) {
    auto tFoundit = sgpImplementation->mEvents.find(strEventName);
    if (tFoundit != sgpImplementation->mEvents.end())
        return;
    FMOD::Studio::EventDescription* pEventDescription = NULL;
    audio_system::ErrorCheck(sgpImplementation->mpStudioSystem->getEvent(strEventName.c_str(), &pEventDescription));
    if (pEventDescription){
        FMOD::Studio::EventInstance* pEventInstance = NULL;
        audio_system::ErrorCheck(pEventDescription->createInstance(&pEventInstance));
        if (pEventInstance){
            sgpImplementation->mEvents[strEventName] = pEventInstance;
        }
    }   
}

void audio_system::PlayEvent(const string &strEventName) {
    auto tFoundit = sgpImplementation->mEvents.find(strEventName);
    if (tFoundit == sgpImplementation->mEvents.end()){
        LoadEvent(strEventName);
        tFoundit = sgpImplementation->mEvents.find(strEventName);
        if (tFoundit == sgpImplementation->mEvents.end())
            return;
    }
    tFoundit->second->start();
}

void audio_system::StopEvent(const string &strEventName, bool bImmediate) {
    auto tFoundIt = sgpImplementation->mEvents.find(strEventName);
    if (tFoundIt == sgpImplementation->mEvents.end())
        return;
    
    FMOD_STUDIO_STOP_MODE eMode;
    eMode = bImmediate ? FMOD_STUDIO_STOP_IMMEDIATE : FMOD_STUDIO_STOP_ALLOWFADEOUT;
    audio_system::ErrorCheck(tFoundIt->second->stop(eMode));
}

bool audio_system::IsEventPlaying(const string &strEventName) const {
    auto tFoundIt = sgpImplementation->mEvents.find(strEventName);
    if (tFoundIt == sgpImplementation->mEvents.end())
        return false;

    FMOD_STUDIO_PLAYBACK_STATE* state = NULL;
    if (tFoundIt->second->getPlaybackState(state) == FMOD_STUDIO_PLAYBACK_PLAYING) {
        return true;
    }
    return false;
}

void audio_system::GetEventParameter(const string &strEventName,
                                     const string &strParameterName,
                                     float* parameter) {
    auto tFoundIt = sgpImplementation->mEvents.find(strEventName);
    if (tFoundIt == sgpImplementation->mEvents.end())
        return;
    
    FMOD::Studio::ParameterInstance* pParameter = NULL;
    audio_system::ErrorCheck(tFoundIt->second->getParameter(strParameterName.c_str(), &pParameter));
    audio_system::ErrorCheck(pParameter->getValue(parameter));
}

void audio_system::SetEventParameter(const string &strEventName,
                                     const string &strParameterName, float fValue) {
    auto tFoundIt = sgpImplementation->mEvents.find(strEventName);
    if (tFoundIt == sgpImplementation->mEvents.end())
        return;
    
    FMOD::Studio::ParameterInstance* pParameter = NULL;
    audio_system::ErrorCheck(tFoundIt->second->getParameter(strParameterName.c_str(), &pParameter));
    audio_system::ErrorCheck(pParameter->setValue(fValue));
}

FMOD_VECTOR audio_system::VectorToFmod(const vec3& vPosition){
    FMOD_VECTOR fVec;
    fVec.x = vPosition.x;
    fVec.y = vPosition.y;
    fVec.z = vPosition.z;
    return fVec;
}

float audio_system::dbToVolume(float dB)
{
    return powf(10.0f, 0.05f * dB);
}

float audio_system::VolumeTodb(float volume)
{
    return 20.0f * log10f(volume);
}

int audio_system::ErrorCheck(FMOD_RESULT result) {
    if (result != FMOD_OK){
        cout << "FMOD ERROR " << result << endl;
        return 1;
    }
    // cout << "FMOD all good" << endl;
    return 0;
}

void audio_system::Shutdown() {
    delete sgpImplementation;
}
