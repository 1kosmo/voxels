#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

enum SIDES
{
    LEFT = 0,
    RIGHT,
    TOP,
    BOTTOM,
    NEAR,
    FAR
};

struct plane
{
    glm::vec3 normal;
    float d;

    plane() = default;
    plane(glm::vec3 normal, float d);
    // Can also make constructor that takes 3 points on plane 
};

struct obb;

struct aabb
{
    glm::vec3 center;
    glm::vec3 halfwidths;

    aabb(glm::vec3 c, glm::vec3 hws);
    aabb(const obb& oriented);
    void expand_to_fit(const aabb& other);
};

struct obb
{
    /* c */ glm::vec3 center;
    /* u */ glm::vec3 obb_basis[3]; // Local x-, y-, and z-axes
    /* e */ glm::vec3 halfwidths;
    obb() = default;
    obb(glm::vec3 basis[3], glm::vec3 halfwidths);
    obb(glm::vec3 center, glm::vec3 basis[3], glm::vec3 halfwidths);
    obb(aabb axis_aligned, glm::quat& rotation);
};

struct frustum
{
    float m_near;
    float m_far;
    float m_fov;
    plane sides[6];

    glm::mat4 m_projection;
    glm::mat4 m_inverse_projection;

    frustum() = default;
    frustum(float near, float far, float fov, float aspect_ratio);

    // Ray tracing
    glm::vec3 send_ray_from_origin(float x_ndc, float y_ndc,
                                   glm::mat4 view, glm::vec4 viewport); 

    // Bounds checking
    bool is_aabb_inside(aabb box);
    bool is_obb_inside(obb box);
};
