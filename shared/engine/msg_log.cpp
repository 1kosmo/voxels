#include <iostream>
#include <cstdio>
#include <stdarg.h>

#include "msg_log.hpp"

namespace msg_log
{
    void info_to_cout(std::string format, ...)
    {
        va_list args;
        va_start(args, format);
        vprintf(format.c_str(), args);
        va_end(args);
    }

    void error_to_cout(std::string format, ...)
    {
        va_list args;
        va_start(args, format);
        printf("ERROR : ");
        vprintf(format.c_str(), args);
        va_end(args);
    }

    void warning_to_cout(std::string format, ...)
    {
        va_list args;
        va_start(args, format);
        printf("WARNING : ");
        vprintf(format.c_str(), args);
        va_end(args);
    }
}
