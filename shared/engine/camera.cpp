#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtx/string_cast.hpp"

#include "camera.hpp"

camera::camera()
    : m_position(0.0), m_camera_target(0.0, 0.0, 1.0, 1.0), m_up(0.0f, 1.0f, 0.0f),
      m_x_rot(0.0f), m_y_rot(0.0f), m_z_rot(0.0f),
      m_changed(true), m_pos_changed(true), m_no_rotation_calculation(false)
{
}

camera::camera(glm::vec3 pos, glm::vec3 camera_target)
    : m_position(pos), m_camera_target(camera_target, 1.0), m_up(0.0f, 1.0f, 0.0f),
      m_x_rot(0), m_y_rot(0), m_z_rot(0),
      m_changed(true), m_pos_changed(true), m_no_rotation_calculation(false)
{
    calculate_rotation();
}

void camera::relative_move(glm::vec3 delta)
{
    m_position += delta;
    m_changed = true;
    m_pos_changed = true;
}

void camera::pan_x(float delta)
{
    // m_rotation = glm::rotate(m_rotation, glm::radians(delta), 
    //                          glm::vec3(1.0f, 0.0f, 0.0f));
    m_x_rot += glm::radians(delta);
    m_x_rot = fmax(fmin(m_x_rot, m_max_x_rot), m_min_x_rot);
    m_changed = true;
}

void camera::pan_y(float delta)
{
    // m_rotation = glm::rotate(m_rotation, glm::radians(delta),
    //                          glm::vec3(0.0f, 1.0f, 0.0f));
    m_y_rot += glm::radians(delta);
    // m_y_rot = fmod(m_y_rot, glm::pi<float>());
    m_changed = true;
}

void camera::pan_z(float delta)
{
    // m_rotation = glm::rotate(m_rotation, glm::radians(delta), 
    //                          glm::vec3(0.0f, 0.0f, 1.0f));
    // m_z_rot = fmod(m_z_rot + glm::radians(delta), glm::p; 
    m_changed = true;
}

void camera::set_new_target(glm::vec3 target)
{
    m_camera_target = glm::vec4(glm::normalize(target - m_position), 1.0);
    m_changed = true;
}

void camera::calculate_rotation()
{
    if (m_no_rotation_calculation || !m_changed)
    {
        return;
    }
    m_changed = false;
    
    // First rotate y axis, then x
    m_rotation = glm::angleAxis(m_y_rot, glm::vec3(0.0f, 1.0f, 0.0f));
    m_rotation = glm::rotate(m_rotation, m_x_rot, glm::vec3(1.0f, 0.0f, 0.0f));
    // Normalize
    // m_rotation = glm::normalize(m_rotation);

    m_look_at = m_position + glm::vec3(m_rotation * m_camera_target);
}

bool camera::changed()
{
    return m_changed;
}

glm::mat4 camera::get_transform()
{
    calculate_rotation();

    return glm::lookAt(m_position, m_look_at, m_up);
}

glm::mat4 camera::get_inverse_transform()
{
    // glm::vec3 look_at = glm::vec3(m_rotation * m_camera_front);

    // return glm::inverse(
    //     glm::lookAt(m_position, m_position + look_at, glm::vec3(0.0, 1.0, 0.0))); 
    return glm::inverse(glm::lookAt(m_position, m_look_at, m_up));
}

bool camera::changed_pos()
{
    return m_pos_changed;
}

glm::vec3 camera::get_pos()
{
    m_pos_changed = false;
    return m_position;
}

glm::vec3 camera::get_view_dir()
{
    // return glm::vec3(m_rotation * m_camera_front);
    return glm::normalize(m_look_at - m_position);
}

glm::vec3 camera::get_left_of_view_dir()
{
    // glm::quat left = glm::rotate(m_rotation, glm::radians(90.0f), 
    //                              glm::vec3(0.0f, 1.0f, 0.0f));    
    // return glm::vec3(left * m_camera_front);
    return -glm::normalize(glm::cross(glm::normalize(m_look_at - m_position), m_up));
}

glm::vec3 camera::get_right_of_view_dir()
{
    // glm::quat right = glm::rotate(m_rotation, glm::radians(-90.0f), 
    //                               glm::vec3(0.0f, 1.0f, 0.0f));    
    // return glm::vec3(right * m_camera_front);
    return glm::normalize(glm::cross(glm::normalize(m_look_at - m_position), m_up));
}
