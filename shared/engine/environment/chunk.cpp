#include <functional>

#include "chunk.hpp"

chunk::chunk()
    : m_data(), // This zero initializes the array
      m_updated(true), m_num_cubes(0)
{
}

chunk::chunk(glm::ivec3 pos)
    : m_data(), m_pos(pos), m_updated(true), m_num_cubes(0)
{
}

glm::ivec3 get_nearest_chunk_pos(int rx, int ry, int rz)
{
    // TODO : This is broken
    return glm::ivec3(rx / CHUNK_SIDE - (rx < 0),
                      ry / CHUNK_SIDE - (ry < 0), 
                      rz / CHUNK_SIDE - (rz < 0));
}

glm::ivec3 get_nearest_chunk_pos(glm::ivec3& pos)
{
    // TODO : This is broken
    return glm::ivec3((pos.x + (pos.x < 0)) / CHUNK_SIDE - (pos.x < 0),
                      (pos.y + (pos.y < 0)) / CHUNK_SIDE - (pos.y < 0), 
                      (pos.z + (pos.z < 0)) / CHUNK_SIDE - (pos.z < 0));
}

size_t get_chunk_index_at_pos(int x, int y, int z)
{
    return (x + CHUNK_SIDE * (y + CHUNK_SIDE * z));
}

size_t get_chunk_index_at_pos(glm::ivec3& pos)
{
    return (pos.x + CHUNK_SIDE * (pos.y + CHUNK_SIDE * pos.z));
}

glm::ivec3 get_pos_in_chunk(glm::ivec3 pos)
{
    return glm::ivec3((pos.x % CHUNK_SIDE + CHUNK_SIDE) % CHUNK_SIDE,
                      (pos.y % CHUNK_SIDE + CHUNK_SIDE) % CHUNK_SIDE,
                      (pos.z % CHUNK_SIDE + CHUNK_SIDE) % CHUNK_SIDE);
}

glm::ivec3 get_pos_in_chunk(int x, int y, int z)
{
    return glm::ivec3((x % CHUNK_SIDE + CHUNK_SIDE) % CHUNK_SIDE,
                      (y % CHUNK_SIDE + CHUNK_SIDE) % CHUNK_SIDE,
                      (z % CHUNK_SIDE + CHUNK_SIDE) % CHUNK_SIDE);
}

size_t ivec3_hash::operator()(const glm::ivec3& k) const {
    // TODO: find a better hash function - we have a lot of collisions here
    return std::hash<int>()(k.x) ^ std::hash<int>()(k.y) ^ std::hash<int>()(k.z);
}
 
bool ivec3_hash::operator()(const glm::ivec3& a, const glm::ivec3& b) const {
    return a == b;
}

chunk_mesh_ptr chunk::generate_mesh(engine::cube_type_dict& types)
{
    // TODO: See if we can generate mesh using only unsigned chars
    chunk_mesh_ptr mesh(new chunk_mesh);
    mesh->m_num_quads = 0;

    for (int x = 0; x < CHUNK_SIDE; ++x)
    {
        for (int y = 0; y < CHUNK_SIDE; ++y)
        {
            for (int z = 0; z < CHUNK_SIDE; ++z)
            {
                unsigned char type = m_data[get_chunk_index_at_pos(x, y, z)];
                if (type == 0 || type == 255)
                {
                    continue;
                }                
                glm::vec3 color = types[m_data_to_cube_type[type - 1]];

                // Check above
                unsigned char data = m_data[get_chunk_index_at_pos(x, y + 1, z)];
                if (y == (CHUNK_SIDE - 1) || data == 0 || data == 255)
                {
                    mesh->m_vertices.emplace_back(x, y + 1, z + 1);
                    mesh->m_vertices.emplace_back(x + 1, y + 1, z + 1);
                    mesh->m_vertices.emplace_back(x + 1, y + 1, z);
                    mesh->m_vertices.emplace_back(x, y + 1, z);

                    mesh->m_normals.emplace_back(0, 1, 0);
                    mesh->m_normals.emplace_back(0, 1, 0);
                    mesh->m_normals.emplace_back(0, 1, 0);
                    mesh->m_normals.emplace_back(0, 1, 0);

                    mesh->m_colors.push_back(color);
                    mesh->m_colors.push_back(color);
                    mesh->m_colors.push_back(color);
                    mesh->m_colors.push_back(color);

                    mesh->m_num_quads += 1;
                }
                // Check below
                data = m_data[get_chunk_index_at_pos(x, y - 1, z)];
                if (y == 0 || data == 0 || data == 255)
                {
                    mesh->m_vertices.emplace_back(x, y, z);
                    mesh->m_vertices.emplace_back(x + 1, y, z);
                    mesh->m_vertices.emplace_back(x + 1, y, z + 1);
                    mesh->m_vertices.emplace_back(x, y, z + 1);

                    mesh->m_normals.emplace_back(0, -1, 0);
                    mesh->m_normals.emplace_back(0, -1, 0);
                    mesh->m_normals.emplace_back(0, -1, 0);
                    mesh->m_normals.emplace_back(0, -1, 0);

                    mesh->m_colors.push_back(color);
                    mesh->m_colors.push_back(color);
                    mesh->m_colors.push_back(color);
                    mesh->m_colors.push_back(color);
                
                    mesh->m_num_quads += 1;
                }

                // Check front
                data = m_data[get_chunk_index_at_pos(x, y, z - 1)];
                if (z == 0 || data == 0 || data == 255)
                {
                    mesh->m_vertices.emplace_back(x, y + 1, z);
                    mesh->m_vertices.emplace_back(x + 1, y + 1, z);
                    mesh->m_vertices.emplace_back(x + 1, y, z);
                    mesh->m_vertices.emplace_back(x, y, z);

                    mesh->m_normals.emplace_back(0, 0, -1);
                    mesh->m_normals.emplace_back(0, 0, -1);
                    mesh->m_normals.emplace_back(0, 0, -1);
                    mesh->m_normals.emplace_back(0, 0, -1);

                    mesh->m_colors.push_back(color);
                    mesh->m_colors.push_back(color);
                    mesh->m_colors.push_back(color);
                    mesh->m_colors.push_back(color);
                
                    mesh->m_num_quads += 1;
                }
                // Check back
                data = m_data[get_chunk_index_at_pos(x, y, z + 1)];
                if (z == (CHUNK_SIDE - 1) || data == 0 || data == 255)
                {
                    mesh->m_vertices.emplace_back(x + 1, y + 1, z + 1);
                    mesh->m_vertices.emplace_back(x, y + 1, z + 1);
                    mesh->m_vertices.emplace_back(x, y, z + 1);
                    mesh->m_vertices.emplace_back(x + 1, y, z + 1);

                    mesh->m_normals.emplace_back(0, 0, 1);
                    mesh->m_normals.emplace_back(0, 0, 1);
                    mesh->m_normals.emplace_back(0, 0, 1);
                    mesh->m_normals.emplace_back(0, 0, 1);

                    mesh->m_colors.push_back(color);
                    mesh->m_colors.push_back(color);
                    mesh->m_colors.push_back(color);
                    mesh->m_colors.push_back(color);
                
                    mesh->m_num_quads += 1;
                }
                // Check left
                data = m_data[get_chunk_index_at_pos(x - 1, y, z)];
                if (x == 0 || data == 0 || data == 255)
                {
                    mesh->m_vertices.emplace_back(x, y + 1, z + 1);
                    mesh->m_vertices.emplace_back(x, y + 1, z);
                    mesh->m_vertices.emplace_back(x, y, z);
                    mesh->m_vertices.emplace_back(x, y, z + 1);

                    mesh->m_normals.emplace_back(-1, 0, 0);
                    mesh->m_normals.emplace_back(-1, 0, 0);
                    mesh->m_normals.emplace_back(-1, 0, 0);
                    mesh->m_normals.emplace_back(-1, 0, 0);

                    mesh->m_colors.push_back(color);
                    mesh->m_colors.push_back(color);
                    mesh->m_colors.push_back(color);
                    mesh->m_colors.push_back(color);
                
                    mesh->m_num_quads += 1;
                }
                // Check right
                data = m_data[get_chunk_index_at_pos(x + 1, y, z)];
                if (x == (CHUNK_SIDE - 1) || data == 0 || data == 255)
                {
                    mesh->m_vertices.emplace_back(x + 1, y + 1, z);
                    mesh->m_vertices.emplace_back(x + 1, y + 1, z + 1);
                    mesh->m_vertices.emplace_back(x + 1, y, z + 1);
                    mesh->m_vertices.emplace_back(x + 1, y, z);

                    mesh->m_normals.emplace_back(1, 0, 0);
                    mesh->m_normals.emplace_back(1, 0, 0);
                    mesh->m_normals.emplace_back(1, 0, 0);
                    mesh->m_normals.emplace_back(1, 0, 0);

                    mesh->m_colors.push_back(color);
                    mesh->m_colors.push_back(color);
                    mesh->m_colors.push_back(color);
                    mesh->m_colors.push_back(color);
                
                    mesh->m_num_quads += 1;
                }

            } // End first for loop
        }

    } // End all for loops

    return mesh;
}

