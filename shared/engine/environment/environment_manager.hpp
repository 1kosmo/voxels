#pragma once

#include <string>
#include <vector>
#include <unordered_map>

#include <glm/glm.hpp>
#include <glm/vector_relational.hpp>

#include "../transforms.hpp"
#include "../utils/types.hpp"
#include "chunk.hpp"
#include "../bounding_volumes.hpp"

struct transparent_cube_info
{
    glm::ivec3 pos;
    engine::cube_id type;
    engine::chunk_id id;

    transparent_cube_info();
    transparent_cube_info(
        glm::ivec3& pos, engine::cube_id type, engine::chunk_id id);
};

class environment_manager
{
public:
    environment_manager();
    ~environment_manager();
    void clear_chunks();

    int read_from(const char* file_name);
    int write_to(const char* file_name);
    
    engine::changed_chunks get_changed_chunks(engine::cube_type_dict& type_dict);

    void set_cube_at_position(glm::ivec3& pos, engine::cube_id cube);
    void set_transparent_cube_at_position(glm::ivec3& pos, engine::cube_id cube);
    void delete_cube_at_position(glm::ivec3& pos);

    bool intersect_ray(glm::vec3 ray_pos, glm::vec3 ray_dir,
                       const float selection_radius, glm::ivec3& interesected);
    void get_relevant_cubes_for_intersection(aabb& test_target,
                                             std::vector<aabb>& cube_vec);

    // These let the controller know what's changed
    std::vector<transparent_cube_info> m_new_transparent_cubes;
    std::vector<engine::chunk_id> m_deletion_list;

private:
    std::unordered_map<glm::ivec3, chunk*, ivec3_hash> m_chunks;
    std::unordered_map<glm::ivec3, engine::chunk_id, ivec3_hash> m_chunk_ids;
    std::unordered_map<glm::ivec3, transparent_cube_info, ivec3_hash> m_transparent_cubes;

    void create_chunk(glm::ivec3 pos);
};
