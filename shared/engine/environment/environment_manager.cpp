#include <cstdio>

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/string_cast.hpp>

#include "../intersection.hpp"
#include "../msg_log.hpp"
#include "environment_manager.hpp"

environment_manager::environment_manager()
{
    
}

environment_manager::~environment_manager()
{
    clear_chunks();
}

void environment_manager::clear_chunks()
{
    for (std::pair<glm::ivec3, chunk*> c : m_chunks)
    {
        delete c.second;
    }

    m_chunks.clear();
    
    m_deletion_list.reserve(m_chunk_ids.size() + m_transparent_cubes.size());

    for (auto& id : m_chunk_ids)
    {
        m_deletion_list.push_back(id.second);
    }
    m_chunk_ids.clear();

    for (auto& cube : m_transparent_cubes)
    {
        m_deletion_list.push_back(cube.second.id);
    }
    m_transparent_cubes.clear();
}

int environment_manager::read_from(const char* file_name)
{
    clear_chunks();

    // File layout is
    // n -- (number of transparent cubes)
    // x y z type -- (pos and cube type)
    // x -- (number of chunks)
    // x y z -- (position of chunk)
    // dict_size id1 id2 id3 ... -- (id dictionary)
    // 0001200102.... -- (chunk data)

    FILE* file = fopen(file_name, "r");

    if (NULL == file)
    {
        ERROR("Could not open file '%s' when reading environment from file\n",
              file_name);
        return 1;
    }

    size_t amt_transparent;
    fread(&amt_transparent, sizeof(size_t), 1, file);
    for (size_t i = 0; i < amt_transparent; ++i)
    {
        transparent_cube_info cube;
        fread(glm::value_ptr(cube.pos), sizeof(int), 3, file);
        fread(&cube.type, sizeof(engine::cube_id), 1, file);
        cube.id = engine::id_generator::get().chunk();

        m_transparent_cubes[cube.pos] = cube;
        m_new_transparent_cubes.push_back(cube);
    }

    size_t amt_chunks = 0;
    fread(&amt_chunks, sizeof(size_t), 1, file);
    INFO("Reading %u amount of chunks from file '%s'\n",
         amt_chunks, file_name);

    for (size_t i = 0; i < amt_chunks; ++i)
    {
        glm::ivec3 chunk_pos; 
        fread(glm::value_ptr(chunk_pos), sizeof(int), 3, file);
        INFO("Read a chunk at pos %s\n", glm::to_string(chunk_pos).c_str());

        create_chunk(chunk_pos);
        chunk* c = m_chunks[chunk_pos];

        size_t num_defined_types = 0;
        fread(&num_defined_types, sizeof(size_t), 1, file);
        c->m_data_to_cube_type.resize(num_defined_types);
        fread(c->m_data_to_cube_type.data(), sizeof(engine::cube_id),
              num_defined_types, file);
        for (int index = 0; index < num_defined_types; ++index)
        {
            c->m_cube_type_to_data[c->m_data_to_cube_type[index]] = index + 1;
        }

        fread(c->m_data, sizeof(unsigned char), CHUNK_ARRAY_SIZE, file);

        // Mark as updated so it gets sent to renderer
        c->m_updated = true;
    }
    fclose(file);

    return 0;
}

int environment_manager::write_to(const char* file_name)
{
    FILE* file = fopen(file_name, "w");

    if (NULL == file)
    {
        ERROR("Could not open file '%s' when writing environment to file\n",
              file_name);
        return 1;
    }

    size_t amt_transparent = m_transparent_cubes.size();
    fwrite(&amt_transparent, sizeof(size_t), 1, file);
    for (const auto& tcube : m_transparent_cubes)
    {
        const transparent_cube_info& cube = tcube.second;
        fwrite(glm::value_ptr(cube.pos), sizeof(int), 3, file);
        fwrite(&cube.type, sizeof(engine::cube_id), 1, file);
    }    

    // fprintf(file, "%lu", m_chunks.size());
    size_t amt_of_chunks = m_chunks.size();
    fwrite(&amt_of_chunks, sizeof(size_t), 1, file);

    for (std::pair<glm::ivec3, chunk*> c : m_chunks)
    {
        INFO("Writing chunk at pos %s\n", glm::to_string(c.first).c_str());

        fwrite(glm::value_ptr(c.first), sizeof(int), 3, file);
        // fprintf(file, "%d%d%d", c.first.x, c.first.y, c.first.z);

        size_t cube_types = c.second->m_data_to_cube_type.size();
        // fprintf(file, "%u", cube_types);
        fwrite(&cube_types, sizeof(size_t), 1, file);
        fwrite(c.second->m_data_to_cube_type.data(), sizeof(engine::cube_id),
               cube_types, file);

        fwrite(c.second->m_data, sizeof(unsigned char), CHUNK_ARRAY_SIZE, file);
    }

    fclose(file);

    return 0;
}

engine::changed_chunks environment_manager::get_changed_chunks(
    engine::cube_type_dict& type_dict) 
{
    engine::changed_chunks meshes;
    for (std::pair<glm::ivec3, chunk*> c : m_chunks)
    {
        if (!c.second->m_updated)
        {
            continue;
        }

        // Mark the chunk as no longer updated
        c.second->m_updated = false;

        chunk_mesh_ptr mesh = c.second->generate_mesh(type_dict);
        // Give general chunk transform to mesh
        mesh->m_mesh_transform = glm::mat4(1.0);
        mesh->m_mesh_transform[3].x = c.first.x * CHUNK_SIDE;
        mesh->m_mesh_transform[3].y = c.first.y * CHUNK_SIDE;
        mesh->m_mesh_transform[3].z = c.first.z * CHUNK_SIDE;

        meshes.emplace_back(m_chunk_ids[c.first], mesh);
    }

    return meshes;
}

void environment_manager::set_cube_at_position(glm::ivec3& pos,
                                               engine::cube_id cube)
{
    // If a transparent cube was here, it ain't there now
    if (m_transparent_cubes.count(pos))
    {
        m_deletion_list.push_back(m_transparent_cubes[pos].id);
        m_transparent_cubes.erase(pos);
    }

    glm::ivec3 nearest_pos = get_nearest_chunk_pos(pos);
    // INFO("---------- Inserting into chunk (%s) ----------",
    //      glm::to_string(nearest_pos).c_str());

    if (m_chunks.count(nearest_pos) == 0)
    {
        create_chunk(nearest_pos);
    }

    chunk* c = m_chunks[nearest_pos];

    glm::ivec3 pos_in_chunk = get_pos_in_chunk(pos);

    size_t num_of_used_types = c->m_data_to_cube_type.size();
    unsigned char type_index;
    if (c->m_cube_type_to_data.count(cube) == 0)
    {
        type_index = num_of_used_types + 1;
        if (type_index == 255)
        {
            ERROR("Cannot handle any more cube types in this chunk\n");
            return;
        }
        c->m_data_to_cube_type.push_back(cube);
        c->m_cube_type_to_data[cube] = type_index;
    }
    else
    {
        type_index = c->m_cube_type_to_data[cube];
    }

    size_t index = get_chunk_index_at_pos(pos_in_chunk);
    if (c->m_data[index] == 0)
    {
        c->m_num_cubes += 1;
    }
    c->m_data[index] = type_index;
    c->m_updated = true;

    // INFO("Just placed cube of type %u\n at position %s",
    //      cube, glm::to_string(pos).c_str());
    // INFO("Chunk now contains %d cubes\n", c->m_num_cubes); 
}

void environment_manager::set_transparent_cube_at_position(glm::ivec3& pos,
                                                           engine::cube_id cube)
{
    // If a transparent cube was here, it ain't there now
    if (m_transparent_cubes.count(pos))
    {
        m_deletion_list.push_back(m_transparent_cubes[pos].id);
        m_transparent_cubes.erase(pos);
    }
    transparent_cube_info info(pos, cube, engine::id_generator::get().chunk());
    m_transparent_cubes[pos] = info;
    m_new_transparent_cubes.push_back(info);

    glm::ivec3 nearest_pos = get_nearest_chunk_pos(pos);
    // INFO("---------- Inserting into chunk (%s) ----------",
    //      glm::to_string(nearest_pos).c_str());

    if (m_chunks.count(nearest_pos) == 0)
    {
        create_chunk(nearest_pos);
    }

    chunk* c = m_chunks[nearest_pos];

    glm::ivec3 pos_in_chunk = get_pos_in_chunk(pos);

    size_t index = get_chunk_index_at_pos(pos_in_chunk);
    if (c->m_data[index] == 0)
    {
        c->m_num_cubes += 1;
    }
    c->m_data[index] = 255;
    c->m_updated = true;

}

void environment_manager::delete_cube_at_position(glm::ivec3& pos)
{
    if (m_transparent_cubes.count(pos))
    {
        m_deletion_list.push_back(m_transparent_cubes[pos].id);
        m_transparent_cubes.erase(pos);
    }

    glm::ivec3 nearest_pos = get_nearest_chunk_pos(pos);
    if (m_chunks.count(nearest_pos) == 0)
    {
        return;
    }

    chunk* c = m_chunks[nearest_pos];
    glm::ivec3 pos_in_chunk = get_pos_in_chunk(pos);

    size_t index = get_chunk_index_at_pos(pos_in_chunk);
    if (c->m_data[index] != 0 && c->m_data[index] != 255)
    {
        c->m_updated = true;
        c->m_data[index] = 0;
    }
}

void environment_manager::create_chunk(glm::ivec3 pos)
{
    m_chunks[pos] = new chunk(pos);
    m_chunk_ids[pos] = engine::id_generator::get().chunk();
}

bool environment_manager::intersect_ray(
    glm::vec3 ray_pos, glm::vec3 ray_dir, const float selection_radius,
    glm::ivec3& intersected)
{
    bool found = false;

    glm::vec3 ray_end_pos = ray_pos + selection_radius * glm::normalize(ray_dir);
    
    // INFO("Ray start pos is : %s\n", glm::to_string(ray_pos).c_str());
    // INFO("Ray end pos should be : %s\n", glm::to_string(ray_end_pos).c_str());

    std::vector<glm::ivec3> voxels_to_check =
        get_grid_points_between(ray_pos, ray_end_pos, 1.0f);
    unsigned int num_voxels = voxels_to_check.size();

    glm::ivec3 last_chunk_pos = get_nearest_chunk_pos(voxels_to_check[0]);
    chunk* last_chunk = NULL;
    if (m_chunks.count(last_chunk_pos) != 0)
    {
        last_chunk = m_chunks[last_chunk_pos];
    }
    for (unsigned int i = 0; i < num_voxels && !found; ++i)
    {
        glm::ivec3 this_chunk_pos = get_nearest_chunk_pos(voxels_to_check[i]);
        if (last_chunk_pos != this_chunk_pos)
        {
            last_chunk_pos = this_chunk_pos;
            if (m_chunks.count(this_chunk_pos) != 0)
            {
                last_chunk = m_chunks[this_chunk_pos];
            }
            else
            {
                last_chunk = NULL;
                // INFO("No chunk at voxel pos %s\n", glm::to_string(voxels_to_check[i]).c_str());
                continue;
            }
        }
        else if (last_chunk == NULL)
        {
            // INFO("No chunk at voxel pos %s\n", glm::to_string(voxels_to_check[i]).c_str());
            continue;
        }

        // INFO("Checked voxel %s\n", glm::to_string(voxels_to_check[i]).c_str());

        glm::ivec3 pos_in_chunk = get_pos_in_chunk(voxels_to_check[i]);
        if (0 != last_chunk->m_data[get_chunk_index_at_pos(pos_in_chunk)])
        {
            intersected = voxels_to_check[i];
            found = true;
        }
    }

    if (found)
    {
        glm::vec3 point, normal;
        if (intersect_ray_aabb(ray_pos, ray_dir,
                               aabb(glm::vec3(intersected) + glm::vec3(0.5), glm::vec3(0.5)),
                               point, normal))
        {
            intersected += glm::ivec3(normal);
        }
        else
        {
            found = false;
        }
    }
    else 
    {
        intersected = glm::ivec3(ray_end_pos);
    }

    return found;
}

void environment_manager::get_relevant_cubes_for_intersection(
    aabb& target, std::vector<aabb>& cube_vec)
{
    // !!! These functions might be a problem source...
    glm::ivec3 min(floor(target.center - target.halfwidths));
    glm::ivec3 max(floor(target.center + target.halfwidths));

    for (int x = min.x; x <= max.x; ++x)
    {
        for (int y = min.y; y <= max.y; ++y)
        {
            for (int z = min.z; z <= max.z; ++z)
            {
                glm::ivec3 chunk_pos = get_nearest_chunk_pos(x, y, z);
                if (m_chunks.count(chunk_pos) == 0) { continue; }

                chunk* c = m_chunks[chunk_pos];
                glm::ivec3 pos_in_chunk = get_pos_in_chunk(x, y, z);
                if (0 != c->m_data[get_chunk_index_at_pos(pos_in_chunk)])
                {
                    cube_vec.emplace_back(glm::vec3(x, y, z) + glm::vec3(0.5f),
                                          glm::vec3(0.5f));
                }
            } // End z
        }        
    } // End xyz
}

transparent_cube_info::transparent_cube_info() {}

transparent_cube_info::transparent_cube_info(
        glm::ivec3& pos, engine::cube_id type, engine::chunk_id id)
    : pos(pos), type(type), id(id)
{
    
}
