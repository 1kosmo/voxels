#pragma once

#include <vector>

#include <glm/glm.hpp>

#include "../utils/types.hpp"
#include "../transforms.hpp"

#define CHUNK_SIDE 64
#define CHUNK_ARRAY_SIZE CHUNK_SIDE * CHUNK_SIDE * CHUNK_SIDE

glm::ivec3 get_nearest_chunk_pos(int rx, int ry, int rz);
glm::ivec3 get_nearest_chunk_pos(glm::ivec3& pos);
size_t get_chunk_index_at_pos(int x, int y, int z);
size_t get_chunk_index_at_pos(glm::ivec3& pos);
glm::ivec3 get_pos_in_chunk(glm::ivec3 pos);
glm::ivec3 get_pos_in_chunk(int x, int y, int z);

class chunk
{
public:
    chunk();
    chunk(glm::ivec3 pos);

    chunk_mesh_ptr generate_mesh(engine::cube_type_dict& types);

    std::vector<engine::cube_id> m_data_to_cube_type;
    std::unordered_map<engine::cube_id, unsigned char> m_cube_type_to_data;
    unsigned char m_data[CHUNK_ARRAY_SIZE];
    glm::ivec3 m_pos;
    bool m_updated;
    unsigned int m_num_cubes;
};

struct ivec3_hash {
    size_t operator()(const glm::ivec3& k) const;
    bool operator()(const glm::ivec3& a, const glm::ivec3& b) const;
};
