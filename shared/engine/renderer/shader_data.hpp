#pragma once

#include <glm/glm.hpp>

template<class T>
struct uniform_block
{
    T block;
    unsigned int ubo;
    unsigned int binding_point;
    // bool updated;
};

//-- Used GLSL structs

struct DirLight
{
    glm::vec3 direction;
    float pad1;
    glm::vec3 ambient;
    float pad2;
    glm::vec3 diffuse;
    float pad3;
    // glm::vec3 specular;
};

struct PointLight
{    
    glm::vec3 position;
    float pad1;
    glm::vec3 attenuation;
    // float constant;
    // float linear;
    // float quadratic;  
    float pad2;
    glm::vec3 ambient;
    float pad3;
    glm::vec3 diffuse;
    float pad4;
    // glm::vec3 specular;
};

//-- Uniform blocks

struct shader_data_vert
{
    glm::mat4 projection;
    glm::mat4 camera;
};

struct shader_data_static_lights
{
    DirLight main_light;
};

#define MAX_DYNAMIC_LIGHTS 8
struct shader_data_dynamic_lights
{
    PointLight lights[MAX_DYNAMIC_LIGHTS];
};
