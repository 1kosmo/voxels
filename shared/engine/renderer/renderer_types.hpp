#pragma once

#include <ctime>
#include <unistd.h>
#include <memory>
#include <unordered_map>

#include <GL/gl3w.h>
#include <glm/glm.hpp>

#include "../utils/shader_program.hpp"
#include "../utils/types.hpp"

struct mesh_shader
{
    GLint m_vertex_pos_loc;
    GLint m_normal_loc;
    GLint m_color_loc;

    GLint m_uniform_transform;
    GLint m_uniform_shadow_map;
    GLint m_uniform_light_space_transform;
    GLint m_uniform_normal_transform;
    bool m_use_normal_transform;
    // its pointless to keep them, they will never be used again
   // GLuint m_index_vertex_uniforms;
    // GLuint m_index_static_light_uniforms;
    // GLuint m_index_dynamic_light_uniforms;    

    shader_program m_program;
};
typedef std::shared_ptr<mesh_shader> shader_ptr;

class time_keeper
{
public:
    void tick();

    void set_fps_target(unsigned int target);
    
private:
    unsigned int m_fps_target;
    unsigned int m_fps_current;
    timespec m_last_time;
    useconds_t m_last_interval;
    useconds_t m_time_to_sleep;
    useconds_t m_ideal_sleep_time;
};

struct transparent_cube_params
{
    glm::mat4 m_transform;
    glm::vec4 m_color;
    glm::vec3 m_position;
    float m_size;

    transparent_cube_params();
    transparent_cube_params(glm::vec4 color, glm::ivec3 position, float size);
};

class transparent_cube
{
    GLuint m_cube_vao;
    GLuint m_uniform_transform;
    GLuint m_uniform_color;
    
public:
    void init(shader_ptr program);
    transparent_cube();
    ~transparent_cube();

    void draw(const transparent_cube_params& params);
};

class lines
{
    GLuint m_vao;
    GLuint m_vbo;

    GLuint m_uniform_transform;
    GLuint m_uniform_color;

    shader_ptr m_program;

    std::unordered_map<engine::chunk_id, std::pair<glm::vec3, glm::vec3> > m_lines;

public:
    void init(shader_ptr program);
    void set_line(engine::chunk_id, std::pair<glm::vec3, glm::vec3> line);
    void draw();    
};
