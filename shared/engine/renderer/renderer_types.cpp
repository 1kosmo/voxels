#include <unistd.h>

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "renderer_types.hpp"
#include "../utils/gl_error_check.hpp"
#include "../msg_log.hpp"

//-- Time keeper

void time_keeper::set_fps_target(unsigned int target)
{
    m_fps_target = target;
    m_fps_current = 0;

    clock_gettime(CLOCK_REALTIME, &m_last_time);

    m_last_interval = 0;
    m_time_to_sleep = 1000000 / target;
    m_ideal_sleep_time = m_time_to_sleep;
}

void time_keeper::tick()
{
    timespec cur;
    clock_gettime(CLOCK_REALTIME, &cur);
    m_last_interval = (m_last_time.tv_sec - cur.tv_sec) * 1000000
        + ((m_last_time.tv_nsec - cur.tv_nsec) / 1000);

    if (m_last_time.tv_sec < cur.tv_sec)
    {
        // INFO("Running at %d FPS\n", m_fps_current);
        m_fps_current = 1;
    }
    else
    {
        m_fps_current += 1;
    }

    if ((m_ideal_sleep_time * m_fps_current) < (cur.tv_nsec / 1000))
    {
        m_time_to_sleep /= 2;
        usleep(m_time_to_sleep);
    }
    else
    {
        m_time_to_sleep = m_ideal_sleep_time;
        usleep(m_ideal_sleep_time);
    }

    m_last_time = cur;
}

//-- Selection cube

transparent_cube::transparent_cube()
    : m_cube_vao(0), m_uniform_transform(0), m_uniform_color(0)
{
}

transparent_cube::~transparent_cube()
{
    if (m_uniform_color != m_uniform_transform)
    {
        // delete vao
    }
}

void transparent_cube::init(shader_ptr program)
{
    // Find uniform positions
    m_uniform_transform = program->m_program.getUniformLocation("vertex_transform");
    m_uniform_color = program->m_program.getUniformLocation("color");

    // Init Cube VAO
    float vertices[24] =
        {
            -0.5, 0.5, 0.5, // front top left
            0.5, 0.5, 0.5, // front top right
            -0.5, -0.5, 0.5, // front bottom left
            0.5, -0.5, 0.5, // front bottom right
            -0.5, 0.5, -0.5, // back top left
            0.5, 0.5, -0.5, // back top right
            -0.5, -0.5, -0.5, // back bottom left
            0.5, -0.5, -0.5, // back bottom right
        };
    
    char element_buffer[36] =
        {
            3, 1, 0, // Front face upper triangle
            3, 0, 2, // Front face bottom triangle

            1, 5, 4, // Top face upper triangle
            4, 1, 0, // Top face bottom triangle

            7, 5, 1, // Right Side face upper triangle
            7, 1, 3, // Right Side face bottom triangle

            2, 0, 4, // Left Side face upper triangle
            2, 4, 6, // Side Side face bottom triangle

            4, 5, 7, // Back face upper triangle
            4, 7, 6, // Back face bottom triangle

            7, 3, 2, // Bottom face upper triangle
            7, 2, 6, // Bottom face bottom triangle
        };
    
    glGenVertexArrays(1, &m_cube_vao);
    glBindVertexArray(m_cube_vao);

    GLuint cube_vbo = 0;
    GLuint cube_ebo = 0;

    // Bind vbo
    glGenBuffers(1, &cube_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, cube_vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    // Bind ebo
    glGenBuffers(1, &cube_ebo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, cube_ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                 sizeof(element_buffer), element_buffer, GL_STATIC_DRAW);

    // // Enable vertex attribute for position
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,
                          3 * sizeof(GL_FLOAT), nullptr); // stride, offset
    glEnableVertexAttribArray(0);

        // Unbind VAO
    glBindVertexArray(0);
    // And the other buffers we used in it
    glBindBuffer( GL_ARRAY_BUFFER, 0 );
    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );

    CHECK_GL_ERRORS;
}

void transparent_cube::draw(const transparent_cube_params& params)
{
    glUniformMatrix4fv(m_uniform_transform, 1, GL_FALSE,
                       glm::value_ptr(params.m_transform));
    glUniform4fv(m_uniform_color, 1, glm::value_ptr(params.m_color));

    glBindVertexArray(m_cube_vao);
    glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_BYTE, 0);
    glBindVertexArray(0);
}

transparent_cube_params::transparent_cube_params()
{
}

transparent_cube_params::transparent_cube_params(
    glm::vec4 color, glm::ivec3 position, float size)
    : m_transform(1.0f), m_color(color), m_position(position), m_size(size)
{
    m_transform = glm::scale(m_transform, glm::vec3(size));
    float scale_adjustment = 1.0f / size;
    glm::vec3 offset(0.5f);
    m_transform = glm::translate(m_transform, (m_position + offset) * scale_adjustment);
}

void lines::init(shader_ptr program)
{
    m_program = program;
    m_uniform_transform = program->m_program.getUniformLocation("vertex_transform");
    m_uniform_color = program->m_program.getUniformLocation("color");

    glGenVertexArrays(1, &m_vao);
    glGenBuffers(1, &m_vbo);

    glBindVertexArray(m_vao);
    glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
    glBufferData(m_vbo, 2 * sizeof(glm::vec3), NULL, GL_STATIC_DRAW);

    // Enable vertex pos attrib
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,
                          3 * sizeof(GL_FLOAT), nullptr); // stride, offset
    glEnableVertexAttribArray(0);
    
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void lines::set_line(engine::chunk_id id, std::pair<glm::vec3, glm::vec3> line)
{
    m_lines[id] = line;

    size_t vec3_size = sizeof(glm::vec3);
    int amount = m_lines.size();
    glBindBuffer(GL_ARRAY_BUFFER, m_vbo);

    glBufferData(GL_ARRAY_BUFFER, amount * 2 *  vec3_size, NULL,
                 GL_STATIC_DRAW);

    size_t running_offset = 0;
    for (const auto& line : m_lines)
    {
        glBufferSubData(GL_ARRAY_BUFFER, running_offset,
                        vec3_size, glm::value_ptr(line.second.first));
        glBufferSubData(GL_ARRAY_BUFFER, running_offset + vec3_size,
                        vec3_size, glm::value_ptr(line.second.second));
        running_offset += 2 * vec3_size;
    }
    // INFO("BUffered %u bytes of data for drawing lines\n", running_offset);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void lines::draw()
{
    m_program->m_program.enable();
    // Set default uniform values
    glm::mat4 def_mat(1.0);
    glm::vec4 def_color(1.0, 0.0, 0.0, 1.0);

    glUniformMatrix4fv(m_uniform_transform, 1, GL_FALSE, glm::value_ptr(def_mat));
    glUniform4fv(m_uniform_color, 1, glm::value_ptr(def_color));
    
    glBindVertexArray(m_vao);

    glDrawArrays(GL_LINES, 0, m_lines.size() * 2);

    glBindVertexArray(0);
    m_program->m_program.disable();
}
