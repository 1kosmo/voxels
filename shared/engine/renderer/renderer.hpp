#pragma once

#include <unordered_map>

#include <glm/glm.hpp>

#include "../window.hpp"
#include "../utils/types.hpp"
#include "../transforms.hpp"
#include "../bounding_volumes.hpp"
#include "renderer_types.hpp"
#include "shader_data.hpp"
#include "shadow_map.hpp"
#include "lights.hpp"

class renderer
{
public:
    renderer();
    ~renderer();
    int init(window* win, float fov_deg,
             unsigned int window_width, unsigned int window_height,             
             glm::mat4 camera, glm::vec3 camera_pos);

    void draw();

    engine::shader_id load_program(const char* vertex_shader_file,
                                   const char* fragment_shader_file,
                                   bool use_normal_transform = false,
                                   bool find_standard_uniforms = true);

    void update_chunk(engine::chunk_id id, chunk_mesh_ptr chunk,
                      engine::shader_id shader_to_use,
                      bool model_chunk = false);
    void update_chunk_transforms(engine::transformed_chunks& transforms,
                                 bool model_chunk = false);
    void delete_chunk(engine::chunk_id id);
    
    void update_projection(float fov_deg, float width_height_ratio);
    frustum* get_projection_frustum();
    void update_camera_transform(glm::mat4& new_camera);
    void update_camera_position(glm::vec3 pos);
    void update_main_light(glm::vec3 direction, glm::vec3 position,
                           glm::vec3 ambient, glm::vec3 diffuse);
    void update_point_light(engine::light_id id, glm::vec3 position,
                            glm::vec3 attenuation,
                            glm::vec3 ambient, glm::vec3 diffuse);
    void delete_point_light(engine::light_id id);


    void set_transparent_cube(engine::chunk_id,
                              glm::vec4& color, glm::ivec3& pos, float size);
    void delete_transparent_cube(engine::chunk_id);

    void set_line(engine::chunk_id, std::pair<glm::vec3, glm::vec3> line);

    void toggle_cel_shading();

private:
    window* m_window;
    frustum m_projection;
    time_keeper m_timer;

    bool m_use_cel_shader = true;;

    uniform_block<shader_data_vert> m_vertex_uniforms;
    uniform_block<glm::vec3> m_fragment_uniforms;
    shadow_map m_shadow_map;    

    std::unordered_map<engine::shader_id, shader_ptr> m_programs;
    std::unordered_map<engine::chunk_id, chunk_mesh_on_gpu> m_chunks;
    std::unordered_map<engine::chunk_id, chunk_mesh_on_gpu> m_model_chunks;
    lights m_lights;

    engine::shader_id m_basic_program;
    engine::shader_id m_cel_shader;
    lines m_lines;
    transparent_cube m_transparent_cube_drawer;
    std::unordered_map<engine::chunk_id, transparent_cube_params> m_cube_params; 

    GLuint m_general_quad_ebo;
    std::vector<unsigned int> m_general_quad_indices;
    void generate_general_quad_ebo(int quad_qty_to_fit);

    // Drawing functions
    void draw_meshes();
    void draw_cel_shader_pass();
    void draw_shadow_map_pass();
    void draw_transparent_cubes();

    // Init functions
    int init_uniform_buffers();
};

