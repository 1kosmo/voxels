#include "shadow_map.hpp"
#include "../utils/gl_error_check.hpp"

shadow_map::shadow_map()
    : m_program(NULL)
{

}

shadow_map::~shadow_map()
{
    delete m_program;
}

int shadow_map::init(unsigned int window_width, unsigned int window_height)
{
    m_window_width = window_width;
    m_window_height = window_height;
    
    glGenFramebuffers(1, &m_fbo);

    glGenTextures(1, &m_shadow_map);
    glBindTexture(GL_TEXTURE_2D, m_shadow_map);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, 2048, 2048,
                 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
    // Standard initialization for 2D textures
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    float border_color[] = {1.0f, 1.0f, 1.0f, 1.0f};
    glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, border_color);

    glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D,
                           m_shadow_map, 0);

    // We do not want to render into a color buffer, so set draw buffer to NONE
    glDrawBuffer(GL_NONE);
    glReadBuffer(GL_NONE);

    // Unbind
    glBindBuffer(GL_FRAMEBUFFER, 0);

    CHECK_GL_ERRORS;

    // Init the program
    m_program = new shader_program;
    m_program->generateProgramObject();
    m_program->attachVertexShader("../assets/simple_depth_vert.glsl");
    m_program->attachFragmentShader("../assets/simple_depth_frag.glsl");
    m_program->link();

    m_uniform_vertex_transform = m_program->getUniformLocation("vertex_transform");
    m_uniform_light_transform = m_program->getUniformLocation("light_space_transform");
}

void shadow_map::bind_to_write()
{
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_fbo);
}

void shadow_map::bind_to_read(GLenum texture_unit)
{
    glActiveTexture(texture_unit);
    glBindTexture(GL_TEXTURE_2D, m_shadow_map);
}
