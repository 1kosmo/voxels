#include <algorithm>
#include <functional>

#include <glm/gtc/type_ptr.hpp>

#include "renderer.hpp"
#include "../utils/types.hpp"
#include "../msg_log.hpp"

using namespace engine;

void renderer::draw_meshes()
{
    // Static meshes
    for (const std::pair<chunk_id, chunk_mesh_on_gpu>& mesh : m_chunks)
    {
        shader_ptr shader = m_programs[mesh.second.m_shader_to_use];

        // Enable Program
        shader->m_program.enable();

        // Upload uniforms
        glUniformMatrix4fv(shader->m_uniform_transform, 1, GL_FALSE,
                           glm::value_ptr(mesh.second.m_mesh_transform));
        glUniformMatrix4fv(shader->m_uniform_light_space_transform, 1, GL_FALSE,
                           glm::value_ptr(m_lights.m_main_light_transform));

        m_shadow_map.bind_to_read(GL_TEXTURE0);

        // Draw mesh
        glBindVertexArray(mesh.second.m_mesh_vao);
        glDrawElements(GL_TRIANGLES, mesh.second.m_num_quads * 6,
                       GL_UNSIGNED_INT, 0);
        
        // Disable program
        shader->m_program.disable();
    }

    // Models
    for (const std::pair<chunk_id, chunk_mesh_on_gpu>& mesh : m_model_chunks)
    {
        shader_ptr shader = m_programs[mesh.second.m_shader_to_use];

        // Enable Program
        shader->m_program.enable();

        // Upload uniforms
        glUniformMatrix4fv(shader->m_uniform_transform, 1, GL_FALSE,
                           glm::value_ptr(mesh.second.m_mesh_transform));
        glUniformMatrix4fv(shader->m_uniform_light_space_transform, 1, GL_FALSE,
                           glm::value_ptr(m_lights.m_main_light_transform));
        glUniformMatrix3fv(shader->m_uniform_normal_transform, 1, GL_FALSE,
                           glm::value_ptr(mesh.second.m_normal_transform));
            
        m_shadow_map.bind_to_read(GL_TEXTURE0);

        // Draw mesh
        glBindVertexArray(mesh.second.m_mesh_vao);
        glDrawElements(GL_TRIANGLES, mesh.second.m_num_quads * 6,
                       GL_UNSIGNED_INT, 0);
        
        // Disable program
        shader->m_program.disable();
    }
}

void renderer::draw_shadow_map_pass()
{
    glViewport(0, 0, 2048, 2048);
    m_shadow_map.m_program->enable();
    glUniformMatrix4fv(m_shadow_map.m_uniform_light_transform, 1, GL_FALSE,
                       glm::value_ptr(m_lights.m_main_light_transform));

    m_shadow_map.bind_to_write();
    glClear(GL_DEPTH_BUFFER_BIT);

    // To prevent peter-panning, we can cull the front faces
    glCullFace(GL_FRONT);

    for (const std::pair<chunk_id, chunk_mesh_on_gpu>& mesh : m_chunks)
    {
        // Upload uniforms
        glUniformMatrix4fv(m_shadow_map.m_uniform_vertex_transform, 1, GL_FALSE,
                           glm::value_ptr(mesh.second.m_mesh_transform));
        // Draw mesh
        glBindVertexArray(mesh.second.m_mesh_vao);
        glDrawElements(GL_TRIANGLES, mesh.second.m_num_quads * 6, GL_UNSIGNED_INT, 0);
    }
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    // Reset normal back-face culling
    glCullFace(GL_BACK);
    glViewport(0, 0, m_shadow_map.m_window_width, m_shadow_map.m_window_height);
}

void renderer::draw_cel_shader_pass()
{
    glCullFace(GL_FRONT);

    shader_ptr cel_shader = m_programs[m_cel_shader];
    cel_shader->m_program.enable();
    for (const std::pair<chunk_id, chunk_mesh_on_gpu>& mesh : m_chunks)
    {
        glUniformMatrix4fv(cel_shader->m_uniform_transform, 1, GL_FALSE,
                           glm::value_ptr(mesh.second.m_mesh_transform));
        // Draw mesh
        glBindVertexArray(mesh.second.m_mesh_vao);
        glDrawElements(GL_TRIANGLES, mesh.second.m_num_quads * 6,
                       GL_UNSIGNED_INT, 0);
    }
    for (const std::pair<chunk_id, chunk_mesh_on_gpu>& mesh : m_model_chunks)
    {
        glUniformMatrix4fv(cel_shader->m_uniform_transform, 1, GL_FALSE,
                           glm::value_ptr(mesh.second.m_mesh_transform));
        // Draw mesh
        glBindVertexArray(mesh.second.m_mesh_vao);
        glDrawElements(GL_TRIANGLES, mesh.second.m_num_quads * 6,
                       GL_UNSIGNED_INT, 0);
    }
    cel_shader->m_program.disable();

    glCullFace(GL_BACK);
}

void renderer::draw_transparent_cubes()
{
    glDisable(GL_CULL_FACE);
    glEnable(GL_BLEND);

    shader_ptr shader = m_programs[m_basic_program];        
    shader->m_program.enable();

    std::vector<transparent_cube_params> cubes;
    cubes.reserve(m_cube_params.size());
    for (const std::pair<engine::cube_id, transparent_cube_params>& cube :
             m_cube_params)
    {
        cubes.push_back(cube.second);
    }

    glm::vec3 camera_pos = m_fragment_uniforms.block;
    auto sort_by_dist = [&](transparent_cube_params a, transparent_cube_params b)
    {
        return glm::length(camera_pos - a.m_position) >
        glm::length(camera_pos - b.m_position);
    };
    std::sort(cubes.begin(), cubes.end(), sort_by_dist);

    for (transparent_cube_params& cube : cubes)
    {
        m_transparent_cube_drawer.draw(cube);
    }

    shader->m_program.disable();

    glDisable(GL_BLEND);
    glEnable(GL_CULL_FACE);
}
