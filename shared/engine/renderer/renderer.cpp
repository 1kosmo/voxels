#include <GL/gl3w.h>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/string_cast.hpp>

#include <imgui/imgui.h>

#include "renderer.hpp"
#include "../utils/gl_error_check.hpp"
#include "../msg_log.hpp"

using namespace engine;

const int amount_verts_in_cube = 36;

void renderer::draw()
{
    glClearColor(0.41f, 0.41f, 0.41f, 1.0f);
    glClearDepth(1.0);
    glViewport(0, 0, (int)ImGui::GetIO().DisplaySize.x, (int)ImGui::GetIO().DisplaySize.y);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Render all other chunks
    draw_shadow_map_pass();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    if (m_use_cel_shader)
    {
        draw_cel_shader_pass();
    }
    draw_meshes();

    // Draw lines
    m_lines.draw();

    // Draw transparent objects
    draw_transparent_cubes();

    // Restore state
    glBindVertexArray(0);

    // Draw GUI
    ImGui::Render();

    // Swap Buffers
    m_window->double_buffer();
    
    // Limit FPS
    m_timer.tick();
}

engine::shader_id renderer::load_program(const char* vertex_shader_file,
                                         const char* fragment_shader_file,
                                         bool use_normal_transform,
                                         bool find_standard_uniforms)
{
    // TODO: Pass in vector of uniform names, along with function pointers
    //    to be called before drawing with the shader to init those uniforms
    
    shader_id new_shader = id_generator::get().shader();

    shader_ptr shader_info(new mesh_shader());
    shader_program* program = &shader_info->m_program;

    // To make GLSL program : generate program object -> attach -> link
    program->generateProgramObject();
    program->attachVertexShader(vertex_shader_file);
    program->attachFragmentShader(fragment_shader_file);
    program->link();

    shader_info->m_vertex_pos_loc = 0;
    shader_info->m_normal_loc = 1;
    shader_info->m_color_loc = 2;

    if (find_standard_uniforms)
    {
        shader_info->m_uniform_transform = program->getUniformLocation(
            "vertex_transform");

        shader_info->m_uniform_light_space_transform = program->getUniformLocation(
            "light_space_transform");

        // Initialize shadow map to use texture unit 0
        shader_info->m_uniform_shadow_map = program->getUniformLocation(
            "shadow_map");
        program->enable();
        glUniform1i(shader_info->m_uniform_shadow_map, 0);
        program->disable();

        if (use_normal_transform)
        {
            shader_info->m_uniform_normal_transform = program->getUniformLocation(
                "normal_transform");
            shader_info->m_use_normal_transform = true;
        }
    }

    {
        GLuint index = program->getUniformBlockLocation("shader_data_vert");
        if (index != GL_INVALID_INDEX)
        {
            glUniformBlockBinding(program->getProgramObject(), index,
                                  m_vertex_uniforms.binding_point);
        }
    }
    {
        GLuint index = program->getUniformBlockLocation("shader_data_frag");
        if (index != GL_INVALID_INDEX)
        {
            glUniformBlockBinding(program->getProgramObject(), index,
                                  m_fragment_uniforms.binding_point);
        }
    }
    {
        GLuint index = program->getUniformBlockLocation("static_lights");
        if (index != GL_INVALID_INDEX)
        {
            glUniformBlockBinding(program->getProgramObject(), index,
                                  m_lights.m_static_light_uniforms.binding_point);
        }
    }
    {
        GLuint index = program->getUniformBlockLocation("dynamic_lights");
        if (index != GL_INVALID_INDEX)
        {
            glUniformBlockBinding(program->getProgramObject(), index,
                                  m_lights.m_dynamic_light_uniforms.binding_point);
        }
    }

    CHECK_GL_ERRORS;

    m_programs[new_shader] = shader_info;
    
    return new_shader;
}

void renderer::update_chunk(chunk_id id, chunk_mesh_ptr chunk,
                            shader_id shader_to_use, bool model_chunk)
{
    if (m_programs.count(shader_to_use) == 0)
    {
        ERROR("Renderer received chunk with invalid shader_id\n");
        return;
    }

    chunk_mesh_on_gpu gpu_reference;
    if (!m_chunks.count(id))
    {
        glGenVertexArrays(1, &gpu_reference.m_mesh_vao);
        glGenBuffers(1, &gpu_reference.m_vertex_buffer);
        glGenBuffers(1, &gpu_reference.m_normal_buffer);
        glGenBuffers(1, &gpu_reference.m_color_buffer);
    }
    else
    {
        gpu_reference = m_chunks[id];
    }

    // shader_ptr shader = m_programs[shader_to_use];

    gpu_reference.m_num_quads = chunk->m_num_quads;
    gpu_reference.m_shader_to_use = shader_to_use;
    gpu_reference.m_mesh_transform = chunk->m_mesh_transform;

    // Check that general ebo is big enough
    if (m_general_quad_indices.size() < (gpu_reference.m_num_quads * 6))
    {
        generate_general_quad_ebo(gpu_reference.m_num_quads);
    }

    // -- Start binding everything to VAO
    glBindVertexArray(gpu_reference.m_mesh_vao);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_general_quad_ebo);
    // Bind mesh vertices
    glBindBuffer(GL_ARRAY_BUFFER, gpu_reference.m_vertex_buffer);
    glBufferData(GL_ARRAY_BUFFER, gpu_reference.m_num_quads * 4 * sizeof(glm::vec3),
                 chunk->m_vertices.data(), GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,
                          3 * sizeof(GL_FLOAT), (GLvoid*) 0);
    glEnableVertexAttribArray(0);

    // Bind mesh normals
    glBindBuffer(GL_ARRAY_BUFFER, gpu_reference.m_normal_buffer);
    glBufferData(GL_ARRAY_BUFFER, gpu_reference.m_num_quads * 4 * sizeof(glm::vec3),
                 chunk->m_normals.data(), GL_STATIC_DRAW);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE,
                          3 * sizeof(GL_FLOAT), (GLvoid*) 0);
    glEnableVertexAttribArray(1);

    // Bind mesh vertex colors
    glBindBuffer(GL_ARRAY_BUFFER, gpu_reference.m_color_buffer);
    glBufferData(GL_ARRAY_BUFFER, gpu_reference.m_num_quads * 4 * sizeof(glm::vec3),
                 chunk->m_colors.data(), GL_STATIC_DRAW);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE,
                          3 * sizeof(GL_FLOAT), (GLvoid*) 0);
    glEnableVertexAttribArray(2);

    // -- Unbind used vars
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    CHECK_GL_ERRORS;

    if (model_chunk)
    {
        m_model_chunks[id] = gpu_reference;
    }
    else
    {
        m_chunks[id] = gpu_reference;
    }
}

void renderer::update_chunk_transforms(engine::transformed_chunks& transforms,
                                       bool model_chunk)
{
    for (std::pair<engine::chunk_id, glm::mat4> t : transforms)
    {
        chunk_mesh_on_gpu* mesh_info;
        if (model_chunk)
        {
            mesh_info = &m_model_chunks[t.first];
        }
        else
        {
            mesh_info = &m_chunks[t.first];
        }
        mesh_info->m_mesh_transform = t.second;
        if (m_programs[mesh_info->m_shader_to_use]->m_use_normal_transform)
        {
            mesh_info->m_normal_transform = glm::mat3(
                glm::transpose(glm::inverse(t.second)));
        }
    }
}

void renderer::delete_chunk(engine::chunk_id id)
{
    m_model_chunks.erase(id);
    m_chunks.erase(id);
    m_cube_params.erase(id);
}

void renderer::update_projection(float fov_deg, float width_height_ratio)
{
    m_projection = frustum(0.1f, 1000.0f, fov_deg, width_height_ratio);
    m_vertex_uniforms.block.projection = m_projection.m_projection;

    glBindBuffer(GL_UNIFORM_BUFFER, m_vertex_uniforms.ubo);
    glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(glm::mat4),
                    glm::value_ptr(m_vertex_uniforms.block.projection));
    glBindBuffer(GL_UNIFORM_BUFFER, 0); 
}

frustum* renderer::get_projection_frustum()
{
    return &m_projection;
}

void renderer::update_camera_transform(glm::mat4& new_camera)
{
    m_vertex_uniforms.block.camera = new_camera;

    glBindBuffer(GL_UNIFORM_BUFFER, m_vertex_uniforms.ubo);
    glBufferSubData(GL_UNIFORM_BUFFER, sizeof(glm::mat4), sizeof(glm::mat4),
                    glm::value_ptr(m_vertex_uniforms.block.camera));
    glBindBuffer(GL_UNIFORM_BUFFER, 0); 
}

void renderer::update_camera_position(glm::vec3 pos)
{
    m_fragment_uniforms.block = pos;

    glBindBuffer(GL_UNIFORM_BUFFER, m_fragment_uniforms.ubo);
    glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(glm::vec3),
                    glm::value_ptr(m_fragment_uniforms.block));
    glBindBuffer(GL_UNIFORM_BUFFER, 0); 
}

void renderer::update_main_light(glm::vec3 direction, glm::vec3 position,
                                 glm::vec3 ambient, glm::vec3 diffuse)
{
    m_lights.set_main_light(direction, position, ambient, diffuse);
}

void renderer::update_point_light(engine::light_id id, glm::vec3 position,
                                  glm::vec3 attenuation,
                                  glm::vec3 ambient, glm::vec3 diffuse)
{
    m_lights.set_point_light(id, position, attenuation, ambient, diffuse);
}

void renderer::delete_point_light(engine::light_id id)
{
    m_lights.delete_point_light(id);    
}

void renderer::set_transparent_cube(engine::chunk_id id,
                                    glm::vec4& color, glm::ivec3& pos, float size)
{
    m_cube_params[id] = transparent_cube_params(color, pos, size);
}

void renderer::delete_transparent_cube(engine::chunk_id id)
{
    if (m_cube_params.count(id))
    {
        m_cube_params.erase(id);
    }
}

void renderer::set_line(engine::chunk_id id, std::pair<glm::vec3, glm::vec3> line)
{
    // INFO("Line set at %s ---> %s\n", glm::to_string(line.first).c_str(),
    //      glm::to_string(line.second).c_str());
    m_lines.set_line(id, line);
}

////////////////////////////// INIT Functions //////////////////////////////
renderer::renderer()
{
}

renderer::~renderer()
{
}

int renderer::init(window* win, float fov_deg,
                   unsigned int window_width, unsigned int window_height,
                   glm::mat4 camera, glm::vec3 camera_pos)
{
    //-- Init member variables
    m_window = win;
    m_timer.set_fps_target(144);
    
    //-- OpenGL options
    // Cull back faces
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    glFrontFace(GL_CCW);
    // Transparency function
    glBlendFunc(GL_SRC_ALPHA, GL_SRC_ALPHA);
    // Depth testing
    glEnable(GL_DEPTH_TEST);
    // glDepthFunc

    //-- Init Uniform buffer objects
    init_uniform_buffers();

    // Initialize projection and viewing matrices
    update_projection(fov_deg, (float) window_width / (float) window_height);
    update_camera_transform(camera);
    update_camera_position(camera_pos);

    // Init general quad ebo
    glGenBuffers(1, &m_general_quad_ebo);
    generate_general_quad_ebo(64);

    // Init basic program to use with single cubes and lines
    m_basic_program = load_program("../assets/basic_shader_vert.glsl",
                                   "../assets/basic_shader_frag.glsl",
                                   false, false);
    m_cel_shader = load_program("../assets/cel_shader_vert.glsl",
                                "../assets/cel_shader_frag.glsl",
                                false, false);
    {
        shader_ptr info = m_programs[m_cel_shader];
        info->m_uniform_transform = info->m_program.getUniformLocation(
            "vertex_transform");
    }
        

    // Init member sub-types
    m_transparent_cube_drawer.init(m_programs[m_basic_program]);
    m_lines.init(m_programs[m_basic_program]);
    m_shadow_map.init(window_width, window_height);

    CHECK_GL_ERRORS;

    return 0;
}

int renderer::init_uniform_buffers()
{
    glGenBuffers(1, &m_vertex_uniforms.ubo);
    glBindBuffer(GL_UNIFORM_BUFFER, m_vertex_uniforms.ubo);
    glBufferData(GL_UNIFORM_BUFFER, sizeof(shader_data_vert), NULL,
                 GL_STATIC_DRAW);
    m_vertex_uniforms.binding_point = 0;

    glGenBuffers(1, &m_fragment_uniforms.ubo);
    glBindBuffer(GL_UNIFORM_BUFFER, m_fragment_uniforms.ubo);
    glBufferData(GL_UNIFORM_BUFFER, sizeof(glm::vec3), NULL,
                 GL_STATIC_DRAW);
    m_fragment_uniforms.binding_point = 1;

    glBindBufferBase(GL_UNIFORM_BUFFER, m_vertex_uniforms.binding_point,
                     m_vertex_uniforms.ubo);
    glBindBufferBase(GL_UNIFORM_BUFFER, m_fragment_uniforms.binding_point,
                     m_fragment_uniforms.ubo);

    // Other uniform buffers
    m_lights.init_uniform_buffers();

    // Unbind Uniform buffers
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

void renderer::generate_general_quad_ebo(int quad_qty_to_fit)
{
    if (quad_qty_to_fit < (m_general_quad_indices.size() / 6))
    {
        ERROR("Just tried to resize general quad EBO to a smaller buffer!\n");
        return;
    }

    unsigned int num_vertices = quad_qty_to_fit * 6;
    unsigned int ebo_size;
    for (ebo_size = 1; ebo_size < num_vertices; ebo_size <<= 1) {}

    m_general_quad_indices.reserve(ebo_size);
    
    unsigned int cur_quad_start = (m_general_quad_indices.size() / 6) * 4;
    ebo_size -= ebo_size % 6;
    for (unsigned int i = m_general_quad_indices.size(); i < ebo_size; i += 6)
    {
        // Top triangle
        m_general_quad_indices.push_back(cur_quad_start);
        m_general_quad_indices.push_back(cur_quad_start + 1);
        m_general_quad_indices.push_back(cur_quad_start + 2);

        // Bottom triangle
        m_general_quad_indices.push_back(cur_quad_start);
        m_general_quad_indices.push_back(cur_quad_start + 2);
        m_general_quad_indices.push_back(cur_quad_start + 3);

        cur_quad_start += 4;
    }

    // Bind the buffer to ebo
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_general_quad_ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, ebo_size * sizeof(GL_UNSIGNED_INT),
                 m_general_quad_indices.data(), GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void renderer::toggle_cel_shading()
{
    m_use_cel_shader = !m_use_cel_shader;
}
