#include <GL/gl3w.h>

#include <glm/gtc/matrix_transform.hpp>

#include "lights.hpp"
#include "../msg_log.hpp"

int lights::init_uniform_buffers()
{
    // Initialize uniform blocks used for lights

    glGenBuffers(1, &m_static_light_uniforms.ubo);
    glBindBuffer(GL_UNIFORM_BUFFER, m_static_light_uniforms.ubo);
    glBufferData(GL_UNIFORM_BUFFER, sizeof(shader_data_static_lights), NULL,
                 GL_STATIC_DRAW);
    m_static_light_uniforms.binding_point = 2;

    // Zero memory of dynamic lights
    m_dynamic_light_uniforms.block = {};
    for (int i = 0; i < MAX_DYNAMIC_LIGHTS; ++i)
    {
        m_dynamic_light_uniforms.block.lights[i].attenuation[0] = 1.0;
    }

    glGenBuffers(1, &m_dynamic_light_uniforms.ubo);
    glBindBuffer(GL_UNIFORM_BUFFER, m_dynamic_light_uniforms.ubo);
    glBufferData(GL_UNIFORM_BUFFER, sizeof(shader_data_dynamic_lights),
                 (GLvoid*)&m_dynamic_light_uniforms.block, GL_STATIC_DRAW);
    m_dynamic_light_uniforms.binding_point = 3;

    glBindBufferBase(GL_UNIFORM_BUFFER, m_static_light_uniforms.binding_point,
                     m_static_light_uniforms.ubo);
    glBindBufferBase(GL_UNIFORM_BUFFER, m_dynamic_light_uniforms.binding_point,
                     m_dynamic_light_uniforms.ubo);
}

void lights::set_main_light(glm::vec3& direction, glm::vec3& look_at_position,
                            glm::vec3& ambient, glm::vec3& diffuse)
{
    auto& light = m_static_light_uniforms.block.main_light;
    light.direction = direction;
    light.ambient = ambient;
    light.diffuse = diffuse;

    glBindBuffer(GL_UNIFORM_BUFFER, m_static_light_uniforms.ubo);
    glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(shader_data_static_lights),
                    (GLvoid*) &m_static_light_uniforms.block);
    glBindBuffer(GL_UNIFORM_BUFFER, 0); 

    // Calculate main light transform
    // Position of the light itself is calculated by going backwards in the
    // direction that the light is coming from
    glm::vec3 position = look_at_position - 50.0f * direction;
    m_main_light_transform = 
        glm::ortho(-75.0f, 75.0f, -75.0f, 75.0f, 1.0f, 100.0f);
    m_main_light_transform = m_main_light_transform *
        glm::lookAt(position, look_at_position, glm::vec3(0.0f, 1.0f, 0.0f));
}

void lights::set_point_light(engine::light_id& id, glm::vec3& position,
                             glm::vec3& attenuation,
                             glm::vec3& ambient, glm::vec3& diffuse)
{
    // Allocate a slot for the point light if it doesn't have one yet
    if (m_point_lights.count(id) == 0)
    {
        engine::light_id invalid_id = engine::id_generator::get().light();
        unsigned char chosen_index = MAX_DYNAMIC_LIGHTS;
        for (int i = 0; i < MAX_DYNAMIC_LIGHTS; ++i)
        {
            if (m_point_light_slots[i] == invalid_id)
            {
                chosen_index = i;
                break;
            }
        }
        if (chosen_index == MAX_DYNAMIC_LIGHTS)
        {
            ERROR("Can't add point light because all available slots are used.\n");
            return;
        }

        m_point_light_slots[chosen_index] = id;
        m_point_lights[id] = chosen_index;
    }

    // Get the slot allotted to the point light and update data
    auto index = m_point_lights[id];
    auto& light = m_dynamic_light_uniforms.block.lights[index];
    light.position = position;
    light.attenuation = attenuation;
    light.ambient = ambient;
    light.diffuse = diffuse;

    glBindBuffer(GL_UNIFORM_BUFFER, m_static_light_uniforms.ubo);
    glBufferSubData(GL_UNIFORM_BUFFER,
                    index * sizeof(PointLight),
                    sizeof(PointLight),
                    (GLvoid*) &light);
    glBindBuffer(GL_UNIFORM_BUFFER, 0); 
}

void lights::delete_point_light(engine::light_id& id)
{
    auto index = m_point_lights[id];
    auto& light = m_dynamic_light_uniforms.block.lights[index];
    light.position = glm::vec3(0);
    light.attenuation = glm::vec3(0);
    light.ambient = glm::vec3(0);
    light.diffuse = glm::vec3(0);

    glBindBuffer(GL_UNIFORM_BUFFER, m_static_light_uniforms.ubo);
    glBufferSubData(GL_UNIFORM_BUFFER,
                    index * sizeof(PointLight),
                    sizeof(PointLight),
                    (GLvoid*) &light);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
}
