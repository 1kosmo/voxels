#pragma once

#include <unordered_map>

#include "../utils/types.hpp"
#include "shader_data.hpp"

class lights
{
public:
    lights() = default;
    ~lights() = default;

    int init_uniform_buffers();
    uniform_block<shader_data_static_lights> m_static_light_uniforms;
    glm::mat4 m_main_light_transform;
    uniform_block<shader_data_dynamic_lights> m_dynamic_light_uniforms;

    void set_main_light(glm::vec3& direction, glm::vec3& position,
                        glm::vec3& ambient, glm::vec3& diffuse);
    void set_point_light(engine::light_id& id, glm::vec3& position,
                         glm::vec3& attenuation,
                         glm::vec3& ambient, glm::vec3& diffuse);
    void delete_point_light(engine::light_id& id);

private:
    std::unordered_map<engine::light_id, unsigned char> m_point_lights;
    engine::light_id m_point_light_slots[MAX_DYNAMIC_LIGHTS];

};

