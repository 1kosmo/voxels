#pragma once

#include <GL/gl3w.h>

#include "../utils/shader_program.hpp"

class shadow_map
{
public:
    shadow_map();
    ~shadow_map();

    int init(unsigned int window_width, unsigned int window_height);

    void bind_to_write();
    void bind_to_read(GLenum texture_unit);

    shader_program* m_program;
    GLuint m_uniform_light_transform;
    GLuint m_uniform_vertex_transform;

    unsigned int m_window_width;
    unsigned int m_window_height;

private:
    GLuint m_fbo;
    GLuint m_shadow_map;
};
