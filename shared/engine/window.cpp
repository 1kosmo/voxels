#include <GL/gl3w.h>
#include <imgui/imgui_impl_sdl_gl3.h>

#include "window.hpp"
#include "msg_log.hpp"

int window::create_window(std::string window_name, int screen_width,
                          int screen_height, unsigned int current_flags)
{
    // Setup SDL
    if (SDL_Init(SDL_INIT_VIDEO|SDL_INIT_TIMER) != 0)
    {
        ERROR("SDL2 message : %s\n", SDL_GetError());
        return -1;
    }

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS,
                        SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
    SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    // OpenGL 3.3
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);

    Uint32 flags = SDL_WINDOW_OPENGL;

    if (current_flags & (int)window_flags::INVISIBLE) {
        flags |= SDL_WINDOW_HIDDEN;
    }
    if (current_flags & (int)window_flags::FULLSCREEN) {
        flags |= SDL_WINDOW_FULLSCREEN_DESKTOP;
    }
    if (current_flags & (int)window_flags::BORDERLESS) {
        flags |= SDL_WINDOW_BORDERLESS;
    }

    m_sdl_window = SDL_CreateWindow(window_name.c_str(), SDL_WINDOWPOS_CENTERED,
                                    SDL_WINDOWPOS_CENTERED,
                                    screen_width, screen_height, flags);
    if (m_sdl_window == nullptr) {
        ERROR("SDL Window could not be created!\n");
        return 1;
    }

    m_gl_context = SDL_GL_CreateContext(m_sdl_window);
    if (m_gl_context == nullptr) {
        ERROR("Could not create OpenGL Context!\n");
        return 1;
    }

    if (gl3wInit() < 0)
    {
        ERROR("Could not initialize gl3w!\n");
        return 1; 
    }

    // Disable VSync
    SDL_GL_SetSwapInterval(0);

    // Bind ImGui to window
    ImGui_ImplSdlGL3_Init(m_sdl_window);

    // Initialize vars
    m_screen_height = screen_height;
    m_screen_width = screen_width;
    // Initialize modifiers
    m_shift_mod = false;
    m_ctrl_mod = false;
    m_alt_mod = false;

    return 0;
}

void window::double_buffer()
{
    SDL_GL_SwapWindow(m_sdl_window);
}

void window::close_window()
{
    ImGui_ImplSdlGL3_Shutdown();
    SDL_GL_DeleteContext(m_gl_context);
    SDL_DestroyWindow(m_sdl_window);
    SDL_Quit();
    m_sdl_window = nullptr;
}

SDL_Window* window::get_sdl_window()
{
    return m_sdl_window;
}

void window::handle_input(int& flags)
{
    SDL_Event e;
    while (SDL_PollEvent(&e) != 0)
    {
        // See if event pertains to ImGui
        ImGui_ImplSdlGL3_ProcessEvent(&e);

        if (e.type == SDL_MOUSEMOTION)
        {
            handle_mouse_move(flags, e);
        }
        else if (e.type == SDL_KEYDOWN ||
                 e.type == SDL_KEYUP)
        {
            if (ImGui::GetIO().WantCaptureKeyboard)
            {
                continue;
            }
            
            switch (e.key.keysym.sym)
            {
            case SDLK_RSHIFT: case SDLK_LSHIFT:
                if (e.type == SDL_KEYDOWN)
                {
                    m_shift_mod = true;
                }
                else
                {
                    m_shift_mod = false;
                }                
                break;

            case SDLK_RCTRL: case SDLK_LCTRL:
                if (e.type == SDL_KEYDOWN)
                {
                    m_ctrl_mod = true;
                }
                else
                {
                    m_ctrl_mod = false;
                }                
                break;

            case SDLK_RALT: case SDLK_LALT:
                if (e.type == SDL_KEYDOWN)
                {
                    m_alt_mod = true;
                }
                else
                {
                    m_alt_mod = false;
                }
                break;

            default:
                handle_key(flags, e);                
            }
        }
        else if (e.type == SDL_MOUSEBUTTONDOWN ||
                 e.type == SDL_MOUSEBUTTONUP ||
                 e.type == SDL_MOUSEWHEEL)
        {
            handle_mouse_button(flags, e);
        }
        else if (e.type == SDL_QUIT)
        {
            flags |= (int)input_flags::QUIT;            
        }
    }
}
