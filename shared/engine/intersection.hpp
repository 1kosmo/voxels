#pragma once

#include <vector>

#include <glm/glm.hpp>

#include "bounding_volumes.hpp"

enum class test_result
{
    TRUE,
    FALSE,
    // For planes and whatnot
    ABOVE,
    BELOW
};

test_result test_aabb_plane(aabb b, plane p);
test_result test_obb_plane(obb b, plane p);

test_result test_aabb_aabb(aabb& a, aabb& b);
test_result test_aabb_aabb(aabb& a, aabb& b, glm::vec3& normal, float& penetration);
test_result test_moving_aabb_aabb(aabb& a, aabb& b, glm::vec3 va, glm::vec3 vb,
                                  float& tfirst, float& tlast);

void closest_point_point_aabb(glm::vec3 point, aabb box, glm::vec3& closest);

std::vector<glm::ivec3> get_grid_points_between(
    glm::vec3 start, glm::vec3 end, float cell_size);

bool intersect_ray_aabb(glm::vec3 ray_pos, glm::vec3 ray_dir, aabb box,
                        glm::vec3& intersection_point, glm::vec3& normal);
bool intersect_ray_obb(glm::vec3 ray_pos, glm::vec3 ray_dir, obb box,
                       glm::vec3& intersection_point, glm::vec3& normal);
