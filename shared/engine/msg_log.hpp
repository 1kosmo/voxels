#pragma once

#include <string>

namespace msg_log
{
    void info_to_cout(std::string format, ...);
    void error_to_cout(std::string format, ...);
    void warning_to_cout(std::string format, ...);
}

#define INFO(format, ...) msg_log::info_to_cout(format, ##__VA_ARGS__)
#define ERROR(format, ...) msg_log::error_to_cout(format, ##__VA_ARGS__)
#define WARNING(format, ...) msg_log::warning_to_cout(format, ##__VA_ARGS__)
