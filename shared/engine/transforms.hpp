#pragma once

#include <vector>
#include <memory>

#include <glm/glm.hpp>

#include "utils/types.hpp"

struct chunk_mesh
{
    // Quads layout : TL TR BR BL
    std::vector<glm::vec3> m_vertices;
    std::vector<glm::vec3> m_normals;
    std::vector<glm::vec3> m_colors;
    glm::mat4 m_mesh_transform;
    size_t m_num_quads;
};

typedef std::shared_ptr<chunk_mesh> chunk_mesh_ptr;

struct chunk_mesh_on_gpu
{
    engine::shader_id m_shader_to_use;
    unsigned int m_mesh_vao;
    unsigned int m_vertex_buffer;
    unsigned int m_normal_buffer;
    unsigned int m_color_buffer;
    glm::mat4 m_mesh_transform;
    glm::mat3 m_normal_transform;
    size_t m_num_quads;
};

// struct cube_collection_axis_aligned
// {
//     std::vector<glm::mat4> m_position;
//     glm::mat4 m_scale;  // All cubes in a collection have the same scale 
// };

// struct cube_collection_oriented
// {
//     std::vector<glm::mat4> m_position;
//     glm::mat4 m_orientation;
//     glm::mat4 m_parent_transform;
//     glm::mat4 m_scale;  // All cubes in a collection have the same scale 
// };

// Types
namespace engine
{
    typedef
    std::vector<std::pair<engine::chunk_id, chunk_mesh_ptr> >
    changed_chunks;

    typedef
    std::vector<std::pair<engine::chunk_id, glm::mat4> >
    transformed_chunks;
}
