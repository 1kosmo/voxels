#include "shader_program.hpp"
#include "shader_exception.hpp"
#include "gl_error_check.hpp"

#include <glm/gtc/type_ptr.hpp>
using glm::value_ptr;

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
using namespace std;

//------------------------------------------------------------------------------------
shader_program::Shader::Shader()
    : shaderObject(0),
      filePath()
{

}

//------------------------------------------------------------------------------------
shader_program::shader_program()
        : programObject(0),
          prevProgramObject(0),
          activeProgram(0)
{

}
//------------------------------------------------------------------------------------
void shader_program::generateProgramObject() {
    if(programObject == 0) {
        programObject = glCreateProgram();
    }
}

//------------------------------------------------------------------------------------
void shader_program::attachVertexShader (
		const char * filePath
) {
    vertexShader.shaderObject = createShader(GL_VERTEX_SHADER);
    vertexShader.filePath = filePath;

    extractSourceCodeAndCompile(vertexShader);
}

//------------------------------------------------------------------------------------
void shader_program::attachFragmentShader (
		const char * filePath
) {
    fragmentShader.shaderObject = createShader(GL_FRAGMENT_SHADER);
    fragmentShader.filePath = filePath;

    extractSourceCodeAndCompile(fragmentShader);
}

//------------------------------------------------------------------------------------
void shader_program::attachGeometryShader (
		const char * filePath
) {
    geometryShader.shaderObject = createShader(GL_GEOMETRY_SHADER);
    geometryShader.filePath = filePath;

    extractSourceCodeAndCompile(geometryShader);
}

//------------------------------------------------------------------------------------
void shader_program::extractSourceCodeAndCompile (
		const Shader & shader
) {
    string shaderSourceCode;
    extractSourceCode(shaderSourceCode, shader.filePath);

    compileShader(shader.shaderObject, shaderSourceCode);
}

//------------------------------------------------------------------------------------
void shader_program::recompileShaders() {
    extractSourceCodeAndCompile(vertexShader);
    extractSourceCodeAndCompile(fragmentShader);
    extractSourceCodeAndCompile(geometryShader);
}

//------------------------------------------------------------------------------------
/*
* Extracts source code from file located at 'filePath' and places contents into
* 'shaderSource'.
*/
void shader_program::extractSourceCode (
		string & shaderSource,
		const string & filePath
) {
    ifstream file;

    file.open(filePath.c_str());
    if (!file) {
        stringstream strStream;
        strStream << "Error -- Failed to open file: " << filePath << endl;
        throw shader_exception(strStream.str());
    }

    stringstream strBuffer;
    string str;

    while(file.good()) {
        getline(file, str, '\r');
        strBuffer << str;
    }
    file.close();

    strBuffer << '\0';  // Append null terminator.

    shaderSource = strBuffer.str();
}

//------------------------------------------------------------------------------------
/*
* Links all attached shaders within the shader_program.
* Note: This method must be called once before calling shader_program::enable().
*/
void shader_program::link() {
    if(vertexShader.shaderObject != 0) {
        glAttachShader(programObject, vertexShader.shaderObject);
    }

    if(fragmentShader.shaderObject != 0) {
        glAttachShader(programObject, fragmentShader.shaderObject);
    }

    if(geometryShader.shaderObject != 0) {
        glAttachShader(programObject, geometryShader.shaderObject);
    }

    glLinkProgram(programObject);
    checkLinkStatus();

    CHECK_GL_ERRORS;
}

//------------------------------------------------------------------------------------
shader_program::~shader_program() {
    deleteShaders();
}

//------------------------------------------------------------------------------------
void shader_program::deleteShaders() {
    glDeleteShader(vertexShader.shaderObject);
    glDeleteShader(fragmentShader.shaderObject);
    glDeleteProgram(programObject);
}

//------------------------------------------------------------------------------------
GLuint shader_program::createShader (
		GLenum shaderType
) {
    GLuint shaderId = glCreateShader(shaderType);
    CHECK_GL_ERRORS;

    return shaderId;
}

//------------------------------------------------------------------------------------
void shader_program::compileShader (
		GLuint shaderObject,
		const string & shaderSourceCode
) {
    const char * sourceCodeStr = shaderSourceCode.c_str();
    glShaderSource(shaderObject, 1, (const GLchar **)&sourceCodeStr, NULL);

    glCompileShader(shaderObject);
    checkCompilationStatus(shaderObject);

    CHECK_GL_ERRORS;
}

//------------------------------------------------------------------------------------
void shader_program::checkCompilationStatus (
		GLuint shaderObject
) {
    GLint compileSuccess;

    glGetShaderiv(shaderObject, GL_COMPILE_STATUS, &compileSuccess);
    if (compileSuccess == GL_FALSE) {
        GLint errorMessageLength;
        // Get the length in chars of the compilation error message.
        glGetShaderiv(shaderObject, GL_INFO_LOG_LENGTH, &errorMessageLength);

        // Retrieve the compilation error message.
        GLchar errorMessage[errorMessageLength + 1]; // Add 1 for null terminator
        glGetShaderInfoLog(shaderObject, errorMessageLength, NULL, errorMessage);

        string message = "Error Compiling Shader: ";
        message += errorMessage;

        throw shader_exception(message);
    }
}

//------------------------------------------------------------------------------------
void shader_program::enable() const {
    glUseProgram(programObject);
    CHECK_GL_ERRORS;
}

//------------------------------------------------------------------------------------
void shader_program::disable() const {
    glUseProgram((GLuint)NULL);
    CHECK_GL_ERRORS;
}

//------------------------------------------------------------------------------------
void shader_program::checkLinkStatus() {
    GLint linkSuccess;

    glGetProgramiv(programObject, GL_LINK_STATUS, &linkSuccess);
    if (linkSuccess == GL_FALSE) {
        GLint errorMessageLength;
        // Get the length in chars of the link error message.
        glGetProgramiv(programObject, GL_INFO_LOG_LENGTH, &errorMessageLength);

        // Retrieve the link error message.
        GLchar errorMessage[errorMessageLength];
        glGetProgramInfoLog(programObject, errorMessageLength, NULL, errorMessage);

        stringstream strStream;
        strStream << "Error Linking Shaders: " << errorMessage << endl;

        throw shader_exception(strStream.str());
    }
}

//------------------------------------------------------------------------------------
/*
 * Returns the GL shader program object name.
 */
GLuint shader_program::getProgramObject() const {
    return programObject;
}

//------------------------------------------------------------------------------------
/*
 * Returns the location value of a uniform variable within the shader program.
 */
GLint shader_program::getUniformLocation (
		const char * uniformName
) const {
    GLint result = glGetUniformLocation(programObject, (const GLchar *)uniformName);

    if (result == -1) {
        stringstream errorMessage;
        errorMessage << "Error obtaining uniform location: " << uniformName;
        throw shader_exception(errorMessage.str());
    }

    return result;
}

GLint shader_program::getUniformBlockLocation(const char* uniformName) const
{
    GLuint result = glGetUniformBlockIndex(
        programObject, (const GLchar *)uniformName);

    return result;
}

//------------------------------------------------------------------------------------
/*
 * Returns the location value of an attribute variable within the shader program.
 */
GLint shader_program::getAttribLocation (
		const char * attributeName
) const {
    GLint result = glGetAttribLocation(programObject, (const GLchar *)attributeName);

    if (result == -1) {
        stringstream errorMessage;
        errorMessage << "Error obtaining attribute location: " << attributeName;
        throw shader_exception(errorMessage.str());
    }

    return result;
}

