#pragma once

#include <vector>
#include <unordered_map>
#include <glm/glm.hpp>

namespace engine
{
    // ID Types
    typedef unsigned int chunk_id;
    typedef unsigned int cube_id;
    typedef unsigned int light_id;
    typedef unsigned int character_id;

    typedef unsigned int shader_id;

    class id_generator
    {
    public:
        static id_generator& get();

        cube_id cube();
        chunk_id chunk();
        light_id light();
        character_id character();
        shader_id shader();

        cube_id invalid_cube();
        chunk_id invalid_chunk();
        light_id invalid_light();
        character_id invalid_character();        
        shader_id invalid_shader();

    private:
        id_generator();
        static id_generator* m_instance;

        engine::cube_id cube_id_ctr;
        engine::chunk_id chunk_id_ctr;
        engine::light_id light_id_ctr;
        engine::character_id character_id_ctr;
        engine::shader_id shader_id_ctr;
    };

    // Non-ID Types
    typedef std::unordered_map<engine::cube_id, glm::vec3> cube_type_dict;

    int read_cube_type_dict(const char* filename, std::vector<cube_id>& order,
                            cube_type_dict& dict);
    int write_cube_type_dict(const char* filename, std::vector<cube_id>& order,
                             cube_type_dict& dict);
}
