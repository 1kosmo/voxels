/**
* ShaderException
*/

#pragma once

#include <exception>
#include <string>
using std::string;

/*
 * Exception class for encapsulating ShaderProgram error messages.
 */
class shader_exception : public std::exception {
public:
    shader_exception(const string & message)
        : error_message(message) { }

    virtual ~shader_exception() noexcept { }

    virtual const char * what() const noexcept {
        return error_message.c_str();
    }

private:
    string error_message;
};
