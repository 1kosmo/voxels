#include <cstdio>
#include <glm/gtc/type_ptr.hpp>

#include "types.hpp"
#include "../msg_log.hpp"

namespace engine
{
    id_generator* id_generator::m_instance = nullptr;

    id_generator& id_generator::get()
    {
        if (nullptr == m_instance)
        {
            m_instance = new id_generator();
        }
        return *m_instance;
    }

    cube_id id_generator::cube()
    {
        return ++cube_id_ctr;
    }

    chunk_id id_generator::chunk()
    {
        return ++chunk_id_ctr;
    }

    light_id id_generator::light()
    {
        return ++light_id_ctr;
    }

    character_id id_generator::character()
    {
        return ++character_id_ctr;
    }

    shader_id id_generator::shader()
    {
        return ++shader_id_ctr;
    }

    id_generator::id_generator()
        : cube_id_ctr(0), chunk_id_ctr(0), light_id_ctr(0), character_id_ctr(0),
          shader_id_ctr(0)
    {}

    cube_id id_generator::invalid_cube()
    {
        return 0;
    }

    chunk_id id_generator::invalid_chunk()
    {
        return 0;
    }

    light_id id_generator::invalid_light()
    {
        return 0;
    }

    character_id id_generator::invalid_character()
    {
        return 0; 
    }

    shader_id id_generator::invalid_shader()
    {
        return 0;
    }

    int read_cube_type_dict(const char* file_name, std::vector<cube_id>& order,
                            cube_type_dict& dict)
    {
        dict.clear();

        // File layout is
        // x -- number of types
        // r g b r g b ... -- color values
        FILE* file = fopen(file_name, "r");        
        if (NULL == file)
        {
            ERROR("Could not open file '%s' when reading cube type dict\n",
                  file_name);
            return 1;
        }

        size_t amt_types = 0;
        fread(&amt_types, sizeof(size_t), 1, file);

        size_t existing_types = order.size();

        for (int i = 0; i < amt_types; ++i)
        {
            glm::vec3 color; 
            fread(glm::value_ptr(color), sizeof(float), 3, file);

            cube_id id;
            if (i < existing_types)
            {
                id = order[i];
            }
            else
            {
                id = id_generator::get().cube();
                order.push_back(id);
            }
            dict[id] = color;
        }
        fclose(file);

        return 0;
    }

    int write_cube_type_dict(const char* file_name, std::vector<cube_id>& order,
                             cube_type_dict& dict)
    {
        FILE* file = fopen(file_name, "w");        
        if (NULL == file)
        {
            ERROR("Could not open file '%s' when writing cube type dict\n",
                  file_name);
            return 1;
        }

        size_t amt_types = order.size();
        fwrite(&amt_types, sizeof(size_t), 1, file);

        for (cube_id id : order)
        {
            glm::vec3 color = dict[id];
            fwrite(glm::value_ptr(color), sizeof(float), 3, file);
        }
        fclose(file);

        return 0;
    }
}
