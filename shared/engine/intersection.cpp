#include <limits>

#include "intersection.hpp"

using namespace glm;

test_result test_aabb_plane(aabb b, plane p)
{
    // // These two lines not necessary with a (center, extents) AABB representation
    // Point c = (b.max + b.min) * 0.5f; // Compute AABB center
    // Point e = b.max - c; // Compute positive extents
    // Compute the projection interval radius of b onto L(t) = b.c + t * p.n
    float r = b.halfwidths[0] * abs(p.normal[0]) +
        b.halfwidths[1] * abs(p.normal[1]) +
        b.halfwidths[2] * abs(p.normal[2]);
    // Compute distance of box center from plane
    float s = dot(p.normal, b.center) - p.d;
    // Intersection occurs when distance s falls within [-r,+r] interval
    if (abs(s) <= r)
    {
        return test_result::TRUE;
    }
    else if (s <= -r)
    {
        return test_result::BELOW;
    }
    else // if (r <= s)
    {
        return test_result::ABOVE;
    }
}

test_result test_obb_plane(obb b, plane p)
{
    // Compute the projection interval radius of b onto L(t) = b.c + t * p.n
    float r = b.halfwidths[0] * abs(dot(p.normal, b.obb_basis[0])) +
        b.halfwidths[1] * abs(dot(p.normal, b.obb_basis[1])) +
        b.halfwidths[2] * abs(dot(p.normal, b.obb_basis[2]));
    // Compute distance of box center from plane
    float s = dot(p.normal, b.center) - p.d;
    // Intersection occurs when distance s falls within [-r,+r] interval
    if (abs(s) <= r)
    {
        return test_result::TRUE;
    }
    else if (s <= -r)
    {
        return test_result::BELOW;
    }
    else // if (r <= s)
    {
        return test_result::ABOVE;
    }
}

test_result test_aabb_aabb(aabb& a, aabb& b)
{
    if (fabs(a.center[0] - b.center[0]) > (a.halfwidths[0] + b.halfwidths[0]))
    {
        return test_result::FALSE;
    }
    if (fabs(a.center[1] - b.center[1]) > (a.halfwidths[1] + b.halfwidths[1]))
    {
        return test_result::FALSE;
    }
    if (fabs(a.center[2] - b.center[2]) > (a.halfwidths[2] + b.halfwidths[2]))
    {
        return test_result::FALSE;
    }

    return test_result::TRUE;
}

test_result test_moving_aabb_aabb(aabb& a, aabb& b, vec3 va, vec3 vb,
                                  float& tfirst, float& tlast)
{
    if (test_aabb_aabb(a, b) == test_result::TRUE)
    {
        tfirst = tlast = 0.f;
        return test_result::TRUE;
    }

    // Relative velocity
    vec3 v = vb - va;

    tfirst = 0.0f;
    tlast = 1.0f;

    vec3 a_min = a.center - a.halfwidths; vec3 a_max = a.center + a.halfwidths;
    vec3 b_min = b.center - b.halfwidths; vec3 b_max = b.center + b.halfwidths;

    for (int i = 0; i < 3; i++)
    {
        if (v[i] < 0.0f)
        {
            if (b_max[i] < a_min[i]) { return test_result::FALSE; } // Nonintersecting and moving apart
            if (a_max[i] < b_min[i])
            {
                tfirst = fmax((a_max[i] - b_min[i]) / v[i], tfirst);
            }
            if (b_max[i] > a_min[i])
            {
                tlast = fmin((a_min[i] - b_max[i]) / v[i], tlast);
            }
        }
        if (v[i] > 0.0f)
        {
            if (b_min[i] > a_max[i]) { return test_result::FALSE; } // Nonintersecting and moving apart
            if (b_max[i] < a_min[i])
            {
                tfirst = fmax((a_min[i] - b_max[i]) / v[i], tfirst);
            }
            if (a_max[i] > b_min[i])
            {
                tlast = fmin((a_max[i] - b_min[i]) / v[i], tlast);
            }
        }
        // No overlap possible if time of first contact occurs after time of last contact
        if (tfirst > tlast) { return test_result::FALSE; }
    }
    return test_result::TRUE;
}

static bool test_axis(vec3 axis, float min_a, float max_a, float min_b, float max_b,
                       vec3& mtvAxis, float& mtv_distance)
{
    float axis_len_sq = dot(axis, axis);

    // If the axis is degenerate then ignore
    if (axis_len_sq < 1.0e-8f)
    {
        return false;
    }

    // Calculate the two possible overlap ranges
    // Either we overlap on the left or the right sides
    float d0 = (max_b - min_a);   // 'Left' side
    float d1 = (max_a - min_b);   // 'Right' side

    // Intervals do not overlap, so no intersection
    if (d0 <= 0.0f || d1 <= 0.0f)
    {
        return true;
    }
    // Find out if we overlap on the 'right' or 'left' of the object.
    float overlap = (d0 < d1) ? d0 : -d1;

    // The mtd vector for that axis
    vec3 sep = axis * (overlap / axis_len_sq);

    // The mtd vector length squared
    float sep_len_sq = dot(sep, sep);

    // If that vector is smaller than our computed Minimum Translation Distance use
    // that vector as our current MTV distance
    if (sep_len_sq < mtv_distance)
    {
        mtv_distance = sep_len_sq;
        mtvAxis = sep;
    }

    return false;
}

test_result test_aabb_aabb(aabb& a, aabb& b, glm::vec3& normal, float& penetration)
{
    vec3 a_min = a.center - a.halfwidths; vec3 a_max = a.center + a.halfwidths;
    vec3 b_min = b.center - b.halfwidths; vec3 b_max = b.center + b.halfwidths;

    float mtv_distance = std::numeric_limits<float>::max();
    vec3 mtv_axis; 

    if (test_axis(glm::vec3(1.f, 0.f, 0.f), a_min.x, a_max.x, b_min.x, b_max.x,
                  mtv_axis, mtv_distance))
    {
        return test_result::FALSE;
    }
    if (test_axis(glm::vec3(0.f, 1.f, 0.f), a_min.y, a_max.y, b_min.y, b_max.y,
                  mtv_axis, mtv_distance))
    {
        return test_result::FALSE;
    }
    if (test_axis(glm::vec3(0.f, 0.f, 1.f), a_min.z, a_max.z, b_min.z, b_max.z,
                  mtv_axis, mtv_distance))
    {
        return test_result::FALSE;
    }

    penetration = sqrt(mtv_distance) * 1.001f;
    normal = normalize(mtv_axis);

    return test_result::TRUE;
}

void closest_point_point_aabb(glm::vec3 point, aabb box, glm::vec3& closest)
{
    // For each coordinate axis, if the point coordinate value is
    // outside box, clamp it to the box, else keep it as is
    glm::vec3 box_min = box.center - box.halfwidths;
    glm::vec3 box_max = box.center + box.halfwidths;
    for (int i = 0; i < 3; i++) {
        float v = point[i];
        if (v < box_min[i]) v = box_min[i];
        if (v > box_max[i]) v = box_max[i];
        closest[i] = v;
    }
}

//////////////////// Ray Marching ////////////////////

std::vector<ivec3> get_grid_points_between(vec3 start, vec3 end, float cell_size)
{
    // Side dimensions of the square cell
    // Determine start grid cell coordinates (i, j)
    // int i = (int)x1;
    // int j = (int)y1;
    // vec3 ray_dir = normalize(end - start);
    // start += ray_dir * 0.1f;

    // Flooring is important for negative coordinates
    ivec3 int_start(floor(start / cell_size));
    ivec3 int_end(floor(end / cell_size));

    // Determine end grid cell coordinates (iend, jend)
    // int iend = (int)x2;
    // int jend = (int)y2;
    // Determine in which primary direction to step
    int di = ((start.x < end.x) ? 1 : ((start.x > end.x) ? -1 : 0));
    int dj = ((start.y < end.y) ? 1 : ((start.y > end.y) ? -1 : 0));
    int dk = ((start.z < end.z) ? 1 : ((start.z > end.z) ? -1 : 0));
    // Determine tx and ty, the values of t at which the directed segment
    // (x1,y1)-(x2,y2) crosses the first horizontal and vertical cell
    // boundaries, respectively. Min(tx, ty) indicates how far one can
    // travel along the segment and still remain in the current cell
    float minx = cell_size * floorf(start.x / cell_size);
    float maxx = minx + cell_size;
    float tx = ((start.x > end.x) ? (start.x - minx) : (maxx - start.x))
        / fabs(end.x - start.x);

    float miny = cell_size * floorf(start.y / cell_size);
    float maxy = miny + cell_size;
    float ty = ((start.y > end.y) ? (start.y - miny) : (maxy - start.y))
        / fabs(end.y - start.y);

    float minz = cell_size * floorf(start.z / cell_size);
    float maxz = minz + cell_size;
    float tz = ((start.z > end.z) ? (start.z - minz) : (maxz - start.z))
        / fabs(end.z - start.z);

    // Determine deltax/deltay, how far (in units of t) one must step
    // along the directed line segment for the horizontal/vertical
    // movement (respectively) to equal the width/height of a cell
    float deltatx = cell_size / fabs(end.x - start.x);
    float deltaty = cell_size / fabs(end.y - start.y);
    float deltatz = cell_size / fabs(end.z - start.z);

    tx += deltatx;
    ty += deltaty;
    tz += deltatz;    

    // Main loop. Visits cells until last cell reached
    std::vector<ivec3> cells;
    for (;;)
    {
        cells.push_back(int_start);
        if (tx <= ty && tx <= tz)
        {
            // tx smallest, step in x            
            if (int_start.x == int_end.x) break;
            tx += deltatx;
            int_start.x += di;
        }
        else if (ty <= tz)
        {
            // ty smallest, step in y
            if (int_start.y == int_end.y) break;
            ty += deltaty;
            int_start.y += dj;
        }
        else
        {
            // tz smallest, step in z
            if (int_start.z == int_end.z) break;
            tz += deltatz;
            int_start.z += dk;
        }
    }

    return cells;
}

//////////////////// Ray tracing intersection ////////////////////

bool intersect_ray_aabb(glm::vec3 ray_pos, glm::vec3 ray_dir, aabb box,
                        glm::vec3& intersection_point, glm::vec3& normal)
{
    // Initialize intersection points to min and max
    float tmin = 0.0f;
    float tmax = std::numeric_limits<float>::max();

    glm::vec3 boxmin = box.center - box.halfwidths;
    glm::vec3 boxmax = box.center + box.halfwidths;

    const float epsilon = std::numeric_limits<float>::epsilon();

    for (int i = 0; i < 3; ++i)
    {
        if (fabs(ray_dir[i]) < epsilon)
        {
            // Ray is parallel, so just check if ray is within slab or not
            if (ray_pos[i] < boxmin[i] || ray_pos[i] > boxmax[i]) { return false; }
        }
        else
        {
            // Compute intersection t value of ray with near and far plane of side
            float ood = 1.0f / ray_dir[i];
            float t1 = (boxmin[i] - ray_pos[i]) * ood;
            float t2 = (boxmax[i] - ray_pos[i]) * ood;

            if (t1 > t2) { std::swap(t1, t2); }

            // Compute the intersecion of slab intersection intervals
            tmin = fmax(t1, tmin);
            tmax = fmin(t2, tmax);

            // If slab intersection empty, exit with no intersection
            if (tmin > tmax) { return false; }
        }
    }
    // Ray intersects all 3 points
    intersection_point = ray_pos + tmin * ray_dir;

    // Calculate normal
    if (fabs(intersection_point.x - boxmin.x) < epsilon)
    {
        // std::cout << " Got square normal! " << std::endl;
        normal = vec3(-1.0, 0.0, 0.0);
    }
    else if (fabs(intersection_point.x - boxmax.x) < epsilon)
    {
        // std::cout << " Got square normal! " << std::endl;
        normal = vec3(1.0, 0.0, 0.0);
    }
    else if (fabs(intersection_point.y - boxmin.y) < epsilon)
    {
        // std::cout << " Got square normal! " << std::endl;
        normal = vec3(0.0, -1.0, 0.0);
    }
    else if (fabs(intersection_point.y - boxmax.y) < epsilon)
    {
        // std::cout << " Got square normal! " << std::endl;
        normal = vec3(0.0, 1.0, 0.0);
    }
    else if (fabs(intersection_point.z - boxmin.z) < epsilon)
    {
        // std::cout << " Got square normal! " << std::endl;
        normal = vec3(0.0, 0.0, -1.0);
    }
    else if (fabs(intersection_point.z - boxmax.z) < epsilon)
    {
        // std::cout << " Got square normal! " << std::endl;
        normal = vec3(0.0, 0.0, 1.0);
    }

    return true;
}

bool intersect_ray_obb(vec3 ray_pos, vec3 ray_dir, obb box,
                       glm::vec3& intersection_point, vec3& normal)
{
    // Transform the ray to OBB space
    vec3 ray_pos_m_box = ray_pos - box.center;
    vec3 obb_ray_start(dot(ray_pos_m_box, box.obb_basis[0]),
                       dot(ray_pos_m_box, box.obb_basis[1]),
                       dot(ray_pos_m_box, box.obb_basis[2]));

    vec3 ray_end_m_box =  ray_pos + ray_dir - box.center;
    vec3 obb_ray_end(dot(ray_end_m_box, box.obb_basis[0]),
                     dot(ray_end_m_box, box.obb_basis[1]),
                     dot(ray_end_m_box, box.obb_basis[2]));
    vec3 obb_ray_dir = obb_ray_end - obb_ray_start;

    // Use aabb intersection test
    if (intersect_ray_aabb(obb_ray_start, obb_ray_dir,
                           aabb(box.center, box.halfwidths),
                           intersection_point, normal))
    {
        // TODO 
        return true;
    }

    return false;
}
