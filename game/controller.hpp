#pragma once

#include <engine/window.hpp>
#include <engine/characters/camera_3p.hpp>
#include <engine/renderer/renderer.hpp>
#include <engine/environment/environment_manager.hpp>
#include <engine/utils/types.hpp>
#include <engine/characters/character_manager.hpp>
#include <engine/audio_system.hpp>

#include "entity_controller.hpp"

class controller : public window
{
public:
    ~controller();

    int init();
    void run();

private:
    virtual void handle_key(int& flags, SDL_Event& event);
    virtual void handle_mouse_button(int&flags, SDL_Event& event);
    virtual void handle_mouse_move(int&flags, SDL_Event& event);

    bool m_game_started = false;
    bool m_game_paused = false;
    bool m_draw_player_bound = false;

    engine::character_id m_player;
    entity_controller m_entities;

    camera_3p m_camera;
    // Camera vars
    glm::vec3 m_camera_pos;
    glm::mat4 m_camera_transform;
    void position_main_light();

    renderer m_renderer;

    engine::cube_type_dict m_cube_type_dict;
    engine::cube_type_dict m_character_cube_types;
    environment_manager m_environment;
    character_manager m_character_manager;
    void debug_draw_player_bounding_box();

    engine::shader_id m_basic_environment_shader;
    engine::shader_id m_basic_character_shader;

    void gui_logic(int& flag);

    // Variables related to program logic
    audio_system m_audio_system; 
    int init_sound();

    bool m_middle_mouse_down = false;
};

