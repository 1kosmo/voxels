file(GLOB root_source "*.cpp")
file(GLOB_RECURSE source "*.cpp")

add_executable(run-game ${root_source} ${source})

# target_link_libraries(run-game ${GCC_COVERAGE_LINK_FLAGS} ${my_libraries})
target_link_libraries(run-game ${my_libraries})
