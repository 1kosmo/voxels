#include "entity_controller.hpp"

using namespace std::chrono;

void entity_controller::init(character_manager* characters)
{
    ms m_game_start_time = duration_cast< milliseconds >(
        system_clock::now().time_since_epoch());    
    m_characters = characters;

    auto f1 = m_characters->spawn_cube_with_color(glm::vec3(-2, 2, -1), 1);
    m_friendlies.insert(f1);
    m_patrols.emplace_back(f1, glm::vec2(-2, -1), glm::vec2(-2, 3));

    auto e1 = m_characters->spawn_cube_with_color(glm::vec3(-3, 4, -40), 2);
    auto e2 = m_characters->spawn_cube_with_color(glm::vec3(0.0f, 4, -47), 2);
    auto e3 = m_characters->spawn_cube_with_color(glm::vec3(2.f, 4, -47), 2);

    m_enemies.insert(e1);
    m_enemies.insert(e2);
    m_enemies.insert(e3);    
}

void entity_controller::update()
{
    ms now = duration_cast< milliseconds >(
        system_clock::now().time_since_epoch());    
    int time_diff = (now - m_game_start_time).count();

    for (auto& patrol : m_patrols)
    {
        glm::vec2 cur_pos(m_characters->get_character_position(patrol.entity));
        if (glm::ivec2(cur_pos) == patrol.cur_target)
        {
            m_characters->set_character_forward_acceleration(patrol.entity, 0.0f);
            patrol.cur_target = (patrol.cur_target == patrol.end) ?
                patrol.start : patrol.end;
            patrol.orientation_multiplier *= -1.f;
        }
        
        // glm::vec2 dir = glm::vec2(patrol.cur_target) - cur_pos;
        // glm::vec2 cdir(m_characters->get_character_facing_direction(
        //                    patrol.entity));
        // m_characters->add_character_relative_orientation(patrol.entity,
        //                                                  glm::dot(dir, cdir));
        m_characters->set_character_forward_acceleration(
            patrol.entity, patrol.orientation_multiplier * 1.0);     
    }
}

entity_patrol::entity_patrol(engine::character_id entity, glm::ivec2 start,
                             glm::ivec2 end)
    : entity(entity), start(start), end(end), cur_target(end),
      orientation_multiplier(1.0)
{
}
