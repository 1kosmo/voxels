#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/string_cast.hpp>
#include <imgui/imgui_impl_sdl_gl3.h>

#include <engine/msg_log.hpp>

#include "controller.hpp"

void controller::gui_logic(int& flag)
{
    ImGui_ImplSdlGL3_NewFrame(m_sdl_window);

    static bool first_run = true;
    if (first_run)
    {
        ImGui::SetNextWindowPos(ImVec2(m_screen_width / 2.0 - 100,
                                       m_screen_height / 2.0 - 100));
        first_run = false;
    }

    static bool show_debug_window(true);
    // ImGuiWindowFlags window_flags(ImGuiWindowFlags_AlwaysAutoResize);
    ImGuiWindowFlags window_flags(ImGuiWindowFlags_NoResize);
    float opacity(0.5f);

    if (!m_game_started)
    {
        ImGui::Begin("Block Hopper in Candy Land", &show_debug_window, ImVec2(300,300),
                     window_flags);
        if (ImGui::Button("Play"))
        {
            m_game_started = true;
            m_camera = camera_3p();
            m_camera.set_tracking_distance(5.0f);
            SDL_SetRelativeMouseMode(SDL_TRUE);            
        }
        ImGui::NewLine();
        if (ImGui::Button("Quit Game"))
        {
            flag |= (int)input_flags::QUIT;
        }        
        
        ImGui::End();
    }

    if (m_game_paused)
    {
        ImGui::Begin("Block Hopper in Candy Land", &show_debug_window, ImVec2(150,100),
                     window_flags);
        if (ImGui::Button("Resume"))
        {
            m_game_paused = false;
            SDL_SetRelativeMouseMode(SDL_TRUE);            
        }
        ImGui::NewLine();
        if (ImGui::Button("Quit Game"))
        {
            flag |= (int)input_flags::QUIT;
        }

        ImGui::NewLine();
        if (ImGui::Button("Toggle Cel-Shading"))
        {
            m_renderer.toggle_cel_shading();
        }
        if (ImGui::Checkbox("Show Player Bounding Box", &m_draw_player_bound))
        {
            INFO("THIS GOT CALLED\n");
            debug_draw_player_bounding_box();
        }
        
        ImGui::End();
    }
}

void controller::handle_key(int& flags, SDL_Event& event)
{
    if (!m_game_started || m_game_paused)
    {
        if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_ESCAPE
            && m_game_paused)
        {
            m_game_paused = false;
        }
        return;
    }
    
    if (event.type == SDL_KEYDOWN)
    {
        switch (event.key.keysym.sym)
        {
        case SDLK_w:
        case SDLK_UP:
            m_character_manager.set_character_forward_acceleration(m_player, 7.f);
            break;
        case SDLK_s:
        case SDLK_DOWN:
            m_character_manager.set_character_forward_acceleration(m_player, -7.f);
            break;
        case SDLK_d:
        case SDLK_RIGHT:
            m_character_manager.set_character_side_acceleration(m_player, 7.f);
            break;
        case SDLK_a:
        case SDLK_LEFT:
            m_character_manager.set_character_side_acceleration(m_player, -7.f);
            break;

        // Buttons with functionality
        case SDLK_SPACE:
            m_character_manager.make_character_jump(m_player, 5.0f);
            break;

        case SDLK_ESCAPE:
            SDL_SetRelativeMouseMode(SDL_FALSE);                        
            m_game_paused = m_game_started && !m_game_paused;
            break;

        }
    }
    else if (event.type == SDL_KEYUP)
    {
        switch (event.key.keysym.sym)
        {
        case SDLK_w:
        case SDLK_UP:
            m_character_manager.set_character_forward_acceleration(m_player, 0.0f);
            break;
        case SDLK_s:
        case SDLK_DOWN:
            m_character_manager.set_character_forward_acceleration(m_player, 0.0f);
            break;
        case SDLK_d:
        case SDLK_RIGHT:
            m_character_manager.set_character_side_acceleration(m_player, 0.0f);
            break;
        case SDLK_a:
        case SDLK_LEFT:
            m_character_manager.set_character_side_acceleration(m_player, 0.0f);
            break;
        }
    }
}

void controller::handle_mouse_button(int&flags, SDL_Event& event)
{
    if (event.type == SDL_MOUSEBUTTONDOWN)
    {
        switch (event.button.button)
        {
        case SDL_BUTTON_LEFT:

            break;
        case SDL_BUTTON_MIDDLE:
            m_middle_mouse_down = true;
            break;
        case SDL_BUTTON_RIGHT:

            break;
        }
    }
    else if (event.type == SDL_MOUSEBUTTONUP)
    {
        switch (event.button.button)
        {
        case SDL_BUTTON_MIDDLE:
            m_middle_mouse_down = false;
            break;
        }
    }
}

void controller::handle_mouse_move(int&flags, SDL_Event& event)
{
    if (!m_game_started || m_game_paused)
    {
        return;
    }
    
    int x = event.motion.x;
    int y = event.motion.y;

    // INFO("Mouse coords: (%d, %d)\n",  x, y);

    int x_rel = event.motion.xrel;
    int y_rel = event.motion.yrel;

    if (m_middle_mouse_down)
    {
        m_camera.pan_x(-y_rel);
        m_camera.pan_y(-x_rel);
    }
    else
    {
        m_character_manager.add_character_relative_orientation(m_player,
                                                               -x_rel);
    }
}
