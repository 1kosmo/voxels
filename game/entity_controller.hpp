#pragma once

#include <time.h>
#include <cstring>
#include <chrono>
#include <unordered_set>

#include <engine/characters/character_manager.hpp>
#include <engine/utils/types.hpp>

struct entity_patrol
{
    engine::character_id entity;
    glm::ivec2 start, end;
    glm::ivec2 cur_target;
    float orientation_multiplier;

    entity_patrol() = default;
    entity_patrol(engine::character_id entity, glm::ivec2 start, glm::ivec2 end);
};

class entity_controller
{
public:
    void init(character_manager* characters);
    void update();

    typedef std::chrono::milliseconds ms;
    ms m_game_start_time;
    
    character_manager* m_characters;
    // std::vector<engine::character_id> m_entities;
    std::unordered_set<engine::character_id> m_enemies;
    std::unordered_set<engine::character_id> m_friendlies;
    std::vector<entity_patrol> m_patrols;
};
