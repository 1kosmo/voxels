#include <random>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/string_cast.hpp>

#include <engine/msg_log.hpp>

#include "controller.hpp"

int controller::init()
{
    create_window("Game", 1280, 720, 0);

    // FMOD Initialization 
    init_sound();

    m_renderer.init(this, 90, m_screen_width, m_screen_height,
                    m_camera.get_transform(), m_camera.get_pos());
    
    m_character_manager.init(&m_audio_system);

    m_basic_environment_shader =
        // TODO : Copy assets folder to build folder 
        m_renderer.load_program("../assets/phong_aligned_objects_vert.glsl",
                                "../assets/phong_aligned_objects_frag.glsl");
    m_basic_character_shader =
        m_renderer.load_program("../assets/phong_general_vert.glsl",
                                "../assets/phong_aligned_objects_frag.glsl",
                                true);

    // Read data files
    std::vector<engine::cube_id> order;
    engine::read_cube_type_dict("../assets/main.dict", order, m_cube_type_dict);
    engine::read_cube_type_dict("../assets/model.dict", order, m_character_cube_types);
    
    m_environment.read_from("../assets/world.env");

    position_main_light();

    // Test spawn location
    m_player = m_character_manager.spawn_cube(glm::vec3(1.f, 25.0f, 4.0f));
    m_character_manager.set_character_absolute_orientation(m_player, 180);
    
    m_camera.set_tracking_distance(50.0f);
    m_camera.pan_y(30);
    m_camera.track_point(glm::vec3(-1.f, 3.f, -36.f),
                         glm::vec3(1.f, 0.f, 0.f));

    m_entities.init(&m_character_manager);

    // Init Random Seed
    srand(time(NULL));


    return 0;
}

controller::~controller()
{
    m_audio_system.Shutdown();
    close_window();
}

void controller::run()
{
    int flags = 0;
    while (0 == (flags & (int)input_flags::QUIT))
    {
        if (m_game_started)
        {
            m_camera.track_character(m_player, m_character_manager);
        }

        // Handling input must be done before ImGui logic code
        handle_input(flags);
        gui_logic(flags);

        // Character handling 
        if (!(m_game_paused || !m_game_started))
        {
            m_entities.update();
            m_character_manager.move_characters(m_environment);
        }

        if (m_draw_player_bound)
        {
            debug_draw_player_bounding_box();
        }

        // Environment updates
        auto changed_chunks = m_environment.get_changed_chunks(m_cube_type_dict);
        for (auto& chunk : changed_chunks)
        {
            m_renderer.update_chunk(chunk.first, chunk.second,
                                    m_basic_environment_shader);
        }
        if (m_environment.m_deletion_list.size() > 0)
        {
            for (engine::chunk_id chunk : m_environment.m_deletion_list)
            {
                m_renderer.delete_chunk(chunk);
            }
        }
        if (m_environment.m_new_transparent_cubes.size() > 0)
        {
            for (auto cube : m_environment.m_new_transparent_cubes)
            {
                glm::vec4 color(m_cube_type_dict[cube.type], 0.3f); 
                m_renderer.set_transparent_cube(cube.id, color, cube.pos, 1.0);
            }
        }

        // Character updates
        auto changed_characters =
            m_character_manager.get_changed_chunks(m_character_cube_types);
        for (auto& body_node : changed_characters)
        {
            m_renderer.update_chunk(body_node.first, body_node.second,
                                    m_basic_character_shader, true);
        }
        auto character_transforms = m_character_manager.get_transformed_chunks();
        m_renderer.update_chunk_transforms(character_transforms, true);

        if (m_camera.changed())
        {
            m_camera_transform = m_camera.get_transform();

            m_renderer.update_camera_transform(m_camera_transform);
        }
        if (m_camera.changed_pos())
        {
            m_camera_pos = m_camera.get_pos();

            m_renderer.update_camera_position(m_camera_pos);
        }

        m_renderer.draw();

        // FMOD needs to be called once per tick
        m_audio_system.Update();
    }
}

void controller::position_main_light()
{
    glm::vec3 pos(-4.f, 44.f, -14.f);
    glm::vec3 look_at(-4.f, 16.f, -38.f);

    m_renderer.update_main_light(
        glm::normalize(look_at - pos), // Direction
        look_at, // Position
        glm::vec3(0.15f, 0.15f, 0.15f), // Ambient
        glm::vec3(1.0f, 1.0f, 1.0f)); // Diffuse
}

void controller::controller::debug_draw_player_bounding_box()
{
    static bool first_run = true;
    static engine::chunk_id line_ids[12];
    
    if (first_run)
    {
        for (int i = 0; i < 12; ++i)
        {
            line_ids[i] = engine::id_generator::get().chunk();
        }
        first_run = false;
    }

    if (!m_draw_player_bound)
    {
        for (int i = 0; i < 12; ++i)
        {
            m_renderer.set_line(line_ids[i],
                                std::make_pair(glm::vec3(0.0), glm::vec3(0.0)));
        }
        return;
    }

    aabb box = m_character_manager.get_character_bounding_aabb(m_player);

    glm::vec3 x(box.halfwidths.x, 0.f, 0.f);
    glm::vec3 y(0.f, box.halfwidths.y, 0.f);
    glm::vec3 z(0.f, 0.f, box.halfwidths.z);    

    //Front face
    m_renderer.set_line(
        line_ids[0], std::make_pair(box.center + x + y + z, box.center + x - y + z));
    m_renderer.set_line(
        line_ids[1], std::make_pair(box.center + x + y + z, box.center - x + y + z));
    m_renderer.set_line(
        line_ids[2], std::make_pair(box.center - x - y + z, box.center + x - y + z));
    m_renderer.set_line(
        line_ids[3], std::make_pair(box.center - x - y + z, box.center - x + y + z));

    //Back face
    m_renderer.set_line(
        line_ids[4], std::make_pair(box.center + x + y - z, box.center + x - y - z));
    m_renderer.set_line(
        line_ids[5], std::make_pair(box.center + x + y - z, box.center - x + y - z));
    m_renderer.set_line(
        line_ids[6], std::make_pair(box.center - x - y - z, box.center + x - y - z));
    m_renderer.set_line(
        line_ids[7], std::make_pair(box.center - x - y - z, box.center - x + y - z));

    // Right side
    m_renderer.set_line(
        line_ids[8], std::make_pair(box.center + x + y + z, box.center + x + y - z));
    m_renderer.set_line(
        line_ids[9], std::make_pair(box.center + x - y + z, box.center + x - y - z));

    // Left side
    m_renderer.set_line(
        line_ids[10], std::make_pair(box.center - x + y + z, box.center - x + y - z));
    m_renderer.set_line(
        line_ids[11], std::make_pair(box.center - x - y + z, box.center - x - y - z));
}

int controller::init_sound()
{
    m_audio_system.Init();
    m_audio_system.LoadSound("../assets/effect_bounce.wav");
    m_audio_system.LoadSound("../assets/effect_jump.wav");
    m_audio_system.LoadSound("../assets/effect_wind.wav");

    return 0;
}
