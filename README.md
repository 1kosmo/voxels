# Build Instructions
Execute the following instruction under the project root:
```
mkdir build && cd build && cmake .. && make && cd ..
```
To run the game execute the `build/run-game` binary while inside the `build` folder:
```
cd build && ./run-game && cd ..
```
To run the the environment editor execute `cd build && ./env-editor && cd ..`.

# Controls
- WASD to move
- Space to jump
- ESC to pause and open option menu
