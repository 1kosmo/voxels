\documentclass {article}
\usepackage{fullpage}
\usepackage[backend=bibtex,style=ieee]{biblatex}

\bibliography{./bib}{}

\begin{document}

~\vfill
\begin{center}
\Large

A5 Project Documentation

Title: Voxel World Platformer

Name: Konstantin Bogomolov

Student ID: 20550560

User ID: kbogomol
\end{center}
\vfill ~\vfill~
\newpage
\section{Manual}
System requirements: CMake 3.5 or higher installed (Graphics lab computers satisfy this).

\subsection{Steps for build:}
\begin{itemize}
\item Under project root, make 'build' directory : \texttt{mkdir build}
\item Go to this build directory : \texttt{cd build}
\item Execute CMake targeting root directory: \texttt{cmake ..}
\item Execute Make : \texttt{make}
\end{itemize} 

\subsection{Binaries:}
This will create two binaries under the build directory:
\begin{itemize}
\item \texttt{final-project}, which is the binary for the game;
\item \texttt{env-editor}, which is the binary for my editor I used to make the level in the game.
\end{itemize}
Both binaries must be run from the build directory, as they reference assets stored under \texttt{../assets/}.

\subsection{Usage Instructions:}
\texttt{final-project} :
\begin{itemize}
\item WASD to move character
\item Move mouse to change character orientation
\item Space to jump
\item Press middle mouse button and move mouse to change camera orientation
\item ESC to pause and open pause menu
\end{itemize}
\texttt{env-editor} :
\begin{itemize}
\item Load in a sample ``env'' and ``dict'' file by typing in \texttt{../assets/world.env} and \texttt{../assets/main.dict} in their respective fields, and pressing ``Read ENV'' and ``Read DICT''.
\item Move camera with IJKL.
\item Orient camera by pressing middle mouse button and moving the mouse.
\item Press Space to place a cube of the selected material in the world
\item Use WASD to move selected position in X/Z coordinates; use Shift-W and Shift-S to move selected position in Y coordinates.
\item ESC to close program
\end{itemize}

\newpage
  
\section{Purpose}
The purpose was to make a fighting game with voxel (cube) based graphics for both characters and the environment. \\

I have implemented environment modeling, but was not able to implement character modeling and animation; this of course means that the ``fighting game'' portion of my project was left unfinished. Aside from that and the implementation of particle systems, however, I feel like I have completed the rest of my objectives and goals.

\section{Statement}
  The original idea was to make a third-person (over the shoulder) fighting game, where the goal was to traverse a dungeon and defeat enemies, where both the environment and the characters would be made out of simple, untextured, cube primitives. The game would be based around a sword fighting system, where damage gets represented by characters' bodies becoming visibly broken. For example, as the player swings a sword into an enemy, the hit will register as visible damage on the enemy, with parts of his body removed where the sword hit. \\

  First I will need to make an editing mode in order to place individual cubes to model my environments and characters. I will build upon Assignment 3 to implement keyframe animations to make a walking animation for the characters. I will implement static collision detection for characters against floors and walls, and then dynamic collision detection for the aforementioned damage system. To visualize this damage system, imagine a character body made of many small cubes, that when hit with a sword becomes damaged by some of the cubes being deleted in places where the sword hit. This is also where I intend to use a particle system, in order to spray blood from the damaged parts of the body. Aside from these elements that make the core of the gameplay, I will also have to synchronize sound with sword hits and footsteps, implement shadows, and include scenes that demonstrate trasparency in certain objects. \\

  I think the idea is interesting because of the proposed damage system, because it goes away from the old video-game idea of ``hit-points'' and ``health-bars'', and creates the potential to have very cool looking fighting scenarios, of one character fighting with an arm chopped off, for example. I suspect most of the challenge will come from the dynamic collision component of this game, because even though I will have basic cube-on-cube collisions, I think I will have to implement a sophisticated hierarchy of bounding volumes to perform collisions with, because otherwise collision would probably be too expensive, considering each character will be made of hundreds of voxels. Another challenge will come with implementing efficient storage of the voxels that make the environment. Storing information about individual character models will be even more of a challenge, considering I will have to represent hierarchy of body parts for animations, and the individual cubes that make up those body parts. \\

  Having not had the time to properly implement key-frame animation, however, I had to modify the project to be a simple platformer where the player controls a simple cube. Never the less, during this project I have learned to implement robust and reliable collision detection, for both dynamic and static objects; I have learned how to implement shadow maps and how to deal with their artifacts; and I have also learned how to do simple cel-shading. 
  \section{Technical Outline}

  \subsection{Data storage:}
  To store information about the environment and characters I will need to use a data format to keep track of position and cube type.

  On runtime, cube type was kept track with unique IDs bound to color values represented by 3 floats, stored in an unordered map.

  Storing the cube positions was more complex, as I could not store the entirety of my level in one big structure, since the size of the level was not predetermined. I solved this issue in the traditional way that other voxel engines have, by splitting the world into ``chunks'', and allocating new chunks as they became necessary. Segmenting the world in this way also allowed me to make my rendering efficient, which I'll touch upon later. Each chunk represents a $64*64*64$ voxel space in the world. When loading a level, chunks get loaded one at a time, as an array of size $64*64*64$, and get stored in an unordered map, addressable by chunk position: this allows quick $O(1)$ retrieval/update of any voxel.

  Character models work very similarly to world chunks, except that a ``chunk'' in a character model is a part of a character, such as an arm, torso, or leg, which is represented by a 3D array of arbitrary dimensions, instead of fixed size like world chunks. These character nodes also record information such as their position, orientation, and scale relative to the parent, whereas world chunks record only position. Each character model is defined by a collection of nodes, each addressable by ID of the node, which allows $O(1)$ operations on any part of the model without a tree traversal. Having each node addressable by ID also allows me to write the data of each model to a file in the same fashion as world chunks are written, just with the extra preamble of which ID is a child of which other ID.

  \subsection{Efficient Rendering:}
  Instead of rendering every voxel contained in a chunk, I have opted to generate a mesh from each chunk. This allows for a very important optimization of reducing the amount draw calls I have per mesh from $64*64*64$ draw calls, to just 1. I also considered using instanced rendering, a technique which would allow for a similar improvement. Generating a mesh has another benefit over instanced rendering, however, in that I can cull sides of a voxel that are not being shown on the outside of the mesh. Using instanced rendering per cube would have forced me to draw every cube in its' entirety even if only one of its faces shown.

  A new mesh only needs to get generated when a chunk gets updated. So in my game, the only time meshes are generated is at the very start. Since my level only uses about 7 chunks, that means only 7 draw calls are used to render the whole level (disregarding extra passes for shadow map generation and cel-shading, and rendering transparent voxels).

  \subsection{Collision Detection:}
  Collision detection was implemented as described by Ericson \autocite{collision} as AABB to AABB (axis aligned bounding box) character on character collisions and character to wall or floor collisions. While the collision detection was easy to get working, the hardest part was implementing the collision resolution. I ended up implementing a system where 2 dynamic characters would actively push each other in opposite directions, while a character colliding with a static cube would not automatically be pushed away.

  My collision detection is optimized by the fact that I only test static collision against voxel-space that a character occupies -- which is something I can easily determined pased on the dimensions of a character and his position. For dynamic collision detection, I test every character against every other character every time, which isn't too efficient at $O(n^2)$, but is sufficient for my purposes. I could further optimize this by testing a character only against characters that are located in the same chunk.

  \subsection{Shadows:}
  According to research on the internet, a reliable way to implement shadows is to use shadow maps as first desribed by Williams \autocite{shadow-maps}. I did this by generating a Z-buffer for my directional light source, which recorded which the depth of the object closest to the light source \autocite{real-time-render}. I did this in a separate rendering pass, where I rendered the scene from the point of view of the light source, and wrote the depth values to a $2048*2048$ texture. On the actual rendering pass from the point of view of my camera, I also passed in the transformation of the light source, and computed both transformations for each vertex; if the $z$ depth value of a fragment was higher than the depth value stored in the shadow map texture, then I eliminated directional lighting for it, making it look in the shadow.

  \subsection{Transparent Objects:}
  Transparent objects were handled separately from the chunks. I opted to draw every transparent voxel one by one, because of the order that transparent objects had to be drawn in. I had to draw the furthest away transparent voxels first, so that the colors would look natural if one transparent object overlapped another in the point of view of the camera.

  \subsection{Cel-Shading:}
  To do cel-shading, I made a separate rendering pass that drew only the backfacing polygons of my voxels in black, with a small offset to each vertex to make these backfaces visible behind the actually visible front-faces \autocite{cel-shading}. I had to be careful not to add too much offset to each vertex, because then the seams of my meshes would become visible in the back-faces. While this was a crude way to implement cel-shading, it added a lot of flair to my rather plain looking voxels and achieved the goal it was supposed to.

  \subsection{Animation:}
  While I did not implement animation, I planned to do it with linear quaternion interpolations for body part rotations between set keyframes \autocite{quats}. This seemed very efficient in terms of rendering, because I would not have to upload any new meshes for body part movements, but only update body-part transformation matrices of the body parts that moved. I also planned to bind sound effects to keyframes, which would have synchronize the sound of a footstep to an actual step, for example.

  \subsection{Particle System:}
  I also did not implement a particle system. My plan was to draw the particles using instanced rendering. I was planning to use billboarding as suggested by Akenine-Moller et al. \autocite{real-time-render} to render each particle quad as always facing the camera, and then change the color or texture of each quad based on how I wanted it to look.  

  \printbibliography
\newpage

\section{Implementation Details}

\subsection{Code walkthrough:}
As mentioned in the ``Binaries'' section of the manual, I had to make two different programs : one to edit the level and one to be the game (I also planned on implementing a third editor for character models and animation, but did not have the time for it). Both of these programs used the same components for a lot of shared functionality, which I encapsulated under the \texttt{/shared/engine/} library.

\subsubsection{Game and env-editor binaries:}
The general way each binary is structured using my ``engine'' is like this:
\begin{enumerate}
\item Make \texttt{controller} class (see \texttt{controller.cpp} under \texttt{/game/}) that inherits from engine \texttt{window} class and defines input handling functions (see \texttt{program\_io.cpp.}).
\item Instantiate engine \texttt{renderer} and \texttt{camera} classes.
\item Give \texttt{renderer} meshes and mesh transform matrices with unique ID's to draw something. In the engine, two classes have the capability to generate meshes to be passed to the \texttt{renderer}: \texttt{environment\_manager} and \texttt{character\_manager}.

  When a mesh is updated, it only needs to get passed to the \texttt{renderer} once before it's stored in an OpenGL buffer that is tracked by the unique ID of the mesh.
\item In the main loop I handle input and handle various manager updates before the draw calls, because I found that to feel the most responsive.
\end{enumerate}

\subsubsection{Engine:}
The main 3 sections of the engine are:
\begin{itemize}
\item \texttt{engine/renderer/} : Contains everything to do with drawing
  \begin{itemize}
  \item \texttt{renderer.cpp} : Main rendering loop; interface for controller to use; and contains shader program initialization.
  \item \texttt{draw.cpp} : General mesh drawing, shadow-map drawing pass, and cel-shading pass functions.
  \item \texttt{shadow\_map.cpp} : Encapsulates initialization and use of shadow map texture
  \item \texttt{lights.cpp} : Encapsulates initialization and use of directional and point lights.
  \item \texttt{renderer\_types.cpp} : Minor helper classes for various purposes.
  \item \texttt{shader\_data.hpp} : C++ structs that map directly to GLSL uniform buffer objects used in my shaders.
  \end{itemize}
\item \texttt{engine/environment/} : Stores and updates the environment
  \begin{itemize}
  \item \texttt{chunk.cpp} : Class that contains an array representing a 3D chunk of voxels in the world. Of note is the mesh-generation that is done per chunk here.
  \item \texttt{environment\_manager.cpp} : Dictionary of chunks that manages their updates, storage and retrieval. Provides interface to controller to read and write chunks, and to compute chunk meshes.
  \end{itemize}
\item \texttt{engine/characters/} : Creates character entities and manages their movements
  \begin{itemize}
  \item \texttt{character\_node.cpp} : Defines one single node in the character, stores all of its' transformations relative to its' parent. 
  \item \texttt{character.cpp} : A collection of nodes; the hierarchial structure of a character is defined here. Global operations on characters like movement and orientation are also defined here.
  \item \texttt{character\_manager.cpp} : Manages instances of characters and provides interface to control them.
  \item \texttt{camera\_3p.cpp} : Camera class specific to tracking a character over-the-shoulder.
  \end{itemize}
\end{itemize}

The engine is also made up of these smaller modules:
\begin{itemize}
\item \texttt{window.cpp} : SDL window initialization and input handler. ``Controllers'' defined in my programs inherit from this class.
\item \texttt{camera.cpp} : Implementation of a camera
\item \texttt{audio\_system.cpp} : Wrapper around FMOD initialization and function calls.
\item \texttt{bounding\_volumes.cpp} and \texttt{intersection.cpp} : Code that handles intersection tests between certain volumes.
  \item \texttt{msg\_log.cpp} and \texttt{utils/*.cpp} : Message logging, error checking, shader program wrapper and custom typedefs. 
  
  \end{itemize}

\subsection{3rd Party Libraries:}  
All 3rd party libraries are dynamically linked and packaged in with the project under the \texttt{/shared/} directory, so the project should compile and run on any Linux computer that has CMake installed. There are some differences from libraries used in assignments. All of the used libraries are:
\begin{itemize}
\item gl3w : Used for OpenGL context creation. \texttt{/shared/GL/}
\item SDL2 : Used for window creation and handling input. \texttt{/shared/SDL2/}
\item imgui : Used for UI. \texttt{/shared/imgui/}
\item glm : Linear algebra library. \texttt{/shared/glm/}
\item fmod : Used for handling sound. \texttt{/shared/fmod/}
\end{itemize}

\subsection{Assets:}
Under the \texttt{/assets/} folder are files that my project expects to find on runtime. These include GLSL programs, level data, and sound files.
\subsubsection{Shaders:}
The simplest shader program I use is one to draw my transparent cubes, defined under \texttt{basic\_shader\_\{vert|frag\}.glsl}. \\
The shader program I use for generating my shadow map texture is defined by \texttt{simple\_depth\_\{vert|frag\}.glsl}. \\ 
The cel-shading pass of my rendering is done under \texttt{cel\_shader\_\{vert|frag\}.glsl}. \\
The most interesting shader is one that's used to draw my chunk meshes, which is

\texttt{phong\_aligned\_objects\_\{vert|frag\}.glsl} and \texttt{phong\_general\_vert.glsl}. This shader includes phong diffuse lighting and shadow application using the shadow map texture. \\

You will notice that most shaders contain common uniform buffer object definitions. This is done so that my updates to program uniforms are less verbose for complex uniform data like that for lights, but also so that I am not forced to explicitly bind and unbind every program when updating my data.

\subsubsection{Level data:}
The level data is in the file \texttt{/assets/world.env}; the accompanying voxel-type dictionary file is \texttt{/assets/main.dict}. \\

Level data is written in a custom binary format the details of which can be found in \texttt{environment\_manager.cpp}. Essentialy, the format writes out every used chunk in its entirety, using one character per voxel. 

\newpage


\noindent{\Large\bf Objectives:}

{\hfill{\bf Full UserID:   kbogomol}\hfill{\bf Student ID:   20550560}\hfill}

\begin{enumerate}
     \item[\_\_\_ 1:]  Modeling the scene.

     \item[\_\_\_ 2:]  UI.

     \item[\_\_\_ 3:]  Transparency using the alpha channel.

     \item[\_\_\_ 4:]  Keyframe based animation using linear quaternion interpolation.

     \item[\_\_\_ 5:]  Synchronized sound.

     \item[\_\_\_ 6:]  Static collision detection.

     \item[\_\_\_ 7:]  Dynamic collision detection.

     \item[\_\_\_ 8:]  Cel shading.

     \item[\_\_\_ 9:]  Shadows using shadow mapping.

     \item[\_\_\_ 10:]  Particle system.
\end{enumerate}

% Delete % at start of next line if this is a ray tracing project
% A4 extra objective:
\end{document}
