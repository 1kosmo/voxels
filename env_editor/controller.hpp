#pragma once

#include <engine/window.hpp>
#include <engine/camera.hpp>
#include <engine/renderer/renderer.hpp>
#include <engine/environment/environment_manager.hpp>
#include <engine/utils/types.hpp>

class controller : public window
{
public:
    ~controller();

    int init();
    void run();

private:
    virtual void handle_key(int& flags, SDL_Event& event);
    virtual void handle_mouse_button(int&flags, SDL_Event& event);
    virtual void handle_mouse_move(int&flags, SDL_Event& event);

    camera m_camera;
    // Camera vars
    glm::vec3 m_camera_pos;
    glm::mat4 m_camera_transform;
    glm::mat4 m_camera_inverse_transform;
    void position_main_light();

    renderer m_renderer;

    engine::cube_type_dict m_cube_type_dict;
    environment_manager m_environment;

    engine::shader_id m_basic_environment_shader;

    void gui_logic();
    void cube_type_editor();
    void material_editor();

    // Variables related to program logic
    char* m_cur_env_file_name;
    char* m_cur_dict_file_name;
    std::vector<engine::cube_id> m_defined_cube_types;
    std::vector<std::vector<std::pair<engine::cube_id, int> > > m_materials;
    int m_chosen_material;

    // Selection variables
    engine::chunk_id m_selection_cube;
    glm::vec4 m_selection_color;
    glm::ivec3 m_selection_cube_pos;

    engine::chunk_id m_mouse_selection_cube;
    glm::vec4 m_mouse_selection_color;
    glm::ivec3 m_mouse_selection_cube_pos;
    float m_mouse_selection_radius;

    std::pair<bool, glm::ivec3> m_marked_point;
    engine::chunk_id m_marked_point_id;
    glm::vec4 m_marked_point_color;
    void fill_from_marked_to_selected();

    engine::chunk_id m_mouse_selection_ray;

    // Cube placing
    void check_cube_probabilities();
    engine::cube_id create_new_cube_type();
    void place_cube_at(glm::ivec3& pos, bool transparent = false);

    // Key modifiers
    bool m_middle_mouse_down = false;
};

