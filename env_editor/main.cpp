#include <engine/msg_log.hpp>

#include "controller.hpp"

int main(int argc, char** argv)
{
    INFO("Welcome to the game\n");

    controller c;
    if (c.init() == 0)
    {
        c.run();
    }

    return 0;
}
