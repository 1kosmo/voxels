#include <random>
#include <time.h>
#include <cstring>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/string_cast.hpp>

#include <engine/msg_log.hpp>

#include "controller.hpp"

int controller::init()
{
    create_window("Game", 1280, 720, 0);

    m_renderer.init(this, 90, m_screen_width, m_screen_height,
                    m_camera.get_transform(), m_camera.get_pos());
    
    m_basic_environment_shader =
        // TODO : Copy assets folder to build folder 
        m_renderer.load_program("../assets/phong_aligned_objects_vert.glsl",
                                "../assets/phong_aligned_objects_frag.glsl");

    m_cur_env_file_name = new char[32];
    strcpy(m_cur_env_file_name, "world.env");

    m_cur_dict_file_name = new char[32];
    strcpy(m_cur_dict_file_name, "main.dict");

    // Init selection cube
    m_selection_cube_pos = glm::ivec3(0);
    m_mouse_selection_cube_pos = glm::ivec3(0);

    m_selection_cube = engine::id_generator::get().chunk();
    m_selection_color = glm::vec4(1.0f, 1.0f, 1.0f, 0.5f);
    m_renderer.set_transparent_cube(m_selection_cube,
                                    m_selection_color, m_selection_cube_pos, 1.25f);

    m_mouse_selection_radius = 32.0f;
    m_mouse_selection_cube = engine::id_generator::get().chunk();
    m_mouse_selection_color = glm::vec4(1.0f, 0.2f, 0.2f, 0.4f);    

    m_marked_point = std::make_pair(false, glm::ivec3(0));
    m_marked_point_id = engine::id_generator::get().chunk();
    m_marked_point_color = glm::vec4(1.0f, 0.5f, 0.0f, 0.6f);

    m_mouse_selection_ray = engine::id_generator::get().chunk();

    // Initial camera position
    m_camera.relative_move(glm::vec3(0.0, 20.0, 10.0));
    // m_camera.pan_y(180);
    m_camera.pan_x(65);
    
    // Init Random Seed
    srand(time(NULL));

    create_new_cube_type();

    return 0;
}

controller::~controller()
{
    close_window();
}

void controller::run()
{
    int flags = 0;
    while (0 == (flags & (int)input_flags::QUIT))
    {
        // Handling input must be done before ImGui logic code
        handle_input(flags);

        gui_logic();

        m_renderer.set_transparent_cube(m_selection_cube, m_selection_color,
                                        m_selection_cube_pos, 1.25);

        auto changed_chunks = m_environment.get_changed_chunks(m_cube_type_dict);
        for (auto& chunk : changed_chunks)
        {
            INFO("Passed on updated chunk to renderer : passing in num of quads = %u\n",
                 chunk.second->m_num_quads);
            m_renderer.update_chunk(chunk.first, chunk.second,
                                    m_basic_environment_shader);
        }
        if (m_environment.m_deletion_list.size() > 0)
        {
            for (engine::chunk_id chunk : m_environment.m_deletion_list)
            {
                m_renderer.delete_chunk(chunk);
            }
            m_environment.m_deletion_list.clear();
        }
        if (m_environment.m_new_transparent_cubes.size() > 0)
        {
            for (auto cube : m_environment.m_new_transparent_cubes)
            {
                glm::vec4 color(m_cube_type_dict[cube.type], 0.3f); 
                m_renderer.set_transparent_cube(cube.id, color, cube.pos, 1.0);
            }
            m_environment.m_new_transparent_cubes.clear();
        }
        
        if (m_camera.changed())
        {
            m_camera_transform = m_camera.get_transform();
            m_camera_inverse_transform = glm::inverse(m_camera_transform);

            m_renderer.update_camera_transform(m_camera_transform);
        }
        if (m_camera.changed_pos())
        {
            m_camera_pos = m_camera.get_pos();

            m_renderer.update_camera_position(m_camera_pos);
        }

        position_main_light();    

        m_renderer.draw();
    }
}

void controller::check_cube_probabilities()
{    
    if (m_materials.size() == 0)
    {
        return;
    }

    int running_sum = 0;
    
    for (std::pair<engine::cube_id, int>& cube_type : m_materials[m_chosen_material])
    {
        int& probability = cube_type.second;
        if (running_sum == 100)
        {
            probability = 0;
            continue;
        }
        if (running_sum + probability > 100)
        {
            probability = 100 - running_sum;
        }
        running_sum += probability;
    }    
}

void controller::place_cube_at(glm::ivec3& pos, bool transparent)
{
    if (m_materials.size() == 0)
    {
        return;
    }

    int choice = rand() % 100;

    engine::cube_id invalid_id = engine::id_generator::get().invalid_cube();
    engine::cube_id chosen = invalid_id;

    int running_sum = 0;
    for (std::pair<engine::cube_id, int>& cube_type : m_materials[m_chosen_material])
    {
        if ((choice <= (running_sum + cube_type.second)) &&
            (choice > running_sum))
        {
            chosen = cube_type.first;
            break;
        }
        running_sum += cube_type.second;
    }

    if (m_cube_type_dict.count(chosen))
    {
        if (transparent)
        {
            m_environment.set_transparent_cube_at_position(pos, chosen);
        }
        else
        {
            m_environment.set_cube_at_position(pos, chosen);
        }
        INFO("Placed cube of color %s\n", to_string(m_cube_type_dict[chosen]).c_str());
    }
    else
    {
        INFO("NOTE: No block was chosen to be placed\n");
    }
}

engine::cube_id controller::create_new_cube_type()
{
    engine::cube_id new_id = engine::id_generator::get().cube();
    m_defined_cube_types.push_back(new_id);
    m_cube_type_dict[new_id] = glm::vec4(0.0, 0.0, 0.0, 1.0);

    return new_id;
}

void controller::fill_from_marked_to_selected()
{
    if (!m_marked_point.first)
    {
        m_marked_point = std::make_pair(true, m_selection_cube_pos);
        m_renderer.set_transparent_cube(
            m_marked_point_id, m_marked_point_color,
            m_marked_point.second, 1.25);
        return;
    }

    glm::ivec3 start(std::min(m_marked_point.second.x, m_selection_cube_pos.x),
                     std::min(m_marked_point.second.y, m_selection_cube_pos.y),
                     std::min(m_marked_point.second.z, m_selection_cube_pos.z));
    glm::ivec3 end(std::max(m_marked_point.second.x, m_selection_cube_pos.x),
                   std::max(m_marked_point.second.y, m_selection_cube_pos.y),
                   std::max(m_marked_point.second.z, m_selection_cube_pos.z));

    int dx = (start.x > end.x) ? (-1) : 1;
    int dy = (start.y > end.y) ? (-1) : 1;
    int dz = (start.z > end.z) ? (-1) : 1;    

    for (int x = start.x; x <= end.x; ++x)
    {
        for (int y = start.y; y <= end.y; ++y)
        {
            for (int z = start.z; z <= end.z; ++z)
            {
                glm::ivec3 pos(x, y, z);
                place_cube_at(pos);
            }
        }
    }

    m_marked_point.first = false;
    m_renderer.delete_transparent_cube(m_marked_point_id);
}

void controller::position_main_light()
{
    glm::vec3 ray_dir = m_camera.get_view_dir();

    glm::ivec3 intersection;
    m_environment.intersect_ray(m_camera_pos, ray_dir, 32, intersection);

    m_renderer.update_main_light(glm::vec3(0.0f, -1.0f, 0.3f), // Direction
                                 glm::vec3(intersection), // Position
                                 glm::vec3(0.15f, 0.15f, 0.15f), // Ambient
                                 glm::vec3(1.0f, 1.0f, 1.0f)); // Diffuse
}
// void controller::position_main_light()
// {
//     glm::vec3 pos(-4.f, 44.f, -20.f);
//     glm::vec3 look_at(-4.f, 16.f, -38.f);

//     m_renderer.update_main_light(
//         glm::normalize(look_at - pos), // Direction
//         look_at, // Position
//         glm::vec3(0.15f, 0.15f, 0.15f), // Ambient
//         glm::vec3(1.0f, 1.0f, 1.0f)); // Diffuse
// }
