#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/string_cast.hpp>
#include <imgui/imgui_impl_sdl_gl3.h>

#include <engine/msg_log.hpp>

#include "controller.hpp"

void controller::gui_logic()
{
    ImGui_ImplSdlGL3_NewFrame(m_sdl_window);

    static bool first_run = true;
    if (first_run)
    {
        ImGui::SetNextWindowPos(ImVec2(50, 50));
        first_run = false;
    }

    static bool show_debug_window(true);
    ImGuiWindowFlags window_flags(ImGuiWindowFlags_AlwaysAutoResize);
    float opacity(0.5f);

    ImGui::Begin("Properties", &show_debug_window, ImVec2(100,100), opacity,
                 window_flags);

    
    ImGui::Text("Camera Position: (%.2f, %.2f, %.2f)",
                m_camera_pos.x, m_camera_pos.y, m_camera_pos.z);
    ImGui::Text("WASD Selection: (%d, %d, %d)",
                m_selection_cube_pos.x, m_selection_cube_pos.y,
                m_selection_cube_pos.z);
    ImGui::Text("Mouse Selection: (%d, %d, %d)",
                m_mouse_selection_cube_pos.x, m_mouse_selection_cube_pos.y,
                m_mouse_selection_cube_pos.z);
    ImGui::Text("Mouse Selection Radius: %.1f", m_mouse_selection_radius);

    ImGui::NewLine();

    ImGui::InputText("ENV Name", m_cur_env_file_name, 32);
    if (ImGui::Button("Read ENV"))
    {
        m_environment.read_from(m_cur_env_file_name);
    }
    ImGui::SameLine();
    if (ImGui::Button("Write ENV"))
    {
        m_environment.write_to(m_cur_env_file_name);
    }
    
    ImGui::NewLine();

    ImGui::InputText("DICT Name", m_cur_dict_file_name, 32);
    if (ImGui::Button("Read DICT"))
    {
        engine::read_cube_type_dict(m_cur_dict_file_name, m_defined_cube_types,
                                    m_cube_type_dict);
    }
    ImGui::SameLine();
    if (ImGui::Button("Write DICT"))
    {
        engine::write_cube_type_dict(m_cur_dict_file_name, m_defined_cube_types,
                                     m_cube_type_dict);
    }

    ImGui::NewLine();

    static bool show_cube_type_editor = false;
    ImGui::Checkbox("Cube Type Editor", &show_cube_type_editor);
    if (show_cube_type_editor)
    {
        ImGui::SetNextWindowPos(ImVec2(m_screen_width - 400, 50));
        ImGui::Begin("Type Editor", &show_cube_type_editor, ImVec2(100, 100),
                     opacity, window_flags);
        cube_type_editor();
        ImGui::End();
    }

    static bool show_material_editor = false;
    ImGui::Checkbox("Material Editor", &show_material_editor);
    if (show_material_editor)
    {
        ImGui::SetNextWindowPos(ImVec2(50, m_screen_height / 1.75));
        ImGui::Begin("Material Editor", &show_material_editor, ImVec2(100, 100),
                     opacity, window_flags);
        material_editor();
        ImGui::End();
    }

    if (m_marked_point.first)
    {
        ImGui::NewLine();
        ImGui::Text("Marked Point : %s",
                    glm::to_string(m_marked_point.second).c_str());
    }

    ImGui::NewLine();
    // ImGui::PushItemWidth(40);
    ImGui::InputInt3("Selection Pos", glm::value_ptr(m_selection_cube_pos));
    // ImGui::PopItemWidth();

    ImGui::End();
}

void controller::cube_type_editor()
{
    int defined_types = m_defined_cube_types.size();
    for (int i = 0; i < defined_types; ++i)
    {
        ImGui::PushID(i);
        ImGui::Text("ID : %u", m_defined_cube_types[i]);
        ImGui::SameLine();
        ImGui::ColorEdit3("##Colour", glm::value_ptr(
                              m_cube_type_dict[m_defined_cube_types[i]]));
        ImGui::PopID();
    }

    if (ImGui::Button("New Type"))
    {
        create_new_cube_type();
    }
}

void controller::material_editor()
{
    int defined_mats = m_materials.size();
    engine::cube_id invalid_type = engine::id_generator::get().invalid_cube();

    for (int i = 0; i < defined_mats; ++i)
    {
        ImGui::PushID(i);
        ImGui::RadioButton("##Mat", &m_chosen_material, i);
        if (i == m_chosen_material)
        {
            int num_types = m_materials[i].size();
            for (int j = 0; j < num_types; ++j)
            {
                std::pair<engine::cube_id, int>& cube_type = m_materials[i][j];
                ImGui::PushID(j);
                ImGui::PushItemWidth(75);
                ImGui::InputInt("Type ID", (int*)&cube_type.first);
                if (m_cube_type_dict.count(cube_type.first))
                {
                    glm::vec3& color = m_cube_type_dict[cube_type.first];
                    ImGui::SameLine();
                    ImGui::InputInt("\%", &cube_type.second);
                    ImGui::SameLine();
                    ImGui::PushItemWidth(30);
                    ImGui::ColorEdit3("##Colour", glm::value_ptr(color));
                    ImGui::PopItemWidth();
                }
                ImGui::PopItemWidth();
                ImGui::PopID();
            }

            if (ImGui::Button("Use another type"))
            {
                m_materials[i].push_back(std::make_pair(invalid_type, 0));
            }
        }

        ImGui::PopID();
    }

    ImGui::NewLine();

    if (ImGui::Button("New Material"))
    {
        m_materials.push_back(std::vector<std::pair<engine::cube_id, int> >());
    }
}

void controller::handle_key(int& flags, SDL_Event& event)
{
    if (event.type == SDL_KEYDOWN)
    {
        switch (event.key.keysym.sym)
        {
        case SDLK_w:
        case SDLK_UP:
            if (m_shift_mod)
            {
                m_selection_cube_pos.y += 1;
            }
            else
            {
                m_selection_cube_pos.z += 1;
            }
            break;
        case SDLK_s:
        case SDLK_DOWN:
            if (m_shift_mod)
            {
                m_selection_cube_pos.y -= 1;
            }
            else
            {
                m_selection_cube_pos.z -= 1;
            }
            break;
        case SDLK_d:
        case SDLK_RIGHT:
            m_selection_cube_pos.x += 1;
            break;
        case SDLK_a:
        case SDLK_LEFT:
            m_selection_cube_pos.x -= 1;
            break;

        case SDLK_i:
            if (m_shift_mod)
            {
                m_camera.relative_move(glm::vec3(0.0, 1.0, 0.0));
            }
            else
            {
                m_camera.relative_move(m_camera.get_view_dir());
            }
            break;
        case SDLK_k:
            if (m_shift_mod)
            {
                m_camera.relative_move(glm::vec3(0.0, -1.0, 0.0));
            }
            else
            {            
                m_camera.relative_move(-m_camera.get_view_dir());
            }
            break;
        case SDLK_l:
            m_camera.relative_move(m_camera.get_right_of_view_dir());
            break;
        case SDLK_j:
            m_camera.relative_move(m_camera.get_left_of_view_dir());
            break;            

            // Buttons with functionality
        case SDLK_SPACE:
            check_cube_probabilities();
            place_cube_at(m_selection_cube_pos);
            break;
        case SDLK_m:
            fill_from_marked_to_selected();
            break;
        case SDLK_t:
            place_cube_at(m_selection_cube_pos, true);
            break;
        case SDLK_BACKSPACE:
            m_environment.delete_cube_at_position(m_selection_cube_pos);
            break;
            
        case SDLK_ESCAPE:
            flags |= (int)input_flags::QUIT;
            break;

        }
    }
}

void controller::handle_mouse_button(int&flags, SDL_Event& event)
{
    if (event.type == SDL_MOUSEBUTTONDOWN)
    {
        switch (event.button.button)
        {
        case SDL_BUTTON_LEFT:
            if (m_ctrl_mod)
            {
                check_cube_probabilities();
                place_cube_at(m_mouse_selection_cube_pos);
            }
            break;
        case SDL_BUTTON_MIDDLE:
            m_middle_mouse_down = true;
            break;
        case SDL_BUTTON_RIGHT:
            if (m_ctrl_mod)
            {
                m_selection_cube_pos = m_mouse_selection_cube_pos;
                m_renderer.set_transparent_cube(m_selection_cube, m_selection_color,
                                                m_selection_cube_pos, 1.25);
            }
            break;
        }
    }
    else if (event.type == SDL_MOUSEBUTTONUP)
    {
        switch (event.button.button)
        {
        case SDL_BUTTON_MIDDLE:
            m_middle_mouse_down = false;
            break;
        }
    }
    else if (event.type == SDL_MOUSEWHEEL && m_ctrl_mod)
    {
        m_mouse_selection_radius =
            fmax(m_mouse_selection_radius + event.wheel.y, 1.0f);
    }
}

void controller::handle_mouse_move(int&flags, SDL_Event& event)
{
    int x = event.motion.x;
    int y = event.motion.y;

    // INFO("Mouse coords: (%d, %d)\n",  x, y);

    int x_rel = event.motion.xrel;
    int y_rel = event.motion.yrel;

    if (m_middle_mouse_down)
    {
        m_camera.pan_x(-y_rel);
        m_camera.pan_y(-x_rel);
    }
    else if (m_ctrl_mod)
    {
        glm::ivec3 intersection;
        glm::vec3 ray_dir =
            m_renderer.get_projection_frustum()->send_ray_from_origin(
                x, m_screen_height - y,
                m_camera_transform,
                glm::vec4(0.0, 0.0, m_screen_width, m_screen_height));
            
        // m_renderer.set_line(m_mouse_selection_ray,
        //                     std::make_pair(m_camera_pos, m_camera_pos + 64.0f * normalize(ray_dir)));

        m_environment.intersect_ray(m_camera_pos, ray_dir,
                                    m_mouse_selection_radius, intersection);

        m_mouse_selection_cube_pos = intersection;
        m_renderer.set_transparent_cube(
            m_mouse_selection_cube, m_mouse_selection_color, 
            m_mouse_selection_cube_pos, 1.25);
    }
}
