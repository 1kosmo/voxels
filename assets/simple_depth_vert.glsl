#version 330 core

layout (location = 0) in vec3 vertex_pos;

uniform mat4 vertex_transform;
uniform mat4 light_space_transform;

void main()
{
    gl_Position = light_space_transform * vertex_transform * vec4(vertex_pos, 1.0);
}
