#version 330 core

layout(location = 0) in vec3 vertex_pos;
layout(location = 1) in vec3 vertex_normal;

out vec3 Normal;

uniform mat4 vertex_transform;

layout (std140) uniform shader_data_vert
{
    mat4 projection;
    mat4 camera;
};

void main()
{
    gl_Position = projection * camera * vertex_transform *
        vec4(vertex_pos + vertex_normal * 0.03, 1.0); 
}
