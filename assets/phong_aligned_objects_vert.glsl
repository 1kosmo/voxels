#version 330 core

layout(location = 0) in vec3 vertex_pos;
layout(location = 1) in vec3 vertex_normal;
layout(location = 2) in vec3 vertex_color;

out vec3 FragPos;
out vec3 Normal;
out vec3 Color;
out vec4 FragPosLightSpace;

uniform mat4 vertex_transform;
uniform mat4 light_space_transform;

layout (std140) uniform shader_data_vert
{
    mat4 projection;
    mat4 camera;
};

void main()
{
    FragPos = vec3(vertex_transform * vec4(vertex_pos, 1.0));
    Color = vertex_color;
    Normal = vertex_normal;
    FragPosLightSpace = light_space_transform * vec4(FragPos, 1.0);

    gl_Position = projection * camera * vec4(FragPos, 1.0); 
}
