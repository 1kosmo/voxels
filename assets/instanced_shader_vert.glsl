#version 330 core

layout(location = 0) in vec3 vertex_pos;
layout(location = 1) in vec4 cube_color;
layout(location = 2) in mat4 instance_matrix;

out vec4 Color;

uniform mat4 projection;
uniform mat4 camera;

void main()
{
    gl_Position = projection * camera * instance_matrix * vec4(vertex_pos, 1.0); 
    Color = cube_color;
}
