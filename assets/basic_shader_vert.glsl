#version 330 core

layout(location = 0) in vec3 vertex_pos;

uniform mat4 vertex_transform;

layout (std140) uniform shader_data_vert
{
    mat4 projection;
    mat4 camera;
};

void main()
{
    gl_Position = projection * camera * vertex_transform * vec4(vertex_pos, 1.0); 
}
