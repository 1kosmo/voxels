#version 330 core

layout(location = 0) in vec3 vertex_pos;
layout(location = 1) in vec3 vertex_normal;
layout(location = 2) in vec3 vertex_color;

out vec3 Color;

uniform mat4 chunk_transform;
uniform mat4 projection;
uniform mat4 camera;

void main()
{
    gl_Position = projection * camera * chunk_transform * vec4(vertex_pos, 1.0); 
    Color = vertex_color;
}
