#version 330 core

#define NR_POINT_LIGHTS 8

in vec3 FragPos;
in vec3 Normal; // The passed in normal is always normalized for this shader
in vec3 Color;
in vec4 FragPosLightSpace;

out vec4 FragColor;

//-- Light structures 
struct DirLight
{
    vec3 direction;
  
    vec3 ambient;
    vec3 diffuse;
    // vec3 specular;
};  

struct PointLight
{    
    vec3 position;
    
    vec3 attenuation;
    // float constant;
    // float linear;
    // float quadratic;  

    vec3 ambient;
    vec3 diffuse;
    // vec3 specular;
};  

// Uniform blocks
layout (std140) uniform shader_data_frag
{
    vec3 view_pos;
};

layout (std140) uniform static_lights
{ 
    DirLight dir_light;
};

layout (std140) uniform dynamic_lights
{
    PointLight point_lights[NR_POINT_LIGHTS];    
};
  
// Fragment Uniforms
uniform sampler2D shadow_map;

//-- Other functions
vec3 CalcDirLight(DirLight light, vec3 view_dir);
vec3 CalcPointLight(PointLight light, vec3 frag_pos, vec3 view_dir);
float shadow_calculation(vec4 frag_pos_light_space, vec3 light_dir);

void main()
{
    // properties
    vec3 view_dir = normalize(view_pos - FragPos);

    // directional
    vec3 result = CalcDirLight(dir_light, view_dir);

    // point lights
    for(int i = 0; i < NR_POINT_LIGHTS; i++)
    {
        result += CalcPointLight(point_lights[i], FragPos, view_dir);
    }
    
    FragColor = vec4(result, 1.0);
}

//-- Light calculation functions
vec3 CalcDirLight(DirLight light, vec3 view_dir)
{
    vec3 light_dir = normalize(-light.direction);
    // diffuse shading
    float diff = max(dot(Normal, light_dir), 0.0);
    // specular shading
    // vec3 reflect_dir = reflect(-light_dir, Normal);
    // float spec = pow(max(dot(view_dir, reflect_dir), 0.0), cur_material.shininess);
    // combine results
    vec3 ambient = light.ambient;
    vec3 diffuse = light.diffuse * diff;;
    // vec3 specular = light.specular * spec * vec3(Color);
    // return (ambient + diffuse + specular);

    float shadow = shadow_calculation(FragPosLightSpace, light_dir);

    return (ambient + diffuse * (1.0 - shadow)) * Color;
}

vec3 CalcPointLight(PointLight light, vec3 frag_pos, vec3 view_dir)
{
    vec3 light_dir = normalize(light.position - frag_pos);
    // diffuse shading
    float diff = max(dot(Normal, light_dir), 0.0);
    // specular shading
    // vec3 reflect_dir = reflect(-light_dir, Normal);
    // float spec = pow(max(dot(view_dir, reflect_dir), 0.0), cur_material.shininess);
    // attenuation
    float distance    = length(light.position - frag_pos);
    float attenuation = 1.0 / (light.attenuation[0] +
                               light.attenuation[1] * distance + 
                               light.attenuation[2] * (distance * distance));    
    // combine results
    vec3 ambient  = light.ambient  * Color;
    vec3 diffuse  = light.diffuse  * diff * Color;
    // vec3 specular = light.specular * spec * vec3(Color);
    ambient  *= attenuation;
    diffuse  *= attenuation;
    // specular *= attenuation;
    // return (ambient + diffuse + specular);
    return (ambient + diffuse);
} 

float shadow_calculation(vec4 frag_pos_light_space, vec3 light_dir)
{
    // Perspective divide to normalize (only necessary for perspective projection)
    vec3 projection_coords = frag_pos_light_space.xyz / frag_pos_light_space.w;

    // Map coords to [0, 1]
    projection_coords = projection_coords * 0.5 + 0.5;

    float closest_depth = texture(shadow_map, projection_coords.xy).r;
    float current_depth = projection_coords.z;

    // float bias = max(0.05 * (1.0 - dot(Normal, light_dir)), 0.005);
    // float bias = 0.0;
    // float shadow = (current_depth - bias > closest_depth) ? 1.0 : 0.0;
    float shadow = (current_depth > closest_depth) ? 1.0 : 0.0;
    if (projection_coords.z > 1.0)
    {
        shadow = 0.0;
    }

    return shadow;
}
